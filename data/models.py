from django.db import models
from django.db.models import Q
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from cStringIO import StringIO
from operator import itemgetter
from taskqueue.models import BackgroundTask
import warnings


class PiliomeDBVersion(models.Model):
    db_version = models.CharField(max_length=30)
    release_notes = models.TextField(blank=True)
    release_date = models.DateTimeField(blank=True, null=True)
    
    def __unicode__(self):
        return 'PiliomeDB version %s' % self.db_version
    
    class Meta:
            verbose_name = 'PiliomeDB Version'
            verbose_name_plural = 'PiliomeDB Versions'


class Taxon(models.Model):
    level = models.CharField(max_length=50) # kingdom, phylum, class, etc.
    name = models.CharField(max_length=200)
    ncbi_taxon_id = models.CharField(max_length=50, blank=True)
    img_taxon_id = models.CharField(max_length=50, blank=True)
    with_genome = models.BooleanField(default=False)
    img_submission_id = models.CharField(max_length=30, blank=True)
    gold_id = models.CharField(max_length=30, blank=True)
    external_links = models.TextField(blank=True)
    genome_type = models.CharField(max_length=30, blank=True)
    sequencing_status = models.CharField(max_length=30, blank=True)
    img_release_date = models.CharField(max_length=30, blank=True)
    img_add_date = models.CharField(max_length=30, blank=True)
    obsolete_flag = models.CharField(max_length=5, blank=True)
    img_product_flag = models.CharField(max_length=5, blank=True)
    is_public = models.CharField(max_length=5, blank=True)
    isolation_country = models.CharField(max_length=100, blank=True)
    ncbi_project_id = models.CharField(max_length=20, blank=True)
    publication_journal = models.TextField(blank=True)
    gold_sequencing_status = models.CharField(max_length=20, blank=True)
    sequencing_center = models.TextField(blank=True)
    assembly_method = models.TextField(blank=True)
    gram_staining = models.CharField(max_length=20, blank=True)
    host_name = models.CharField(max_length=100, blank=True)
    biotic_relationships = models.CharField(max_length=100, blank=True)
    cell_shape = models.CharField(max_length=50, blank=True)
    isolation = models.TextField(blank=True)
    habitat = models.TextField(blank=True)
    oxygen_requirement = models.CharField(max_length=100, blank=True)
    temperature_range = models.CharField(max_length=50, blank=True)
    relevance = models.TextField(blank=True)
    diseases = models.TextField(blank=True)
    energy_source = models.TextField(blank=True)
    child_levels = models.ManyToManyField('self', symmetrical=False, blank=True, related_name='parents')
    task = models.ForeignKey(BackgroundTask, blank=True, null=True)
    db_versions = models.ManyToManyField(PiliomeDBVersion, related_name='taxa')
    sequence_file = models.TextField(blank=True)
    sequence_file_type = models.CharField(max_length=15, blank=True)
    
    class Meta:
            verbose_name = 'Taxon'
            verbose_name_plural = 'Taxa'
            
    def get_ancestors(self):
        if self.parents.count():
            return [self.parents.get_query_set()[0]] + self.parents.get_query_set()[0].get_ancestors()
        else:
            return []
            
    def get_levels(self):
        taxon_levels = {x.level: x.name for x in self.get_ancestors()}
        taxon_levels['taxon_id'] = self.id
        return taxon_levels
        
    def _fetch_descendants(self):
        if self.child_levels.count():
            children = self.child_levels.get_query_set()
            return list(children) + [child.get_descendants() for child in children]
        else:
            return [self]
            
    def get_descendants(self):
        desc = []
        for d in self._fetch_descendants():
            if type(d) == list:
                for x in d:
                    desc.append(x)
            else:
                desc.append(d)
        return list(set(desc))
            
    def get_descendant_genomes(self):
        return [x for x in self.get_descendants() if x.with_genome == True]
    

class Scaffold(models.Model):
    scaffold_id = models.TextField(blank=True)
    scaffold_name = models.TextField()
    taxon_id = models.CharField(max_length=50)
    taxon_name = models.CharField(max_length=300, blank=True)
    topology = models.CharField(max_length=30)
    scaffold_type = models.CharField(max_length=30)
    sequence_length = models.IntegerField(blank=True, null=True)
    gc_content = models.FloatField(blank=True, null=True)
    gene_count = models.IntegerField(blank=True, null=True)
    rna_count = models.IntegerField(blank=True, null=True)
    img_add_date = models.CharField(max_length=15, blank=True)
    img_last_update = models.CharField(max_length=15, blank=True)
    task = models.ForeignKey(BackgroundTask)
    db_versions = models.ManyToManyField(PiliomeDBVersion, related_name='scaffolds')


class Gene(models.Model):
    gene_id = models.CharField(max_length=30, db_index=True)
    signal_peptide = models.CharField(max_length=5, blank=True)
    fused_gene = models.CharField(max_length=5, blank=True)
    locus_tag = models.TextField(blank=True)
    display_name = models.TextField(blank=True)
    kog = models.TextField(blank=True)
    transport_classification = models.TextField(blank=True)
    pfam = models.TextField()
    amino_acid_sequence_length = models.IntegerField(blank=True, null=True)
    transmembrane_helices = models.CharField(max_length=5, blank=True)
    scaffold_name = models.TextField()
    #taxon_name = models.CharField(max_length=300, blank=True)
    scaffold_id = models.TextField()
    gene_symbol = models.CharField(max_length=50, blank=True)
    img_orf_type = models.CharField(max_length=20, blank=True)
    seed = models.TextField(blank=True)
    external_links = models.TextField(blank=True)
    cog = models.TextField()
    taxon_id = models.CharField(max_length=50)
    dna_coordinates = models.TextField()
    start = models.IntegerField(null=True)
    end = models.IntegerField(null=True)
    strand = models.CharField(max_length=2)
    dna_length = models.IntegerField(null=True)
    product_name = models.TextField(blank=True)
    img_term = models.TextField(blank=True)
    swissprot_protein_product = models.CharField(max_length=100, blank=True)
    task = models.ForeignKey(BackgroundTask)
    fasta_description = models.TextField(blank=True)
    amino_acid_sequence = models.TextField(blank=True)
    piliome_annotation = models.CharField(max_length=200, blank=True, db_index=True)
    associated_usher = models.ForeignKey('self', blank=True, null=True)
    cu_cluster_fetch_count = models.IntegerField(blank=True, default=0)
    is_usher_fragment = models.NullBooleanField(null=True, blank=True)
    #cu_cluster_fetched = models.NullBooleanField(blank=True)
    cu_cluster_position = models.IntegerField(db_index=True) # Relative to the usher
    cluster_in_same_scaffold = models.BooleanField(default=True)
    cluster_coded_annotations = models.CharField(max_length=30, blank=True)
    db_versions = models.ManyToManyField(PiliomeDBVersion, related_name='genes')
    
    # Additional annotation
    extra_pfam_a = models.TextField(blank=True)
    extra_pfam_b = models.TextField(blank=True)
    extra_cog = models.TextField(blank=True)
    
    def get_cluster(self):
        if self.piliome_annotation == 'usher':
            cluster = Gene.objects.filter(Q(associated_usher__id=self.id) & Q(scaffold_name=self.scaffold_name))
            cluster = list(cluster) + list(Gene.objects.filter(id=self.id))
            cluster = [(int(x.gene_id), x) for x in cluster]
            cluster.sort(key=itemgetter(0))
            return [x[1] for x in cluster]
        else:
            return None
    
    def get_chaperones(self):
        cluster = self.get_cluster()
        if cluster:
            return [x for x in cluster if x.piliome_annotation == 'chaperone']
        else:
            return None
            
    def _get_nucleotide_sequence(self, formatted=True, force=False):
        gene = Gene.objects.filter(gene_id=self.gene_id)
        taxon_id = None
        for g in gene:
            taxon_id = g.taxon_id
        if taxon_id:
            taxon = Taxon.objects.filter(img_taxon_id=taxon_id)[0]
            if taxon.sequence_file:
                if taxon.sequence_file_type == 'fasta':
                    seq_file = SeqIO.index(taxon.sequence_file, taxon.sequence_file_type)
                    outseq = seq_file[self.gene_id]
                elif taxon.sequence_file_type == 'genbank':
                    gnuc = None
                    sca = Scaffold.objects.filter(scaffold_id=self.scaffold_id)
                    if sca:
                        seq_file = SeqIO.parse(taxon.sequence_file, taxon.sequence_file_type)
                        records = {x.name: x for x in seq_file}
                        record = records[sca[0].scaffold_name]
                        gnuc = None
                        for feature in record.features:
                            if feature.type == 'CDS':
                                if feature.qualifiers['note'][0].split('=')[-1] == self.gene_id:
                                    gnuc = feature.location.extract(record.seq)
                        if not gnuc:
                            sca = None
                    # If fetching through scaffold does not work, take the long and expensive way
                    if not sca:
                        records = SeqIO.parse(taxon.sequence_file, taxon.sequence_file_type)
                        for record in records:
                            for feature in record.features:
                                try:
                                    if feature.qualifiers['note'][0].split('=')[-1] == self.gene_id:
                                        gnuc = feature.location.extract(record.seq)
                                except KeyError:
                                    pass
                    if gnuc:
                        outseq = SeqRecord(gnuc, id=self.gene_id, description=self.fasta_description)
                    else:
                        outseq = None
                        raise Exception('%s - Nucleotide sequence could not be found' % self.gene_id)
            else:
                outseq = None
                raise Exception('%s - The associated taxon has no sequence file' % self.gene_id)
        else:
            outseq = None
            raise Exception('%s - No associated taxon ID' % self.gene_id)
            
        def final_outseq(seq):
            if formatted:
                return seq.format('fasta')
            else:
                return seq
            
        # Test agreement between nuc and prot, then return result
        if outseq:
            prot = SeqIO.read(StringIO(self.get_fasta_sequence()), 'fasta').seq
            with_first_aa = outseq.seq.translate().tostring().strip('*') == prot.tostring()
            without_first_aa = outseq.seq.translate().tostring().strip('*')[1:] == prot.tostring()[1:]
            frame2_translation = outseq.seq[1:].translate().tostring().strip('*') == prot.tostring()
            frame3_translation = outseq.seq[2:].translate().tostring().strip('*') == prot.tostring()
            prot_in_nuc = prot.tostring() in outseq.seq.translate().tostring().strip('*')
            nuc_in_prot = outseq.seq.translate().tostring().strip('*') in prot.tostring()
            premature_stop = outseq.seq.translate().tostring().strip('*').replace('*', 'X') in prot.tostring()
            if with_first_aa or without_first_aa:
                return final_outseq(outseq)
            elif frame2_translation:
                return final_outseq(outseq[1:])
            elif frame3_translation:
                return final_outseq(outseq[2:])
            elif prot_in_nuc: # or nuc_in_prot:
                seq_diff = abs(len(outseq.seq.tostring()) - (len(prot) * 3))
                if seq_diff < 3:
                    return final_outseq(outseq[:(-1*(seq_diff))])
                if force:
                    return outseq
                else:
                    raise Exception('%s - The nucleotide translation differ significantly in length with the amino acid sequence' % self.gene_id)
            elif premature_stop:
                return final_outseq(outseq)
            else:
                if force:
                    return outseq
                else:
                    raise Exception('%s - The nucleotide translation does not match the amino acid sequence' % self.gene_id)
            
    def get_fasta_sequence(self, fasta_id='gene_id', moltype='prot', indicate_usher=False):
        """
        moltype is either prot or nuc
        """
        if moltype == 'prot':
            if fasta_id == 'gene_id':
                fasta_id = self.gene_id
            elif fasta_id == 'fasta_description':
                fasta_id = self.fasta_description
            elif fasta_id == 'gene_object_id':
                fasta_id = self.id
            seq = self.amino_acid_sequence
            seq = seq.replace('-','').replace('*', '')
            if indicate_usher:
                outseq = '>%s usher:%s\n%s\n' % (fasta_id, self.associated_usher.gene_id, seq)
            else:
                outseq = '>%s\n%s\n' % (fasta_id, seq)
        elif moltype == 'nuc':
            outseq = self._get_nucleotide_sequence()
        return outseq
        
    def get_taxon(self):
        taxon = Taxon.objects.filter(img_taxon_id=self.taxon_id)[0]
        return taxon
        
    def get_taxon_details(self):
        taxon = Taxon.objects.filter(img_taxon_id=self.taxon_id)[0]
        return taxon.get_levels()
    

class UsherClade(models.Model):
    usherclade_id = models.CharField(max_length=100)
    name = models.CharField(max_length=30)
    sciphy_run_dir = models.TextField()
    notes = models.TextField(blank=True)


class SciPhyCluster(models.Model):
    cluster_number = models.IntegerField()
    minimum_overlap = models.IntegerField()
    is_monophyletic = models.BooleanField()
    members_count = models.IntegerField()
    members = models.TextField()
    members_removed = models.TextField(blank=True)
    members_added = models.TextField(blank=True)
    alignment = models.TextField()
    hmm_profile = models.TextField(blank=True)
    usher_clade = models.ForeignKey(UsherClade, null=True, blank=True)
    sciphy_run_dir = models.TextField()
    manually_checked = models.BooleanField(default=False)
    notes = models.TextField(blank=True)


class UsherHomologyGroup(models.Model):
    uhg_id = models.CharField(max_length=100)
    modified_cluster_number = models.IntegerField()
    name = models.CharField(max_length=30, blank=True)
    synonyms = models.TextField(blank=True)
    members = models.TextField()
    additional_redundant_members = models.TextField(blank=True)
    all_members = models.TextField(blank=True)
    sciphy_cluster_map = models.TextField() # Dictionary that maps each member to its corresponding sci_phy cluster
    clade = models.CharField(max_length=10, blank=True)
    subclade = models.CharField(max_length=10, blank=True)
    sciphy_run_dir = models.TextField()
    #adhesin_annotated = models.BooleanField(default=False)
    #adhesin_pfam_domains = models.TextField()
    hmm_profile = models.TextField(blank=True)
    taxonomic_distribution = models.TextField(blank=True)
    
    def get_members(self):
        members = eval(self.members) 
        if self.additional_redundant_members:
            members += eval(self.additional_redundant_members)
        return list(set(members))
        
    def get_gene_clusters(self):
        members = self.get_members()
        gene_clusters = []
        for m in members:
            gene = Gene.objects.filter(gene_id=m, associated_usher__isnull=True)[0]
            gene_clusters.append(gene.get_cluster())
        return gene_clusters
        
    def gene_clusters_generator(self):
        members = self.get_members()
        gene_clusters = []
        for m in members:
            gene = Gene.objects.filter(gene_id=m, associated_usher__isnull=True)[0]
            yield gene.get_cluster()

    def get_adhesins(self):
        gene_clusters = self.get_gene_clusters()
        adhesins = []
        for cluster in gene_clusters:
            for m in cluster:
                if m.piliome_annotation == 'adhesin':
                    adhesins.append(m)
        return adhesins 
        
    def get_adhesin_ids(self):
        gene_clusters = self.get_gene_clusters()
        adhesins = []
        for cluster in gene_clusters:
            for m in cluster:
                if m.piliome_annotation == 'adhesin':
                    adhesins.append(str(m.gene_id))
        return adhesins
    
    
class ReferenceStructure(models.Model):
    pdb_id = models.CharField(max_length=10)
    description = models.TextField(blank=True)
    
    def __unicode__(self):
        return self.pdb_id
    

class ReferenceSequence(models.Model):
    accession = models.CharField(max_length=50)
    fasta_sequence = models.TextField()
    sequence_length = models.IntegerField()
    sequence_moltype = models.CharField(max_length=10)
    reference_structure = models.ForeignKey(ReferenceStructure, null=True, blank=True)
    #annotation = models.CharField(max_length=50, blank=True)
    #source_db = models.CharField(max_length=50, blank=True)
    
    def __unicode__(self):
        return self.accession
    

class ReferencePaper(models.Model):
    pubmed_id = models.CharField(max_length=50, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)
    authors = models.TextField()
    title = models.TextField()
    abstract = models.TextField()
    journal = models.TextField()
    volume = models.CharField(max_length=100)
    issue = models.CharField(max_length=10)
    pages = models.CharField(max_length=50)
    year = models.CharField(max_length=10)
    sequences = models.ManyToManyField(ReferenceSequence, blank=True)
    structures = models.ManyToManyField(ReferenceStructure, blank=True)
    linked_to_uhg = models.BooleanField(default=False)
    
    def citation(self):
        fields = (', '.join(eval(self.authors)), self.title, self.journal, self.year, self.volume, self.issue, self.pages)
        return '%s. %s %s (%s) %s[%s]:%s.' % fields
        
    def __unicode__(self):
        return self.pubmed_id


class ReferenceLink(models.Model):
    reference_paper = models.ForeignKey(ReferencePaper)
    reference_sequence = models.ForeignKey(ReferenceSequence)
    matched_gene = models.ForeignKey(Gene)
    search_method = models.CharField(max_length=30)
    alignment_length = models.IntegerField()
    identity = models.FloatField()
    uhg = models.ForeignKey(UsherHomologyGroup, related_name='reference_links')
    notes = models.TextField(blank=True)
    
    
class NonRedundantUsher(models.Model):
    representative = models.ForeignKey(Gene) # Representative usher
    members = models.ManyToManyField(Gene, related_name='nrusher_cluster')
    # The alignment representative is the representative selected by CD-HIT
    # when it is run to reduce redundancy after gappyout trimming with TrimAl
    #gene_name = ''
    alignment_representative = models.ForeignKey('self', null=True, blank=True)
    alignment_sequence = models.TextField()
    task = models.ForeignKey(BackgroundTask)
    sciphy_cluster = models.ForeignKey(SciPhyCluster, null=True, blank=True)
    #usher_type = models.ForeignKey(UsherType, null=True, blank=True)
    #usher_clade = models.ForeignKey(UsherClade, null=True, blank=True)
    db_versions = models.ManyToManyField(PiliomeDBVersion, related_name='non_redundant_ushers') 
    
    
class LiteratureSearched(models.Model):
    pubmed_id = models.CharField(max_length=50)
    proteins = models.TextField(blank=True)
    structures = models.TextField(blank=True)
    searched_against = models.TextField(blank=True)
    #refetched = models.BooleanField(default=False)
    #matched_from_primers = models.BooleanField(default=False)
    #forward_primer = models.TextField(blank=True)
    #reverse_primer = models.
