from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.cache import SimpleCache
from data.models import Gene, Taxon, Scaffold
from tastypie.serializers import Serializer
#from chapusher.models import Usher


class TaxonResource(ModelResource):

    class Meta:
        queryset = Taxon.objects.all()
        resource_name = 'taxon'
        filtering = {'img_taxon_id': ALL}
        cache = SimpleCache(timeout=30)


class ScaffoldResource(ModelResource):

    #taxon = fields.ForeignKey(TaxonResource, 'taxon')

    class Meta:
        queryset = Scaffold.objects.all()
        resource_name = 'scaffold'
        filtering = {'scaffold_name': ALL}
        cache = SimpleCache(timeout=30)


class GeneResource(ModelResource):

    associated_usher = fields.ForeignKey('self', 'associated_usher', null=True)

    class Meta:
        queryset = Gene.objects.all()
        allowed_methods = ['get', 'post', 'put', 'patch']
        resource_name = 'gene'
        filtering = {'id': ALL,
                     'gene_id': ALL,
                     'img_orf_type': ALL,
                     'start': ALL,
                     'end': ALL,
                     'dna_length': ALL,
                     'strand': ALL,
                     'scaffold_name': ALL,
                     'associated_usher': ALL_WITH_RELATIONS}
        limit = 100
        cache = SimpleCache(timeout=30)
        serializer = Serializer(formats=['json'])
        authorization = Authorization()