from django.conf import settings
from joblib import Parallel, delayed
from data.models import Gene, UsherHomologyGroup
import time
import redis


# Establish the connection to the redis db
redis_conn = settings.REDIS_CONNECTION_POOL
r = redis.Redis(connection_pool=redis_conn)

# Fetch all the annotated genes
annotated_genes = Gene.objects.all().exclude(piliome_annotation='')


def save_sequence(gene, moltype='prot'):
    try:
        # Save the nucleotide sequence
        nuc_seq = gene.get_fasta_sequence(moltype=moltype)
        set_value = r.set('sequence__%s__prot' % gene.gene_id, nuc_seq)
    except Exception, e:
        print e


uhgs = UsherHomologyGroup.objects.all()

# Save gene IDs
def save_gene_ids(uhg):
    gene_clusters = uhg.gene_clusters_generator()
    annotations = ['usher', 'chaperone', 'adhesin']
    for annotation in annotations:
        r.delete('uhg%s_%ss' % (uhg.id, annotation))
    for cluster in gene_clusters:
        for m in cluster:
            if m.piliome_annotation:
                annot = m.piliome_annotation
                for annotation in annotations:
                    r.rpush('uhg%s_%ss' % (uhg.id, annotation), m.gene_id)
                    
                    
def joblib_save_sequences():    
    Parallel(n_jobs=2, backend='threading', verbose=5)(delayed(save_sequence)(gene) for gene in annotated_genes)
    
def joblib_save_gene_ids():
    Parallel(n_jobs=2, backend='threading', verbose=5)(delayed(save_gene_ids)(uhg) for uhg in uhgs)