from data.models import ReferencePaper, ReferenceSequence, ReferenceStructure, \
    ReferenceLink, UsherHomologyGroup, ReferenceLink, Gene, LiteratureSearched
from data.utils.pdb_fetcher import search_pdb_structures
from builder.utils.blast import BlastP
from bs4 import BeautifulSoup
from xml.dom import minidom
from django.conf import settings
from Bio import Entrez, SeqIO
import requests
import urllib2
import time
import os


Entrez.email = "joemar.ct@gmail.com"


class FetchPubmed(object):
    
    def __init__(self, pubmed_id, proteins=None, structures=None, uhg=None, match=None):
        self.id = pubmed_id
        self.match = match
        self.proteins = proteins
        self.structures = structures
        self.uhg = uhg
        self.fetch_pubmed_xml()
        self.get_pubmed_article()
        self.get_details()
    
    def fetch_pubmed_xml(self):
        base_url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils"
        query = "/efetch.fcgi?db=pubmed&id=%s&retmode=xml" % self.id
        result = urllib2.urlopen(base_url+query)
        if result.getcode() == 200:
            content = result.read()
            self.xml = content
        
    def get_pubmed_article(self):
        dom = minidom.parseString(self.xml)
        set = dom.getElementsByTagName('PubmedArticleSet')[0]
        pub_article = set.getElementsByTagName('PubmedArticle')[0]
        med_citation = pub_article.getElementsByTagName('MedlineCitation')[0]
        self.pubmed_article = med_citation.getElementsByTagName('Article')
    
    def get_details(self):
        self.journal_node = self.pubmed_article[0].getElementsByTagName('Journal')
        try:
            iso_abbr = self.journal_node[0].getElementsByTagName('ISOAbbreviation')
            self.journal = iso_abbr[0].childNodes[0].data
        except IndexError:
            title = self.journal_node[0].getElementsByTagName('Title')
            self.journal = title[0].childNodes[0].data
        journal_issue = self.journal_node[0].getElementsByTagName('JournalIssue')
        try:
            self.volume = journal_issue[0].getElementsByTagName('Volume')[0].childNodes[0].data
        except IndexError:
            self.volume = ""
        try:
            self.issue = journal_issue[0].getElementsByTagName('Issue')[0].childNodes[0].data
        except IndexError:
            self.issue = ""
        pub_date = journal_issue[0].getElementsByTagName('PubDate')
        self.pub_date = pub_date[0]
        try:
            self.year = pub_date[0].getElementsByTagName('Year')[0].childNodes[0].data
        except IndexError:
            self.year = pub_date[0].getElementsByTagName('MedlineDate')[0].childNodes[0].data.split(' ')[0]
        self.title = self.pubmed_article[0].getElementsByTagName('ArticleTitle')[0].childNodes[0].data
        try:
            pagination = self.pubmed_article[0].getElementsByTagName('Pagination')
            self.pages = pagination[0].getElementsByTagName('MedlinePgn')[0].childNodes[0].data
        except IndexError:
            self.pages = ""
        authors = []
        authorlist = self.pubmed_article[0].getElementsByTagName('AuthorList')[0]
        all_authors = authorlist.getElementsByTagName('Author')
        for author in all_authors:
             try:
                 last_name = author.getElementsByTagName('LastName')[0].childNodes[0].data
             except IndexError:
                 try:
                     last_name = author.getElementsByTagName('CollectiveName')[0].childNodes[0].data
                 except IndexError:
                     last_name = ""
             try:
                 initials = author.getElementsByTagName('Initials')[0].childNodes[0].data
             except IndexError:
                 initials = ""
             authors.append(("%s %s" % (last_name.strip(), initials.strip())).encode('utf-8').strip())
        self.authors = authors
        try:
            abstract_node = self.pubmed_article[0].getElementsByTagName('Abstract')
            abstract_texts = abstract_node[0].getElementsByTagName('AbstractText')
            if len(abstract_texts) > 1:
                abstract = []
                for text in abstract_texts:
                    abstract.append(text.childNodes[0].data)
                self.abstract = " ".join(abstract)
            else:
                self.abstract = abstract_node[0].getElementsByTagName('AbstractText')[0].childNodes[0].data
        except IndexError:
            self.abstract = ""
            
    def save_associated_data(self):
        # Save the proteins and/or structures
        if self.proteins:
            for prot in self.proteins:
                print prot['accession']
                pc = ReferenceSequence.objects.filter(accession=prot['accession'])
                if pc:
                    self.reference_protein = pc[0]
                else:
                    self.reference_protein = ReferenceSequence(
                        accession=prot['accession'],
                        fasta_sequence=prot['sequence'],
                        sequence_length=prot['length'],
                        sequence_moltype='prot'
                    )
                    self.reference_protein.save()
                    self.ref.sequences.add(self.reference_protein)
                    self.ref.save()
        """
        if self.structures:
            for struct in self.structures:
                sc = ReferenceStructure.objects.filter(accession=prot['accession'])
                if not sc:
                refstruct = ReferenceStructure(pdb_id=struct)
                refstruct.save()
                self.ref.structures.add(refstruct)
            self.ref.save()
        """
        # Save the associated uhg
        if self.uhg:
            uhg = UsherHomologyGroup.objects.get(id=self.uhg)
            if 'usher:' in self.match['alignment'].title:
                match_acc = self.match['alignment'].title.split()[-2]
            else:
                match_acc = self.match['alignment'].title.split()[-1]
            hsp = self.match['hsp']
            reflink = ReferenceLink(
                reference_paper=self.ref, 
                reference_sequence=self.reference_protein,
                uhg=uhg, 
                search_method='blastp',
                matched_gene = Gene.objects.filter(gene_id=match_acc)[0],
                alignment_length=hsp.align_length,
                identity=round(float(hsp.identities)/hsp.align_length, 3) * 100
            )
            reflink.save()
            self.ref.linked_to_uhg = True
            self.ref.save()
            
    def save_to_database(self):
        ref_check = ReferencePaper.objects.filter(pubmed_id=self.id)
        if ref_check.count():
            self.ref = ref_check[0]
            self.save_associated_data()
            return ('exists', 'This reference already exists in the database.')
        else:
            # Create reference
            self.ref = ReferencePaper(pubmed_id=self.id,
                        title=self.title,
                        authors=self.authors,
                        abstract=self.abstract,
                        journal=self.journal,
                        volume=self.volume,
                        issue=self.issue,
                        pages=self.pages,
                        year=self.year)
            self.ref.save()
            self.save_associated_data()
            return ('saved', 'This reference has been saved.')
                

class SearchPubmed(object):
    
    def __init__(self, mesh_term, fetch_locally=False, local_seq_db=None):
        self.mesh_term = mesh_term
        self.local_seq_db = local_seq_db
        self.fetch_locally = fetch_locally
        self.id_list = []
        self.fetch_pmid_list()
        self.get_new_ids()
        all_prots_seq = os.path.join(settings.PILIOMEDB_FILES, 'literature_mining', 'all_associated_proteins.fasta')
        self.all_prots = SeqIO.index(all_prots_seq, 'fasta')
        self.seq_store_keys = [x.split('|')[1] for x in list(self.all_prots.keys())]
        if fetch_locally:
            self.accs_map = {}
            for k in self.all_prots:
                prot_id = k.split('|')[1]
                self.accs_map[prot_id] = k
        else:
            self.seq_store = open(all_prots_seq, 'a')

    def fetch_pmid_list(self):
        if self.mesh_term:
            print 'Fetching Pubmed IDs for MeSH term: %s' % self.mesh_term
            all_pmid = 0 # this will be replaced upon first fetch
            pmid_fetched = None
            while pmid_fetched != all_pmid:
                #print pmid_fetched, all_pmid
                handle = Entrez.esearch(db='pubmed', term='%s[MeSH]' % self.mesh_term, retmax=100, retstart=pmid_fetched)
                record = Entrez.read(handle)
                handle.close()
                all_pmid = int(record['Count'])
                self.id_list += record['IdList']
                if pmid_fetched is None:
                    pmid_fetched = len(record['IdList'])
                else:
                    pmid_fetched += len(record['IdList'])
            print 'A total of %s Pubmed IDs were fetched.' % len(self.id_list)
        else:
            print 'Fetching Pubmed IDs from local database not searched against the db...'
            all_lits = LiteratureSearched.objects.all().exclude(searched_against__contains=self.local_seq_db)
            self.id_list = [x.pubmed_id for x in all_lits if x.proteins]
            print 'A total of %s Pubmed IDs were fetched.' % len(self.id_list)
            self.new_ids = self.id_list
            
    def get_new_ids(self):
        if self.mesh_term:
            self.new_ids = []
            for pmid in self.id_list:
                pid = LiteratureSearched.objects.filter(pubmed_id=pmid)
                if not pid:
                    self.new_ids.append(pmid)
            print 'There are %s new IDs.' % len(self.new_ids)

    def parse_html(self, html):
        soup = BeautifulSoup(html)
        result_links = soup.find_all('div', attrs={'class': 'rprt'})
        prots = []
        structs = []
        if result_links:
            for res in result_links:
                for l in res.select('a'):
                    if l.get('class'):
                        if l.get('class')[0] == 'dblinks':
                            acc = l.get('href').split('from_uid=')[-1]
                            if acc not in prots:
                                prots.append(acc)
                    else:
                        acc = l.get('href').split('/')[-1]
                        if '_' in acc and len(acc.split('_')[0]) == 4:
                            if acc not in structs:
                                structs.append(acc)
        return (prots, structs)

    def get_protein_accessions(self, pubmed_id):
        url = 'http://www.ncbi.nlm.nih.gov/protein?LinkName=pubmed_protein&from_uid=%s' % pubmed_id
        req = requests.get(url)
        html = req.content
        prots, structs = self.parse_html(html)
        if not prots:
            url = 'http://www.ncbi.nlm.nih.gov/protein?LinkName=pubmed_protein_refseq&from_uid=%s' % pubmed_id
            req = requests.get(url)
            if req.url != url and '/protein/' in req.url:
                prots.append(req.url.split('/')[-1])
            else:
                html = req.content
                prots, structs = self.parse_html(html)
        data = {}
        if prots:
            data['proteins'] = prots
        if structs:
            data['structures'] = structs
        self.request = req
        return data
            
    def search_and_save(self):
        for i, pmid in enumerate(self.new_ids):
            print '\n', i+1, len(self.new_ids), '--', pmid
            pid = LiteratureSearched.objects.filter(pubmed_id=pmid)
            if pid:
                pid = pid[0]
                data = {}
                if pid.proteins:
                    data['proteins'] = eval(pid.proteins)
            else:
                print '  Fetching associated proteins and/or structures...'
                data = self.get_protein_accessions(pmid)
                pid = LiteratureSearched(pubmed_id=pmid)
                pid.save()
            if data:
                if data['proteins']:
                    pid.proteins = str(data['proteins'])
                    pid.save()
                if data['proteins']:
                    print '  Found %s associated proteins.' % len(data['proteins'])
                    for j, prot in enumerate(data['proteins']):
                        print '  %s/%s - dealing with %s' % (j+1, len(data['proteins']), prot)
                        print '    Fetching sequence...'
                        if self.fetch_locally:
                            try:
                                seq = self.all_prots[self.accs_map[prot]].format('fasta')
                            except KeyError:
                                seq = Entrez.efetch(db='protein', id=prot, rettype='fasta')
                                seq = seq.read()
                        else:
                            seq = Entrez.efetch(db='protein', id=prot, rettype='fasta')
                            seq = seq.read()
                            if prot not in self.seq_store_keys:
                                self.seq_store.write(seq)
                                self.seq_store_keys.append(prot)
                        print '    Running blastp against local database...'
                        p = BlastP(seq, db=self.local_seq_db, fetcher=FetchPubmed)
                        if p.results:
                            print '    Saving hits to database...'
                            save_op = p.save_to_database(verbose=True)
                            if pid.searched_against:
                                sa = eval(pid.searched_against)
                                if self.local_seq_db not in sa:
                                    sa.append(self.local_seq_db)
                            else:
                                sa = [self.local_seq_db]
                            pid.searched_against = str(sa)
                            pid.save()
        if not self.fetch_locally:
            self.seq_store.close()
        # Run the function to search and save PDB structures
        search_pdb_structures()
                            
    def remove_ids_from_db(self):
        ls = LiteratureSearched.objects.filter(pubmed_id__in=self.id_list)
        ls.delete()
        
    def adhoc_fetch(self):
        found_count = 0
        lits = LiteratureSearched.objects.all()
        prots = [x for x in lits if not x.proteins and not x.refetched]
        for i, lit in enumerate(prots):
            print i+1, len(prots), '--', lit.pubmed_id, 'found:', found_count 
            if not lit.proteins:
                while True:
                    try:
                        d = self.get_protein_accessions(lit.pubmed_id)
                        if d:
                            lit.proteins = str(d['proteins'])
                            lit.save()
                            found_count += 1
                            print '  FOUND!'
                    except:
                        print '  Error: Retrying in 10s...'
                        time.sleep(10)
                        continue
                    break
            lit.refetched = True
            lit.save()