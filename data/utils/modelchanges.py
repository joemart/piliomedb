from data.models import Gene, Taxon, PiliomeDBVersion, UsherHomologyGroup
from joblib import Parallel, delayed
from django.db.models import Q
import gc


def transfer_taxa_to_taxa2():
    dbv = PiliomeDBVersion.objects.filter(db_version='0.1')[0]
    taxa = Taxon.objects.all()
    for taxon in taxa:
        """
        parent = taxon.parents.get_query_set()
        if parent:
            if not Taxon2.objects.filter(taxon1_id=parent[0].id).exists():
                ptxd = parent[0].__dict__
                del ptxd['_state']
                ptxd['taxon1_id'] = ptxd['id'] 
                del ptxd['id']
                t2_parent = Taxon2(**ptxd)
                t2_parent.save()
            else:
                t2_parent = Taxon2.objects.filter(taxon1_id=parent[0].id)[0]
        """
        txd = taxon.__dict__
        del txd['_state']
        txd['taxon1_id'] = txd['id'] 
        del txd['id']
        t2 = Taxon2(**txd)
        t2.save()
        t2.db_versions.add(dbv)
        t2.save()
    print 'Finished!'    
                

def _queryset_iterator(queryset, chunksize=1000):
    '''''
    Iterate over a Django Queryset ordered by the primary key

    This method loads a maximum of chunksize (default: 1000) rows in it's
    memory at the same time while django normally would load all rows in it's
    memory. Using the iterator() method only causes it to not preload all the
    classes.

    Note that the implementation of the iterator does not support ordered query sets.
    '''
    pk = 0
    last_pk = queryset.order_by('-pk')[0].pk
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            yield row
        gc.collect()
                

def populate_gene_coord_fields():
    qset = Gene.objects.all()
    genes = _queryset_iterator(qset)
    gcount = 0
    for gene in genes:
        gcount += 1
        print '%s/%s - dealing with %s' % (gcount, qset.count(), gene.gene_id)
        dna_coords = gene.dna_coordinates
        if dna_coords:
            start, end = dna_coords.split()[0].split('..')
            if '(-)' in dna_coords:
                strand = '-'
                dna_length = dna_coords.split('(-)(')[1].split('bp')[0]
            elif '(+)' in dna_coords:
                strand = '+'
                dna_length = dna_coords.split('(+)(')[1].split('bp')[0]
            gene.start = int(start)
            gene.end = int(end)
            gene.dna_length = int(dna_length)
            gene.strand = str(strand)
            gene.save()
    print '****** Done! ******'
    

########################################################################

""" 
Embarassingly parallel loop version using joblib. Executed as follows:

from data.utils.modelchanges import all_genes, _populate
Parallel(n_jobs=4, backend='threading', verbose=5)(delayed(_populate)(gene) for gene in all_genes)
"""

queryset = Gene.objects.filter(Q(start__isnull=True) | Q(end__isnull=True) | Q(dna_length__isnull=True) | Q(strand=''))
all_genes = _queryset_iterator(queryset)

def _populate(gene):
    dna_coords = gene.dna_coordinates
    if dna_coords:
        if not gene.start or not gene.end or not gene.strand or not gene.dna_length:
            start, end = dna_coords.split()[0].split('..')
            if '(-)' in dna_coords:
                strand = '-'
                dna_length = dna_coords.split('(-)(')[1].split('bp')[0]
            elif '(+)' in dna_coords:
                strand = '+'
                dna_length = dna_coords.split('(+)(')[1].split('bp')[0]
            gene.start = int(start)
            gene.end = int(end)
            gene.dna_length = int(dna_length)
            gene.strand = str(strand)
            gene.save()      
            
########################################################################   
         
            
def correct_usher_strands(correction_type=None):
    ushers = Gene.objects.filter(piliome_annotation='usher')
    usher_ids = [str(x.gene_id) for x in ushers]
    
    if correction_type == 1:
        # This is the case where another gene with the same gene_id is
        # present in the database but associated with other ushers.
        # The correction is simply to follow those other genes, which
        # are presumably containing the correct DNA coordinates.          
        i = 0   
        needs_correction = []
        for x in usher_ids:
            us = ushers.filter(gene_id=x)
            strands = set([x.strand for x in us])
            if us.count() > 1 and len(strands) > 1: 
                mu = us.filter(associated_usher__isnull=True)[0]
                correct_dna_length = mu.end - mu.start
                if correct_dna_length != mu.dna_length:
                    i += 1
                    print i, mu.dna_length, [(x.gene_id, x.dna_length, x.strand) for x in us] 
                    needs_correction.append(mu.gene_id)
                    others = us.exclude(associated_usher__isnull=True)
                    mu.dna_coordinates = others[0].dna_coordinates
                    mu.strand = others[0].strand
                    mu.start = others[0].start
                    mu.end = others[0].end
                    mu.dna_length = others[0].dna_length
                    mu.save()
        #return ','.join(needs_correction
        
    elif correction_type == 2:
        # This the case where the gene_id is unique. Correction is deemed necessary
        # because the strand of this usher is different from its two adjacent genes.
        i = 0
        needs_correction = []
        for x in usher_ids:
            adj = [str(a) for a in [int(x) - 1, int(x) + 1]]
            adj_genes = Gene.objects.filter(gene_id__in=adj)
            adj_strands = [a.strand for a in adj_genes]
            mu = ushers.filter(gene_id=x)[0]
            if len(set(adj_strands)) == 1 and adj_strands[0] != mu.strand:
                i += 1
                computed_length = int(mu.end - mu.start)
                print i, x, mu.dna_length, mu.strand, computed_length, adj_strands[0]
                mu.dna_length = computed_length
                mu.strand = adj_strands[0]
                mu.save()
                
    else:
        print 'Please specify the correction type.'
        
        
def update_adhesin_pfam_domains(major_update=True):
    uhgs = UsherHomologyGroup.objects.all()
    if not major_update:
        uhgs = uhgs.filter(adhesin_pfam_domains='[]')
    else:
        for uhg in uhgs:
            uhg.adhesin_pfam_domains = ''
            uhg.save()
    for i, uhg in enumerate(uhgs):
        print '%s/%s - Dealing with %s' % (i+1, uhgs.count(), uhg.id)
        adhesins = uhg.get_adhesins()
        domains = []
        for adh in adhesins:
            if adh.pfam:
                pfam_a1 = adh.pfam.split()[0].replace('pfam', 'PF')
                if pfam_a1 not in domains:
                    domains.append(pfam_a1)
            if adh.extra_pfam_a:
                for pf in eval(adh.extra_pfam_a):
                    pfam_a2 = pf.split('.')[0]
                    if pfam_a2 not in domains:
                        domains.append(pfam_a2)
            if adh.extra_pfam_b:
                for pf in eval(adh.extra_pfam_b):
                    pfam_b = pf.split('.')[0]
                    if pfam_b not in domains:
                        domains.append(pfam_b)
        if adhesins and not domains:
            domains = ['No domains']
        print '\t', domains
        uhg.adhesin_pfam_domains = str(domains)
        uhg.save()
            