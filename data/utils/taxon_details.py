from data.models import Gene, Taxon, UsherHomologyGroup
import json


def get_taxon_distribution(gene_ids_list):
    
    genes = Gene.objects.filter(gene_id__in=gene_ids_list)
    data = {}
    for gene in genes:
        for anc in gene.get_taxon().get_ancestors():
            txid = int(anc.id)
            if txid in data.keys():
                data[txid]['count'] += 1
            else:
                data[txid] = {'count': 1}
                
    def get_tree(node, data):
        
        tree = {}
        #if int(node.id) in data.keys():
        tree['name'] = node.name
        try:
            tree['size'] = data[int(node.id)]['count']
        except KeyError:
            tree['size'] = 0
        if int(node.id) in data.keys() and node.child_levels.get_query_set()[0].level != 'strain':
            tree['children'] = []
            children = node.child_levels.get_query_set()
            for child in children:
                if int(child.id) in data.keys():
                    tree['children'].append(get_tree(child, data))
                    
        return tree
        
    bact = Taxon.objects.get(id=1)
    tree = get_tree(bact, data)
                
    return tree
    
    
def load_uhg_taxon_dist():
    
    uhgs = UsherHomologyGroup.objects.all()
    for i, uhg in enumerate(uhgs):
        print i+1, uhgs.count()
        members = uhg.get_members()
        tree = get_taxon_distribution(members)
        uhg.taxonomic_distribution = json.dumps(tree)
        uhg.save()