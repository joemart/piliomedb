from data.models import ReferenceLink, ReferenceStructure
from Bio import SeqIO
from cStringIO import StringIO
from django.conf import settings
import requests
import re, os


def search_pdb_structures():
    links = ReferenceLink.objects.all()
    for link in links:
        s = StringIO(link.reference_sequence.fasta_sequence)
        seq = SeqIO.read(s, 'fasta')
        if '|pdb|' in seq.id:
            pdb_desc = re.sub('gi(.*)Chain (.), ', '', seq.description)
            pdb_id = seq.id.split('pdb|')[-1].split('|')[0]
            refstruct = ReferenceStructure.objects.filter(pdb_id=pdb_id)
            if not refstruct:
                # Save to database
                refstruct = ReferenceStructure(pdb_id=pdb_id, description=pdb_desc)
                refstruct.save()
                ref_seq = link.reference_sequence
                ref_seq.reference_structure = refstruct
                ref_seq.save()
                # Fetch image from PDB
                url = 'http://www.pdb.org/pdb/images/%s_asr_r_500.jpg' % pdb_id
                img_save_path = os.path.join(settings.BASE_DIR, 'interface', 'static', 'images', '%s.jpg' % pdb_id)
                req = requests.get(url)
                content = req.content
                img = open(img_save_path, 'wb')
                img.write(content)
                img.close()
    print 'Completed!'