## PiliomeDB: The chaperone-usher pili database

PiliomeDB serves as the repository of all genomic and experimental data ever generated about the chaperone-usher systems in bacteria. It implements a dynamic classification scheme of CU pilus types and their adhesin subtypes.

### Features

* Searching for usher types by ID, keyword, FASTA sequence, and PDB structure
* Online curation tools accessible to registered users (a.k.a. curators)
* Each CU type shows an HMM profile, multiple sequence alignment, phylogenetic tree, and taxonomic distribution
* Display of related papers and structures per CU type
* Ability to download data per CU type or for all CU types
* Fine-grained control of data download and retrieval
* Ability to download the source code and database to run the web application locally


### CU Pili Classification Scheme

The CU pili classifications will be generated a custom semi-automated pipeline. The workflow for generating the automated CU pili type classification will be as follows:

1. Grouping of redudant sequences using CD-HIT at -c 1.0 setting
	Input: Combined FASTA file of all ushers
	Output: FASTA file of non-redundant ushers and file specifying the groupings

2. Identification of split ushers
	Input: Non-redundant ushers
	Output: Identification of full-length non-redundant ushers and which ushers are fragments of splits

3. Saving the non-redundant ushers into the database
	Input: The output from CD-HIT
	Output: Populated NonRedundantUsher database model

4. Multiple sequence alignment of the non-redundant ushers using MUSCLE
	Input: Combined FASTA file of the non-redundant ushers
	Output: Aligned FASTA file of the non-redundant ushers
	
5. Automated trimming of the alignment
	Input: Aligned FASTA file of the non-redundant ushers
	Output: Trimmed version of the aligned FASTA file

6. (Iterative) Classification into CU types using SciPhy
	Input: Trimmed alignment file
	Output: SciPhy output files that identify the subfamilies (or types)
	
7. (Iterative) Benchmarking against known closely-related pilus types
	Input: SciPhy output subfamilies and representative usher types
	Output: Classification scheme that discriminates the benchmarked ushers

8. Saving the CU types into the database
	Input: SciPhy output files of the run that passed the benchmarking
	Output: Populated CUPilusType database model
	
9. Saving the typed ushers into the database
	Input: SciPhy output files of the run that passed the benchmarking
	Output: Updated NonRedundantUsher database model noting which one were included in the typing

10. Conversion to phylip format and generation of the phylogenetic tree using FastTree
	Input: Trimmed alignment file
	Output: Phylogenetic tree in newick format
	
11. Visualization of CU pilus types superimposed into the phylogenetic tree
	Input: Phylogenetic tree and the CUPilusType data
	Output: PDF or SVG file of the tree with the CU types highlighted
	
12. Generation and visualization of CU types statistics
	Input: CUPilusType, Taxon, Usher, and NonRedundantUsher data
	Output: Tables and figures presenting and summarizing the statistics of the CU types


### Manual Curation

The attribution of published data to specific pilus types cannot be done in a fully automated way. There is going to be a huge effort among the curators to improve the data that will be associated with each pilus type. Each associated data will be presented as panel on the main column or on the sidebars in a CU pilus type page. For instance, one panel should present all the structures that have been published in RCSB PDB. Another panel will present all the papers that have been published about the pilus type.

The linked information per CU pilus type will be of the following categories, some of which are to be generated automatically from the genomic data: (A = automatic, M = manual)

1. Gene clusters (A + M)
2. HMM profile logo representation (A + M)
3. Multiple sequence alignment (A + M)
4. Taxonomic distribution (A + M)
5. Adhesins and adhesin types (A + M)
6. Serotypes (M)
7. Ligands and inhibitors (A + M) 
8. Structures (A + M)
9. Disease-associations (M)
10. Studies (A + M)
11. Keywords (A + M)

PiliomeDB should enable the submission and review of data for each of these categories. The curation work can be centered around submission of papers published in PubMed. All the related information can be then be automatically fetched from PubMed and PDB but can also be manually refined, if necessary.

Each submitted paper entry is subjected to a review. All of the submitted entries (both approved and under review) will be displayed in the account dashboard of every curator. Every submitted entry opens a discussion thread, in case it is necessary prior to approval. Each approved entry should have a feedback system so that other curators can suggest revisions. For each revision received, a discussion thread is opened. The curators involved should both agree to mark the revision as resolved when agreement is reached. Resolution of a submitted revision can either lead to actual revision or complete removal of the entry.

Furthermore, it should be possible to edit an approved entry and resubmit it for review.


### Version System

All data models (both automated and manual) should be appropriately linked to a PiliomeDB version. The release of a version will be based on the need to revise the classification scheme -- either to include a new type or merge/split existing types.

All data models have a field called db_versions. This field is a ManyToManyField with reference to PiliomeDBVersion. All data will be initially loaded to version 0.1, which is the very first development version. The first release version is going to be version 1.0. All data that do not change in the following version are automatically carried over. Some data will be new to a particular version.

Some changes can happen without changing the version number. Examples are additions in the associated info regarding pilus types resulting from newly added papers. The papers also take db_version values based on the version of the database when the papers are added.

The version system should enable one to easily present the contents in different versions. One should be able to smoothly switch the database from one version to another but the latest version is displayed by default.