from fabric.api import env, run, parallel, sudo, put
from fabric.context_managers import settings as _settings
from boto.ec2.connection import EC2Connection
from fabric.contrib.project import rsync_project
import boto


env.warn_only = True
env.key_filename = ['piliomedb.pem']
proj_path = '/home/ubuntu/piliomedb'

AWS_ACCESS_KEY_ID = 'AKIAITOOLPVD6DTGZV3Q'
AWS_SECRET_ACCESS_KEY = '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'
ec2conn = EC2Connection(AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY)


def set_host(tag=None):
    fs = {'instance-state-name': 'running'}
    fs['tag:%s' % tag.keys()[0]] = tag.values()[0]
    instances = ec2conn.get_only_instances(filters=fs)
    host = instances[0]
    return 'ubuntu@' + str(host.ip_address)
                    
env.hosts = [set_host(tag={'Name': 'piliomedb'})]


# DB parameters
dbuser = 'root'
dbpass = 'toor'
dbname = 'piliomedb'


def uname():
    run('uname -a')


def shell():
    run('/bin/bash')
    
def sync():
    local_dir = '.'
    remote_dir = '/home/ubuntu/piliomedb'
    rsync_project(local_dir=local_dir, remote_dir=remote_dir, delete=True, exclude=['*.pyc','.DS_Store', 'venv/', '.git'])

    
def apt_get_install(*packages):
    sudo('apt-get -y --no-upgrade install %s' % ' '.join(packages), shell=False)


def transfer_files():
    # Transfer the archived project files
    print 'Transferring archived project files...'
    localpath = 'Archive.zip'
    remotepath = '/home/Archive.zip'
    put(localpath, remotepath, use_sudo=True)
    print 'Transfer completed'
    print 'Removing existing project files...'
    run('rm -rf /home/piliomedb')
    # Install unzip to unpack the project files archive
    print 'Unpacking project files...'
    apt_get_install('unzip')
    run('unzip /home/Archive.zip -d /home/piliomedb && rm /home/Archive.zip')
    print 'Project files unpacked'


def upload_file(f, remote_project_path='/home/piliomedb'):
    pass


def install_piliomedb():
    ''' Installs the PiliomeDB web app '''
    
    print 'Installation begins...'
    
    # Upate the packages list
    sudo('apt-get -y update')
    
    # Remove mysql, if any, to be replaced later
    sudo('apt-get -y purge mysql-client-core-5.5')
    run('echo mysql-server mysql-server/root_password password toor | sudo debconf-set-selections')
    run('echo mysql-server mysql-server/root_password_again password toor | sudo debconf-set-selections')
    
    # Install python-software-properties (necessary for installing redis-server)
    apt_get_install('python-software-properties')
    sudo('apt-add-repository ppa:chris-lea/redis-server')
    sudo('apt-get -y update')
    
    # Install and setup redis-server
    apt_get_install('redis-server')
    sudo('stop redis-server')
    sudo('sysctl vm.overcommit_memory=1')
    sudo('cp /home/piliomedb/misc/redis_db/dump.rdb /var/lib/redis/redis.rdb')
    sudo('start redis-server')

    # Install other packages
    apt_get_install('curl build-essential libmysqlclient-dev mysql-server mysql-client python-dev python-pip vim screen')
    apt_get_install('cd-hit muscle clustalo ncbi-blast+')
    with settings(warn_only=True):
        run('mysqladmin -u %s -p%s create %s' % (dbuser, dbpass, dbname))
    sudo('easy_install pip==1.4')
    sudo('pip install virtualenv virtualenvwrapper')
    # Install python packages under in the virtual environment
    with prefix('WORKON_HOME=$HOME/.virtualenvs'):
        with prefix('source /usr/local/bin/virtualenvwrapper.sh'):
            run('mkvirtualenv piliomedb')
            with prefix('workon piliomedb'):
                core_modules = [
                    'MySQL-python==1.2.5',
                    'Django==1.6',
                    'celery==3.1.6',
                    'redis==2.8.0',
                    'django-redis-cache==0.10.2',
                    'South==0.8.4',
                    'django-tastypie==0.11.0',
                    'gunicorn==19.0.0',
                    'django-tastypie==0.11.0',
                    'django-bower==4.8.1',
                    'django-supervisor==0.3.2',
                    'biopython==1.63',
                    'dj-static==0.0.5',
                    'beautifulsoup4==4.3.2',
                    'requests==2.0.1',
                    'joblib==0.8.0a3',
                    'numpy==1.8.0',
                    'biopython==1.63',
                    'django-extensions==1.3.3',
                    'django-forms-bootstrap==3.0.0',
                    'ipython==1.1.0',
                    'django-simplesearch==1.0',
                    'supervisor==3.0',
                    'django-supervisor==0.3.2'
                ]
                
                # Install the core modules
                for mod in core_modules:
                    run('pip install %s' % mod)
                    
                # Create the tables
                run('python /home/piliomedb/manage.py syncdb --noinput --all')
        
                print "\n******** Installation completed! ********\n"
                
        
def load_database():
    ''' Prior to running this command, the SQL dump should first be sent to the server 
    through SCP. It is best to perform this outside of fabric so that progress can be 
    clearly monitored. '''
    
    # Import the SQL dump
    with settings(warn_only=True):
        print 'Wait while importing the SQL dump to the database...'
        sql_dump = '/home/piliomedb/piliomedb_dump.sql'
        run('mysql --user=%s --password=%s --host=localhost %s < %s' % (dbuser, dbpass, dbname, sql_dump))
        run('mv %s %s' % (sql_dump, sql_dump + '_LOADED'))


def launch():
    # Execute run.py
    with prefix('WORKON_HOME=$HOME/.virtualenvs'):
        with prefix('source /usr/local/bin/virtualenvwrapper.sh'):
            with prefix('workon piliomedb'):
                run('python /home/piliomedb/manage.py supervisor -d')
                sudo('service nginx restart')
                sudo('service mysql restart')
                

def supervisor_shell():
    with prefix('WORKON_HOME=$HOME/.virtualenvs'):
        with prefix('source /usr/local/bin/virtualenvwrapper.sh'):
            with prefix('workon piliomedb'):
                run('python /home/piliomedb/manage.py supervisor shell')
                
                
def manage(command):
    with prefix('WORKON_HOME=$HOME/.virtualenvs'):
        with prefix('source /usr/local/bin/virtualenvwrapper.sh'):
            with prefix('workon piliomedb'):
                run('python /home/piliomedb/manage.py %s' % command)
