from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
import redis

# Connect to redis
r = redis.Redis(connection_pool=settings.REDIS_CONNECTION_POOL)
   

class BackgroundTask(models.Model):
    """
    Each run of a tool generates a task
    """
    user = models.ForeignKey(User, blank=True, null=True)
    task_id = models.CharField(max_length=100)
    tool_name = models.CharField(max_length=100)
    step = models.IntegerField(blank=True, null=True)
    pipeline_name = models.CharField(max_length=150, blank=True)
    db_version = models.CharField(max_length=5)
    input_type = models.CharField(max_length=30) # file or database entries
    input_file = models.TextField(blank=True) # path to the uploaded file
    input_file_hash = models.TextField(blank=True)
    total_subtasks = models.IntegerField(blank=True, null=True)
    subtasks_success_count = models.IntegerField(blank=True, default=0)
    subtasks_failure_count = models.IntegerField(blank=True, default=0)
    failed_subtasks = models.TextField(blank=True) # JSON list of subtask IDs
    subtasks = models.TextField(blank=True) # JSON list of subtask IDs
    executed = models.DateTimeField(auto_now_add=True)
    completed = models.DateTimeField(null=True, blank=True)
    aborted = models.DateTimeField(null=True, blank=True)
    misc_output = models.TextField(blank=True)

    
    def increment_success_count(self):
        new_count = r.incr('%s__success' % self.task_id)
        del new_count
    
    
    def get_success_count(self):
        sc = r.get('%s__success' % self.task_id)
        if sc:
            return int(sc)
        else:
            return 0
        
        
    def increment_failure_count(self):
        new_count = r.incr('%s__failure' % self.task_id)
        del new_count
    
    
    def get_failure_count(self):
        fc = r.get('%s__failure' % self.task_id)
        if fc:
            return int(fc)
        else:
            return 0
        
        
    class Meta:
        get_latest_by = "executed"