from django.http import HttpResponse, HttpResponseRedirect
from taskqueue.models import BackgroundTask
#from builder.tasks import revoke_load_ushers_tasks
from celery.result import AsyncResult
from data.models import Gene
from celery import current_app as celery
from datetime import datetime
import json


def task_control(request):
    task_id = request.GET['id']
    command = request.GET['command']
    tool = request.GET['tool_name']
    try:
        step = request.GET['step']
    except KeyError:
        step = None
    main_task = BackgroundTask.objects.filter(task_id=task_id, step=step)[0]
    #subtasks = json.loads(main_task.subtasks)
    
    if command == 'get_percentage':
        success_count = main_task.get_success_count()#main_task.subtasks_success_count
        print success_count
        failure_count = main_task.get_failure_count()
        subtasks_run = success_count + failure_count
        total_subtasks = main_task.total_subtasks
        percentage = (float(subtasks_run)/total_subtasks) * 100
        if (success_count + failure_count) == total_subtasks:
            main_task.completed = datetime.now()
            main_task.subtasks = ''
            main_task.subtasks_success_count = main_task.get_success_count()
            main_task.subtasks_failure_count = main_task.get_failure_count()
            main_task.save()
        return HttpResponse(percentage)
        
    if command == 'abort_task':   
        if tool == 'cu_clusters_fetcher':
            """
            ushers = Gene.objects.filter(piliome_annotation='usher')
            for u in ushers:
                fc = u.cu_cluster_fetch_count
                if fc != 0 and fc < 20:
                    uclust = Gene.objects.filter(associated_usher=u)
                    uclust.delete()
                    u.cu_cluster_fetch_count = 0
                    u.save()
            """
        return_url = request.GET['return_url']
        main_task.subtasks = ''
        main_task.aborted = datetime.now()
        main_task.subtasks_success_count = main_task.get_success_count()
        main_task.subtasks_failure_count = main_task.get_failure_count()
        main_task.save()
        return HttpResponseRedirect(return_url)
        
    if tool == 'load_ushers' and command == 'revoke_task':
        for subtask in subtasks:
            celery.control.revoke(subtask, terminate=True)
        ushers = Gene.objects.filter(task=main_task)
        for usher in ushers:
            usher.delete()
        main_task.aborted = datetime.now()
        main_task.save()
        #revoke_load_ushers_tasks.delay(tasks)
        return HttpResponseRedirect('/account/tools/')