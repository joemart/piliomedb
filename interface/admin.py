from django.contrib import admin
from data.models import PiliomeDBVersion


class PiliomeDBVersionAdmin(admin.ModelAdmin):
    fields = ('db_version', 'release_date')
    
admin.site.register(PiliomeDBVersion)


# TODO -- Create admin models for Usher