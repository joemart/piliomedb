from data.models import Taxon, ReferencePaper, UsherHomologyGroup
from simplesearch.functions import get_query


class Search(object):
    
    def __init__(self, term):
        categories = {
            'Taxa': ['name'], 
            'Papers': ['title', 'abstract', 'authors'], 
            'UHGs': ['id', 'name', 'synonyms']
        }
        results = {}
        for category, fields in categories.items():
            q = get_query(term, fields)
            if category == 'Taxa':
                r = Taxon.objects.filter(q).filter(level='strain')
                results[category] = r
            elif category == 'Papers':
                r = ReferencePaper.objects.filter(q).order_by('-year')
                results[category] = r
            elif category == 'UHGs':
                r = UsherHomologyGroup.objects.filter(q)
                results[category] = r
        self.results = results