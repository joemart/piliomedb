from django import template
from data.models import UsherHomologyGroup

register = template.Library()


@register.filter
def hash(h, key):
    return h[key]


@register.filter
def get_match(aln_title):
    if 'usher:' in aln_title:
        match = aln_title.split()[-2]
    else:
        match = aln_title.split()[-1]
    return match
    

@register.filter
def identify_uhg(aln_title):
    gene_id = aln_title.split()[-1].split(':')[-1]
    uhg = UsherHomologyGroup.objects.filter(members__contains=gene_id)
    if uhg:
        return int(uhg[0].id)
    else:
        return None
        
        
@register.filter
def compute_identity(hsp):
    return round(float(hsp.identities)/hsp.align_length, 3) * 100
    
    
@register.filter
def concatenate_list(list):
    return ', '.join(eval(list))
    
    
@register.filter
def format_authors(authors):
    authors = eval(authors)
    if len(authors) <= 2:
        return ', '.join(authors) + '.'
    else:
        return authors[0] + ' et al.'