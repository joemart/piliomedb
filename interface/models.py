from django.db import models
from django.contrib.auth.models import User
from taskqueue.models import BackgroundTask
import hashlib


class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True)
    title = models.CharField(max_length=10, blank=True)
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    institute_university = models.CharField(max_length=200, blank=True)
    department = models.CharField(max_length=200, blank=True)
    laboratory = models.CharField(max_length=200, blank=True)
    address_1 = models.CharField(max_length=100, blank=True)
    address_2 = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100, blank=True)