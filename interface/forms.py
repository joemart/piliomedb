from data.models import UsherHomologyGroup
from django import forms


class LoginForm(forms.Form):
    
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control', 
                'placeholder': 'Username'
            }))
            
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class' : 'form-control', 
                'placeholder': 'Password'
            }))
    
    
class PDBUploadForm(forms.Form):
    
    name = forms.CharField(
        label='Name', required=True, 
        widget=forms.TextInput(attrs={'class': 'form-control'}))
        
    chain = forms.CharField(
        label='Chain', 
        widget=forms.TextInput(attrs={'class': 'form-control'}))
        
    pdb_file  = forms.FileField(required=True)
    

class SequenceSearchForm(forms.Form):
    db_choices = ['ushers', 'chaperones', 'adhesins']
    
    db = forms.ChoiceField(
        choices=[(x,x) for x in db_choices], 
        label='Database', 
        widget=forms.Select(attrs={'class': 'form-control', 'style': 'width: 200px;'}))
        
    sequence = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 4}))
    

annotation_choices = [
    ('usher', 'Ushers'),
    ('chaperone', 'Chaperones'),
    ('adhesin', 'Adhesins'),
    #('all', 'All')
]

uhgs = UsherHomologyGroup.objects.all()

def uhg_name(uhg):
    if uhg.name:
        if uhg.synonyms:
            synonyms = eval(uhg.synonyms)
            return 'UHG%s - %s (%s)' % (uhg.id, uhg.name, ', '.join(synonyms))
        else:
            return 'UHG%s - %s' % (uhg.id, uhg.name)
    else:
        return 'UHG%s' % uhg.id

class SequenceDownloadForm(forms.Form):
    uhg = forms.ChoiceField(choices=[(int(x.id), uhg_name(x)) for x in uhgs], label='UHG')
    annotation = forms.ChoiceField(choices=annotation_choices)
    moltype  = forms.ChoiceField(choices=[('prot', 'Protein'), ('nuc', 'Nucleotide')], label='Molecule type')
    #prepend_uhgID = forms.BooleanField(label='Prepend UHG ID to gene name', required=False)
    #prepend_usherID = forms.BooleanField(label='Prepend usher ID to gene name', required=False)