from django.shortcuts import render_to_response
from data.models import PiliomeDBVersion
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from interface.forms import LoginForm, PDBUploadForm, SequenceDownloadForm, SequenceSearchForm
from interface.tasks import build_sequence_file, conservation_mapping
from data.utils.pubmed_fetcher import FetchPubmed
from taskqueue.models import BackgroundTask
from django.contrib.auth import logout
from builder.tasks import execute_blast
from data.models import UsherHomologyGroup, ReferencePaper, ReferenceLink, ReferenceStructure
from django.views.decorators.cache import cache_page
from django.core.mail import send_mail
from django.conf import settings
from interface.search import Search
import cPickle as pickle
import random
import uuid
import os


@cache_page(60 * 15)
def home(request):
    dbversion = PiliomeDBVersion.objects.all()[0]
    papers = ReferencePaper.objects.filter(linked_to_uhg=True).order_by('-date_added')[0:3]
    for paper in papers:
        authors = eval(paper.authors)
        if len(authors) > 2:
            paper.author_list = '%s et al' % authors[0]
        else:
            paper.author_list = ', '.join(eval(paper.authors))
    uhgs = UsherHomologyGroup.objects.all()
    ref_links = ReferencePaper.objects.filter(linked_to_uhg=True)
    ref_structures = ReferenceStructure.objects.all()
    context = {
        'papers': papers,
        'version': dbversion.db_version,
        'ref_links': ref_links.count(),
        'ref_structures': ref_structures.count(),
        'uhg_count': uhgs.count(),
        'genomes_count': dbversion.taxa.filter(level='strain', with_genome=True).count(),
        'usher_genes_count': dbversion.genes.filter(piliome_annotation='usher', associated_usher__isnull=True).count()
    }
    return render_to_response('interface/home.html', context, RequestContext(request))


@cache_page(60 * 15)
def browse(request):
    return render_to_response('interface/browse.html', RequestContext(request))
    
    
def conservation_mapper(request):
    if request.method == 'GET':
        form = PDBUploadForm()
        task_id = None
    elif request.method == 'POST':
        form = PDBUploadForm(request.POST, request.FILES)
        if form.is_valid():
            pdb_file = request.FILES['pdb_file']
            pdb_name = '_'.join(request.POST['name'].split())
            chain = request.POST['chain']
            if not chain:
                chain = None
            task_id = str(uuid.uuid4())
            upload_id = task_id.split('-')[-1]
            dest_file = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'uploads', '%s_%s.pdb' % (pdb_name, upload_id))
            with open(dest_file, 'wb+') as destination:
                for chunk in pdb_file.chunks():
                    destination.write(chunk)
            conservation_mapping.apply_async(task_id=task_id, args=(dest_file, pdb_name, chain))
        else:
            task_id = None
    context = {
        'form': form,
        'task_id': task_id
    }
    return render_to_response('interface/conservation_mapper.html', context, RequestContext(request))
    

def download(request):
    if request.method == 'GET':
        form = SequenceDownloadForm()
        task_id = None
    
    elif request.method == 'POST':
        form = SequenceDownloadForm(request.POST)
        if form.is_valid():
            uhg_id = form.cleaned_data['uhg']
            annotation = form.cleaned_data['annotation']
            moltype = form.cleaned_data['moltype']
            task_id = str(uuid.uuid4())
            build_sequence_file.apply_async(task_id=task_id, args=(uhg_id, annotation, moltype))
        else:
            task_id = None
        
    context = {
        'form': form,
        'task_id': task_id
    }
    return render_to_response('interface/download.html', context, RequestContext(request))
    
    
def serve_download_file(request, task_id, file_name):
    task = BackgroundTask.objects.filter(task_id=task_id)[0]
    misc_output = eval(task.misc_output)
    output_file = misc_output['output_file']
    #download_file = os.path.join(settings.PILIOMEDB_FILES, 'downloads', task_id)
    download_file = open(output_file, 'r')
    download_file_contents = download_file.read()
    download_file.close()
    if '.hmm' not in output_file:
        os.remove(output_file)
    return HttpResponse(download_file_contents, content_type='application/force-download')
    

def download_profiles(request):
    hmm_file = os.path.join(settings.MEDIA_ROOT, 'results', 'downloads', 'combined_uhgs.hmm')
    profiles = open(hmm_file, 'r').read()
    return HttpResponse(profiles, content_type='application/force-download')


def user_guide(request):
    return render_to_response('interface/user_guide.html', RequestContext(request))
    
    
def search(request):
    form = SequenceSearchForm()
    if request.method == 'POST':
        method = request.POST['method']
        if method == 'search_by_fasta':
            sequence = request.POST['sequence']
            if sequence:
                db = request.POST['db'].replace(' ', '_')
                task_id = str(uuid.uuid4())
                execute_blast.apply_async(task_id=task_id, args=(request.user, sequence, db))
                results = task_id
            else:
                results = None
    else:
        results = None
    context = {'path': request.path, 'form': form, 'results': results}
    return render_to_response('interface/search.html', context, RequestContext(request))
    

def search_results(request, task_id=None):
    if 'by_sequence' in request.path:
        task = BackgroundTask.objects.filter(task_id=task_id)[0]
        results = pickle.load(open(task.misc_output, 'r'))
        context = {'results': results.blast_record}
        context['search_method'] = 'sequence'
    else:
        r = Search(request.GET['term'])
        context = r.results
        context['search_method'] = 'keywords'
    return render_to_response('interface/search_results.html', context, RequestContext(request))

    
def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/administration/')
                else:
                    return HttpResponse('User not active anymore.')
            else:
                return HttpResponse('Login failed!')
        else:
            return HttpResponse('Form is not valid!')
    elif request.method == 'GET':
        form = LoginForm()
        context = {'form': form}
        return render_to_response('interface/login.html', context, RequestContext(request))


@cache_page(60 * 15)
def list_references(request):
    uhg_id = request.GET['uhg']
    uhg = UsherHomologyGroup.objects.get(id=uhg_id)
    references = uhg.references.get_queryset()
    for ref in references:
        ref.reference.authors = eval(ref.reference.authors)
    context = {'references': references}
    return render_to_response('interface/references.html', context, RequestContext(request))


def send_feedback(request):
    csrf_code = request.POST.get('csrf')
    name = request.POST.get('name')
    email = request.POST.get('email')
    message = request.POST.get('message')
    if csrf_code == 'ABXV0100004':
        email_message = 'Name: %s\nEmail: %s\n\nMessage:\n%s\n' % (name, email, message)
        send_mail('PiliomeDB: User Feedback', email_message, 'feedback_no-reply@piliomedb.org',
            settings.ADMINS, fail_silently=False)
    return HttpResponse('Sent!')


@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')
    

@login_required
#@cache_page(60 * 15)
def administration(request):
    context = {}
    return render_to_response('interface/administration.html', context, RequestContext(request))
    
    
@login_required
def curation_add_reference(request):
    path = request.path
    context = {'path': path}
    return render_to_response('interface/administration.html', context, RequestContext(request))
    
    
def create_reference(request):
    pubmed_id = request.GET['pubmed_id']
    uhg_id = request.GET['uhg_id']
    ref = FetchPubmed(pubmed_id)
    m = ref.save_to_database(uhg_id)
    return HttpResponse(m)
    

@cache_page(60 * 15)
def browse_table_view(request):
    uhgs = UsherHomologyGroup.objects.all()
    if 'order_by' in request.GET.keys():
        orderby = request.GET['order_by']
        if orderby == 'clade':
            uhgs = uhgs.order_by('clade')
    all_domains = []
    for uhg in uhgs:
        uhg.unique_members = len(set(uhg.get_members()))
        if uhg.synonyms:
            if '[' in uhg.synonyms:
                syn = eval(uhg.synonyms)
                uhg.synonyms = ', '.join(syn)
        adh_domains = eval(uhg.adhesin_pfam_domains)
        for domain in adh_domains:
            if domain not in all_domains:
                all_domains.append(domain)
        uhg.adhesin_pfam_domains = adh_domains
    domain_colors = {x: "#%06x" % random.randint(0,0xFFFFFF) for x in all_domains}
    domain_colors['No domains'] = 'black'
    context = {'uhgs': uhgs, 'colors': domain_colors, 'all_domains': all_domains}
    return render_to_response('interface/table_view.html', context, RequestContext(request))
    

def uhg_info(request):
    uhg_id = request.GET['id']
    uhg = UsherHomologyGroup.objects.filter(id=uhg_id)[0]
    if uhg.name:
        if uhg.synonyms:
            try:
                syns = ", ".join(eval(uhg.synonyms))
            except NameError:
                syns = uhg.synonyms
            uhg.uhg_name = "%s (%s)" % (uhg.name, syns)
        else:
            uhg.uhg_name = uhg.name
    else:
        uhg.uhg_name = 'unknown'
    ref_links = uhg.reference_links.get_queryset()
    references = []
    structures = []
    for rl in ref_links:
        ref = rl.reference_paper
        ref.authors = eval(ref.authors)
        references.append(ref)
        if rl.reference_sequence.reference_structure:
            struct = rl.reference_sequence.reference_structure
            structures.append(struct)
    context = {
        'uhg': uhg,
        'references': set(references),
        'structures': structures
    }
    return render_to_response('interface/uhg_info.html', context, RequestContext(request))
    

def strain_view(request):
    pass    


# Unused view function
def tree_comparison(request):
    context = {}
    return render_to_response('interface/tree_compare.html', context, RequestContext(request))  
    

# Unused view function
def tree_comparison2(request):
    context = {'groups': [1,2]}
    return render_to_response('interface/tree_compare2.html', context, RequestContext(request))
