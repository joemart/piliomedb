from celery import current_app as celery
from data.models import UsherHomologyGroup, Gene
from Bio.PDB import PDBList, PDBParser, PPBuilder, PDBIO, Select
from taskqueue.models import BackgroundTask
from builder.utils.blast import BlastP
from subprocess import Popen, PIPE
from django.conf import settings
from data.utils.seq2redis import r
from celery import task
import os


@task
def _write_sequence_to_file(gene_id, seq_file_path, master_task_pk):
    task = BackgroundTask.objects.get(pk=master_task_pk)
    seq_file = open(seq_file_path, 'a')
    seq = r.get(gene_id)
    seq_file.write(seq)
    seq_file.close()
    task.misc_output = str({'output_file': seq_file_path})
    task.save()
    task.increment_success_count()
    

@task
def build_sequence_file(uhg_id, annotation, moltype):
    task_id = build_sequence_file.request.id
    gene_ids = r.lrange('uhg%s_%ss' % (uhg_id, annotation), 0, -1)
    gene_ids = ['sequence__%s__%s' % (x, moltype) for x in gene_ids]
    # Save this background task
    task = BackgroundTask(
        input_type='user download request',
        task_id=task_id,
        tool_name='build_sequence_file',
        total_subtasks = len(gene_ids)
    )
    task.save()
    seq_file_path = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'downloads', task_id + '.fasta')
    for gene in gene_ids:
        subtask = _write_sequence_to_file.apply_async(args=(gene, seq_file_path, task.id))


@task
def _write_pymol_mapping_script(aln_file, conservation_scores, master_task_pk):
    task = BackgroundTask.objects.get(pk=master_task_pk)
    task_id = task.task_id
    working_dir = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'conservation_mapping', task_id)
    args_dict = eval(task.misc_output)
    pdb = open(args_dict['pdb_file'], 'r').read()
    aln = open(aln_file, 'r').read()
    scores = open(conservation_scores,'r').read()
    args = (pdb, aln, scores, args_dict['structure_name'])
    params = 'pdb_string="""%s"""\naln_string="""%s"""\nconservation_scores="""%s"""\nstructure_name="%s"' % args
    script_path = os.path.join(settings.BASE_DIR, 'misc', 'scripts', 'conservation_mapping_script_base.py')
    mapping_script = open(script_path, 'r').read()
    combined = params + '\n' + mapping_script
    outf_path = os.path.join(working_dir, 'conservation_mapping.py')
    outf = open(outf_path, 'w')
    outf.write(combined)
    outf.close()
    args_dict['output_file'] = outf_path
    task.misc_output = str(args_dict)
    task.save()
    # Increment success count
    task.increment_success_count()
    

@task
def _compute_conservation_scores(aln_file, master_task_pk):
    task = BackgroundTask.objects.get(pk=master_task_pk)
    task_id = task.task_id
    working_dir = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'conservation_mapping', task_id)
    script_dir = os.path.join(settings.BASE_DIR, 'misc', 'packages', 'conservation_code', 'scripts')
    matrix = os.path.join(script_dir, 'matrix', 'blosum62.bla')
    distribution = os.path.join(script_dir, 'distributions', 'blosum62.distribution')
    cmd = 'score_conservation.py'
    cmd += ' -m %s -d %s -g 0.999999999999' % (matrix, distribution)
    cmd += ' -s js_divergence %s' % aln_file
    p = Popen(cmd, shell=True, stdout=PIPE)
    conservation_scores, stderr = p.communicate()
    outf_path = os.path.join(working_dir, 'conservation_scores.txt')
    outf = open(outf_path, 'w')
    outf.write(conservation_scores)
    outf.close()
    _write_pymol_mapping_script.apply_async(args=(aln_file, outf_path, task.id))
    # Increment success count
    task.increment_success_count()


@task
def _align_sequences(seq_file, master_task_pk, method='clustalo'):
    task = BackgroundTask.objects.get(pk=master_task_pk)
    aln_out = seq_file.split('.')[0] + '_alignment.fasta'
    if method == 'muscle':
        cmd = 'muscle -in %s -out %s' % (seq_file, aln_out)
    elif method == 'clustalo':
        hmm = os.path.join(settings.BASE_DIR, 'misc', 'hmm', 'PF00419.hmm')
        cmd = 'clustalo -i %s -o %s --hmm-in=%s -v' % (seq_file, aln_out, hmm)
    os.system(cmd)
    _compute_conservation_scores.apply_async(args=(aln_out, task.id))
    # Increment success count
    task.increment_success_count()


class SelectChains(Select):
    """ Only accept the specified chains when saving. """
    def __init__(self, chain_letters):
        self.chain_letters = chain_letters

    def accept_chain(self, chain):
        return (chain.get_id() in self.chain_letters)


@task
def conservation_mapping(original_pdb_file, pdb_name, pdb_chain):
    task_id = conservation_mapping.request.id
    # Save master task
    task = BackgroundTask(
        input_type='PDB file',
        task_id=task_id,
        tool_name='conservation_mapper',
        total_subtasks = 5
    )
    task.save()
    # Create working directory
    working_dir = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'conservation_mapping', task_id)
    if not os.path.exists(working_dir):
        os.mkdir(working_dir)
    # Extract sequence from pdb file
    structure = PDBParser().get_structure(pdb_name, original_pdb_file)
    os.remove(original_pdb_file)
    io = PDBIO()
    io.set_structure(structure)
    if pdb_chain:
        chain = structure[0][pdb_chain]
        pdb_file = os.path.join(working_dir, '%s_%s.pdb' % (pdb_name, pdb_chain))
        io.save(pdb_file, select=SelectChains([pdb_chain]))
    else:
        chain = structure
        pdb_file = os.path.join(working_dir, '%s.pdb' % pdb_name)
        io.save(pdb_file)
    ppb = PPBuilder()
    pp = ppb.build_peptides(chain, aa_only=False)
    seq_string = ''.join([x.get_sequence().tostring() for x in pp])
    fasta_id = '%s_%s' % (pdb_name, pdb_chain)
    fasta_seq = '>%s\n%s\n' % (fasta_id, seq_string)
    # Identify UHG
    annotation = 'adhesins'
    search = BlastP(seq_string, db=annotation, identity_cutoff=60)
    results = search.blast_record
    uhg_id = None
    for alignment in results.alignments:
        for hsp in alignment.hsps:
            if hsp.expect < 0.0001:
                percent_id = round(float(hsp.identities)/hsp.align_length, 3) * 100
                if percent_id >= 90:
                    gene_id = alignment.title.split()[-1].split(':')[-1]
                    uhg = UsherHomologyGroup.objects.filter(members__contains=gene_id)
                    if uhg:
                        uhg_id = int(uhg[0].id)
                        break
        if uhg_id:
            break
    # Increment success count
    task.increment_success_count()
    # If UHG is identified, perform alignment of adhesins in this UHG
    # together with the sequence obtained from the structure
    if uhg_id:
        # Perform alignment
        # Get the sequence file for the matched 
        seq_prots = os.path.join(settings.BASE_DIR, 'misc', 'sequences_prot')
        uhg_annot_seq = os.path.join(seq_prots, annotation, 'uhg%s' % uhg_id, '%s.fasta' % annotation)
        uhg_annot_seq_copy = os.path.join(working_dir, 'sequences.fasta')
        os.system('cp %s %s' % (uhg_annot_seq, uhg_annot_seq_copy))
        seq_nr = os.path.join(working_dir, 'sequences_nr100.fasta')
        cdhit_check = Popen('which cdhit', shell=True, stdout=PIPE)
        stdout, stderr = cdhit_check.communicate()
        if stdout:
            cdhit_exec = 'cdhit'
        else:
            cdhit_exec = 'cd-hit'
        os.system('%s -i %s -o %s -c 1.0 -S 0.0' % (cdhit_exec, uhg_annot_seq_copy, seq_nr))
        struct_seq = os.path.join(working_dir, 'structure.fasta')
        struct_seq_outf = open(struct_seq, 'w')
        struct_seq_outf.write(fasta_seq)
        struct_seq_outf.close()
        combined_seq = os.path.join(working_dir, 'sequences_nr100_plus_structure.fasta')
        os.system('cat %s %s > %s' % (seq_nr, struct_seq, combined_seq))
        # Execute the alignment
        _align_sequences.apply_async(args=(combined_seq, task.id))
        # Save some details for the next tasks
        args_dict = {'pdb_file': pdb_file, 'structure_name': fasta_id, 'uhg_id': uhg_id}
        task.misc_output = str(args_dict)
        task.save()
        # Increment success count
        task.increment_success_count()
    else:
        print 'Cannot be identified with any of the UHGs'