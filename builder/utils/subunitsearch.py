from data.models import Gene, UsherHomologyGroup
from django.conf import settings
import os


class SubunitSearch(object):
    
    def __init__(self, sample_gene_object_id, uhg_id, annotation='subunit', verbose=True):
        self.verbose = verbose
        self.annotation = annotation
        self.sample_gene = sample_gene_object_id
        self.uhg_id = uhg_id
        self.annotated_gene_ids = []

    def execute_blast(self, query, db):
        """
        Both arguments should be file paths to FASTA files
        """
        # Perform the search and parse the results
        if self.verbose: print 'Executing the search...'
        resultspath = os.path.join(settings.PILIOMEDB_FILES, 'temp', 'subunits_search', 'results.txt')
        if os.path.exists(resultspath):
            os.remove(resultspath)
        os.system('/usr/local/bin/blastp -query %s -db %s -out %s -outfmt 7' % (query, db, resultspath))
        results = open(resultspath, 'r')
        matches = [x for x in results.readlines() if "#" not in x]
        matches = [{'gene_object_id': x.split('\t')[1].split('_')[-1], 'identity': float(x.split('\t')[2])} for x in matches]

        # Perform screening of result based on identity cut-off
        matches = [x for x in matches if x['identity'] >= self.identity_cutoff]
        return matches

    def find_subunits(self, relative_to_usher=None, identity_cutoff=50):
        self.identity_cutoff = identity_cutoff
        uhg = UsherHomologyGroup.objects.get(id=self.uhg_id)
        usher_members = uhg.get_members()
        sampleseq_path = os.path.join(settings.PILIOMEDB_FILES, 'temp', 'subunits_search', 'query.fasta')
        sampleseq = open(sampleseq_path, 'w')
        query = Gene.objects.get(id=self.sample_gene)
        sampleseq.write(query.get_fasta_sequence(fasta_id='gene_object_id'))
        sampleseq.close()
        dbseq_path = os.path.join(settings.PILIOMEDB_FILES, 'temp', 'subunits_search', 'uhg%s_dbsequences.fasta' % self.uhg_id)
        dboutdir = os.path.join(settings.PILIOMEDB_FILES, 'temp', 'subunits_search',)
        dbout = os.path.join(dboutdir, 'uhg%s_seqdb' % self.uhg_id)
        if not os.path.exists(dbseq_path):
            dbseq = open(dbseq_path, 'w')
            if self.verbose: print 'Building sequence files...'
            for usher in usher_members:
                ush = Gene.objects.filter(gene_id=usher, associated_usher__isnull=True)[0]
                for member in ush.get_cluster():
                    if member.amino_acid_sequence:
                        dbseq.write(member.get_fasta_sequence(fasta_id='gene_object_id'))
            dbseq.close()
            # Make a BLAST db
            if self.verbose: print "Building BLAST db..."
            os.system('/usr/local/bin/makeblastdb -dbtype prot -in %s -out %s' % (dbseq_path, dbout))
        matches = self.execute_blast(sampleseq_path, dbout)
        #print matches
        if matches:
            if self.verbose: print 'Saving annotations...'
            for match in matches:
                match = match['gene_object_id']
                g = Gene.objects.get(id=match)
                if g.associated_usher:
                    usher_id = g.associated_usher.gene_id
                    if abs(int(usher_id) - int(g.gene_id)) <= relative_to_usher:
                        if not g.piliome_annotation:
                            g.piliome_annotation = self.annotation
                            g.save()
                            self.annotated_gene_ids.append(match)
            if self.verbose: print '%s genes were annotated' % len(set(self.annotated_gene_ids))
            if self.verbose: print '***** Completed! *****'
        else:
            print 'No matches found.'
            
    def undo_annotation(self):
        for gene in self.annotated_gene_ids:
            g = Gene.objects.get(id=gene)
            g.piliome_annotation = ''
            g.save()