from data.models import NonRedundantUsher
import json
import os


def generate_nrushers_sequence_file(outfile):
    outfile = open(outfile, 'w')
    nrushers = NonRedundantUsher.objects.all()
    for nrgroup in nrushers:
        rep = nrgroup.representative
        seq = '>%s\n%s\n' % (rep.fasta_description, rep.amino_acid_sequence)
        outfile.write(seq)
    outfile.close()
    
    
def execute_alignment(seqfile, alnfile, program='clustalo', hmmfile=None):
    if program == 'clustalo':
        cmd = 'clustalo -i %s -o %s -v --hmm-in=%s' % (seqfile, alnfile, hmmfile)
    elif program == 'muscle':
        cmd = 'muscle -in %s -out %s' % (seqfile, alnfile)
    os.system(cmd)
    
    
def execute_alignment_trimming(alnfile, trimmed_alnfile):
    os.system('trimal -in %s -out %s -automated1' % (alnfile, trimmed_alnfile))
    
    
def remove_redundant_sequences(trimmed_alnfile):
    # Execute the CD-HIT clustering
    wdir, fname = os.path.split(trimmed_alnfile)
    nr_trimmed_alnfile = os.path.join(wdir, fname.replace('.fasta', '_nr.fasta'))
    os.system('cdhit -i %s -o %s -c 1.0' % (trimmed_alnfile, nr_trimmed_alnfile))
    # Build the mapping and save as JSON file
    pass