from data.models import Gene, Scaffold


def fix_missing_taxon_ids():
    print 'Fixing missing taxon IDs...'
    count = 0
    ntx = Gene.objects.filter(taxon_id='')
    for gene in ntx:
        count += 1
        print '  %s/%s - Dealing with %s' % (count, ntx.count(), gene.gene_id)
        scaff = Scaffold.objects.filter(scaffold_name=gene.scaffold_name)
        if scaff:
            gene.taxon_id = scaff[0].taxon_id
            gene.save()
    print 'Done!'