from django.conf import settings
from data.models import Gene
import platform
import os


machine = platform.uname()[0]
if machine == 'Linux':
    usrbin = '/usr/bin'
elif machine == 'Darwin':
    ursbin = '/usr/local/bin'


def _get_sequences(gene_ids, outfile, verbose=False):
    outfile = open(outfile, 'w')
    count = 0
    for gid in gene_ids:
        #print gid
        count += 1
        if verbose: print "  %s/%s - dealing with %s" % (count, len(gene_ids), gid)
        g = Gene.objects.filter(gene_id=gid)[0]
        seq = g.get_fasta_sequence()
        outfile.write(seq)
    outfile.close()


def find_similar(query_gene, other_genes, identity_cutoff=50, verbose=False):
    """
    Finds similar sequences
    """
    # Generate the sequence file of the other genes
    if verbose: print "Fetching sequences..."
    seqoutpath = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'context_viewer', 'dbsequences.fasta')
    queryoutpath = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'context_viewer', 'query.fasta')

    if verbose: print 'DB sequences:'
    if os.path.exists(seqoutpath):
        os.remove(seqoutpath)
        _get_sequences(other_genes, seqoutpath)
    else:
        _get_sequences(other_genes, seqoutpath)
    if verbose: print 'Query sequence:'
    if os.path.exists(queryoutpath):
        os.remove(queryoutpath)
        _get_sequences([query_gene], queryoutpath)
    else:
        _get_sequences([query_gene], queryoutpath)

    # Make a BLAST db
    if verbose: print "Building BLAST db..."
    dboutdir = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'context_viewer',)
    dbout = os.path.join(dboutdir, 'seqdb')
    os.system('%s/makeblastdb -dbtype prot -in %s -out %s' % (usrbin, seqoutpath, dbout))

    # Perform the search and parse the results
    if verbose: print 'Executing the search...'
    resultspath = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'context_viewer', 'results.txt')
    if os.path.exists(resultspath):
        os.remove(resultspath)
    os.system('%s/blastp -query %s -db %s -out %s -outfmt 7' % (usrbin, queryoutpath, dbout, resultspath))
    results = open(resultspath, 'r')
    matches = [x for x in results.readlines() if "#" not in x]
    matches = [{'gene_id': x.split('\t')[1], 'identity': float(x.split('\t')[2])} for x in matches]

    # Perform screening of result based on identity cut-off
    matches = [x for x in matches if x['identity'] >= identity_cutoff]
    return matches