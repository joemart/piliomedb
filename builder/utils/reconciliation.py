from ete2 import PhyloTree, TreeStyle, AttrFace, TextFace, add_face_to_node
from django.conf import settings
from data.models import Gene, Taxon
from collections import defaultdict
import cPickle as pickle
from itertools import chain, combinations
import colorsys
import uuid
import random
import json
import os


class HomologyGroups(object):

    def __init__(self):
        # Initialize the data containers
        self.homology_groups = {}
        self.member_to_hg_mapping = {}
        self.merges = {}

    def add_member(self, gene_id, uhg_id):
        """ Adds a gene to a given homology group """
        # Add to homology groups dictionary
        if uhg_id in self.homology_groups.keys():
            self.homology_groups[uhg_id].append(gene_id)
        else:
            self.homology_groups[uhg_id] = [gene_id]
        # Add to the mapping
        self.member_to_hg_mapping[gene_id] = uhg_id

    def check_homology_group(self, gene_ids_list):
        """ Checkst he homology group of the genes in the given list """
        gene_hgs = []
        for gene in gene_ids_list:
            try:
                gene_hg = self.member_to_hg_mapping[gene]
            except KeyError:
                gene_hg = None
            if gene_hg:
                gene_hgs.append(gene_hg)
        return gene_hgs
        

class ClusterMerges(object):
    
    class MergeRecord(object): pass
    
    def __init__(self):
        self.records = defaultdict(list)
        
    def add_record(self, gene_id, cluster_number, merge_node, record_type):
        mr = ClusterMerges.MergeRecord()
        mr.name = gene_id
        mr.cluster = cluster_number
        mr.merge_node = merge_node
        mr.record_type = record_type
        self.records['%s-%s' % (merge_node, cluster_number)].append(mr)
        

class RandomColor(object):
    
    def hls2hex(self, h, l, s):
        """ Converts from HLS to RGB """
        return '#%02x%02x%02x' %tuple(map(lambda x: int(x*255), colorsys.hls_to_rgb(h, l, s)))
        
    def generate(self, h=None, s=0.5, l=0.5):
        """ Returns a RGB color code with the specific Hue, Saturation and
        lightness. Otherwise, it returns a random color. """
        if not h:
            h = random.random()
        return self.hls2hex(h, l, s)


class Reconciliation(object):

    def __init__(self, tree_file_path, clusters_file, rooting='midpoint', cached_content=False):
        """ Intializes the object and its methods """

        print '\nReading the clusters and tree files...'
        # Read the cluster files
        self.clusters = json.load(open(clusters_file))
        # Build the reverse clusters map
        self.clusters_rev = {}
        for k, v in self.clusters.items():
            for x in v:
                self.clusters_rev[x] = k
        # Load data
        cache_dir = os.path.join(settings.PILIOMEDB_FILES, 'temp', 'reconciliation_algorithm')
        tree_dump = os.path.join(cache_dir, 'tree.pickle')
        phyly_dump = os.path.join(cache_dir, 'phyly.pickle')
        if cached_content:
            self.tree = pickle.load(open(tree_dump))
            self.clusters_phyly = pickle.load(open(phyly_dump))
        else:
            # Read the tree file
            self.tree = PhyloTree(tree_file_path, sp_naming_function=None)
            # Prepare the tree
            self.rooting = rooting
            self.prepare_tree()
            pickle.dump(self.tree, open(tree_dump, 'w'))
            # Identify monophyletic clusters
            self.clusters_phyly = {}
            self.identify_clusters_phyly_types()
            pickle.dump(self.clusters_phyly, open(phyly_dump, 'w'))
            
        # Intialize the homology groups object
        self.hgs = HomologyGroups()
        self.hg_counter = 0
        # Execute the reconciliation functions
        self.search_and_compile_merge_records()
        self.resolve_merges()
        
    def assign_species(self, node):
        gene = Gene.objects.filter(gene_id=node.name, piliome_annotation='usher', associated_usher__isnull=True)[0]
        taxon_id = gene.taxon_id
        strain = Taxon.objects.filter(img_taxon_id=taxon_id)[0]
        species = strain.parents.get_query_set()[0]
        if species.name == 'sp.' or species.name == 'sp':
            species_id = str(species.id) + str(strain.id)
        else:
            species_id = str(species.id)
        node.add_feature('taxon_id', gene.taxon_id)
        node.add_feature('species', species_id)
        node.add_feature('strain_name', strain.name)

    def prepare_tree(self, species_assignment_fn=None):
        """ Prepares the phylogenetic tree """

        # Root the tree
        print 'Rooting the tree...'
        if self.rooting == 'midpoint':
            midpoint_node = self.tree.get_midpoint_outgroup()
            self.tree.set_outgroup(midpoint_node)
        else:
            outgroups = self.rooting.split(',')
            outgroup_node = self.tree.get_common_ancestor(*outgroups)
            self.tree.set_outgroup(outgroup_node)

        # Assign the original cluster number as feature for
        # each leaf node
        print 'Assigning the cluster and species features...'
        for node in self.tree:
            if node.is_leaf():
                node.add_feature('cluster', self.clusters_rev[node.name])

        # Fix the assignment of species to each gene
        # then add this species name or code as feature
        if species_assignment_fn:
            for node in self.tree:
                if node.is_leaf():
                    species = species_assignment_fn(node)
                    node.add_feature('species', species)
        else:
            for node in self.tree:
                if node.is_leaf():
                    self.assign_species(node)

        # Infer the evolutionary events using the species
        # overlap algorithm
        self.evol_events = self.tree.get_descendant_evol_events()
        
        def assign_node_IDs(node):
            node.add_feature('node_id', str(uuid.uuid4()).split('-')[4])
            if node.get_children():
                for child in node.get_children():
                    assign_node_IDs(child)
                    
        # Recursively assign node IDs
        assign_node_IDs(self.tree)

    def identify_clusters_phyly_types(self):
        """ Identifies the phyly type of each cluster """

        print 'Identifying phyly types...'
        for k in self.clusters:
            cluster_members = self.clusters[k]
            monophyletic, phyly_type = self.tree.check_monophyly(
                cluster_members, 'name')
            self.clusters_phyly[k] = phyly_type
    
    def search_and_compile_merge_records(self):
        """ Executes the reconcilation algorithm """
            
        # Initialize the cluster merges object
        self.merges = ClusterMerges()

        def recursive_search(node):

            if node.is_leaf():
                parent_of_node = node.get_ancestors()[0]
                details = (node.name, node.cluster, parent_of_node.node_id, 'L%s' % parent_of_node.evoltype)
                self.merges.add_record(*details)
            else:
                children = {c:[] for c in node.get_children()}
                for child in children:
                    node_clusters = [x.cluster for x in node.get_leaves()]
                    children[child] = list(set(node_clusters))
                node_clusters_combined = list(chain(*children.values()))
                # Get the monophyletic child
                mono_child = [(k,v) for k,v in children.items() if len(set(v)) == 1]
                for child in children:
                    if len(set(node_clusters_combined)) == 2:
                        if node.evoltype == 'S':
                            for leaf in node.get_leaves():
                                details = (leaf.name, leaf.cluster, node.node_id, '2S')
                                self.merges.add_record(*details)
                        elif node.evoltype == 'D':
                            if mono_child:
                                mc = mono_child.pop()
                                for leaf in mc.get_leaves():
                                    details = (leaf.name, leaf.cluster, node.node_id, '2D')
                                    self.merges.add_record(*details)
                    if len(set(node_clusters_combined)) == 1:
                        parent_of_node = node.get_ancestors()[0]
                        for leaf in node.get_leaves():
                            details = (leaf.name, leaf.cluster, parent_of_node.node_id, '1%s' % parent_of_node.evoltype)
                            self.merges.add_record(*details)   
                    else:
                        recursive_search(child)
                        
        # Execute the recursive search function
        recursive_search(self.tree) 
        
    def resolve_merges(self):
        
        # Make a list of the cluster numbers
        cluster_numbers = self.clusters.keys()
        
        # Resolve the merges
        for clust in cluster_numbers:
            # Check if there are any merge records for this cluster
            merge_records = []
            for_merging = False
            # Fish out the merge records on the current cluster
            for rec, lst in self.merges.records.items():
                lst_clstrs = set([x.cluster for x in lst])
                if clust in lst_clstrs:
                    rec_typs = ','.join([x.record_type for x in lst])
                    if len(lst_clstrs) > 1 or 'S' in rec_typs:
                        merge_records.append(lst)
                        for_merging = True

            # Do the merges
            if for_merging:
                pass
            else:
                self.hg_counter += 1
                for mem in self.clusters[clust]:
                    self.hgs.add_member(mem, self.hg_counter)
                        
    def render(self, outfile):
        """ Renders a PDF of the tree annotated with the homology groups """
        
        self.tree.ladderize()
        random_color = RandomColor()
        colors = {k: random_color.generate() for k in self.hgs.homology_groups}
        
        # Get the duplications and specieation nodes
        dups = self.tree.search_nodes(evoltype='D')
        specs = self.tree.search_nodes(evoltype='S')
            
        # Color node according to the event type
        for node in dups + specs:
            if not node.is_leaf():
                if node.evoltype == 'D':
                    node.img_style['fgcolor'] = 'Red'
                elif node.evoltype == 'S':
                    node.img_style['fgcolor'] = 'Blue'
                node.img_style['shape'] = 'square'
        
        def layout_fn(node):
            if node.is_leaf():
                species_face = AttrFace('species', fsize=12)
                add_face_to_node(species_face, node, 1, position='aligned')
                try:
                    hg_id = '- %s -' % self.hgs.member_to_hg_mapping[node.name]
                except KeyError:
                    hg_id = ''
                hg_face = TextFace(hg_id, fsize=12)
                add_face_to_node(hg_face, node, 2, position='aligned')
                name_face = AttrFace('strain_name', fsize=12)
                try:
                    bg_color = colors[self.hgs.member_to_hg_mapping[node.name]]
                    name_face.background.color = bg_color
                except KeyError:
                    pass
                add_face_to_node(name_face, node, 3, position='aligned')
                cluster_face = TextFace('- %s' % node.cluster, fsize=12)
                add_face_to_node(cluster_face, node, 4, position='aligned')
        
        ts = TreeStyle()
        ts.draw_guiding_lines = True
        ts.layout_fn = layout_fn
        ts.show_scale = False
        ts.show_leaf_name = False
        self.tree.render(outfile, w=3000, units='mm', tree_style=ts)