from django.conf import settings
from Bio import SeqIO
import requests
import json
import glob
import os
import re


def find_additional_ushers():
    # Get the list of the gene IDs of the existing ushers
    print 'Building the list of existing ushers...'
    bacteria_ushers_dir = os.path.join(settings.PILIOMEDB_FILES, 'bacteria_ushers')
    bacteria_ushers = []
    bacteria_ushers_files = glob.glob(os.path.join(bacteria_ushers_dir, 'ushers_*.faa'))
    for usher_file in bacteria_ushers_files:
        seq = SeqIO.index(usher_file, 'fasta')
        seq_keys = [x for x in seq.keys()]
        bacteria_ushers += seq_keys
    print 'There are %s existing ushers.' % len(bacteria_ushers)
    # List of additional ushers
    additional_ushers = []
    # Find additional ushers from the accompanying annotation files of the genomes
    print 'Searching for additional ushers from the info.xls files...'
    genbank_files_dir = os.path.join(settings.PILIOMEDB_FILES, 'genbank_files')
    annots_files = glob.glob(os.path.join(genbank_files_dir, '*.info.xls'))
    # Add more annotation files to the list from the genomes folder
    genomes_dir = os.path.join(settings.PILIOMEDB_FILES, 'genomes')
    genomes = glob.glob(os.path.join(genomes_dir, '*'))
    for genome in genomes:
        taxon_id = os.path.split(genome)[-1]
        cog_file = os.path.join(genome, '%s.cog.tab.txt' % taxon_id)
        pfam_file = os.path.join(genome, '%s.pfam.tab.txt' % taxon_id)
        gff_file = os.path.join(genome, '%s.gff' % taxon_id)
        if os.path.exists(gff_file):
            if os.path.exists(cog_file):
                annots_files.append(cog_file)
            if os.path.exists(pfam_file):
                annots_files.append(pfam_file)
    acount = 0
    annots_files_count = len(annots_files)
    for annot in annots_files:
        acount += 1
        filename = os.path.split(annot)[-1]
        taxon_id = filename.split('.')[0]
        print ' %s/%s - Dealing with %s' % (acount, annots_files_count, filename)
        if os.path.exists(annot):
            annots_file = open(annot, 'r')
            annots_file = annots_file.readlines()
            for annots_line in annots_file:
                if annots_line:
                    res = re.search('pfam00577|COG3188', annots_line)
                    if res:
                        gene_id = annots_line.split()[0]
                        if gene_id not in bacteria_ushers and gene_id not in additional_ushers:
                            additional_ushers.append((gene_id, taxon_id))
        else:
            print '  The file does not exist!'
    print 'There are %s additional ushers.' % len(additional_ushers)
    return additional_ushers
    

def write_sequence_of_additional_ushers(additional_ushers, outfile):
    print 'Writing the sequences of additional ushers into the outfile...'
    ucount = 0
    additional_ushers_count = len(additional_ushers)
    outfile = open(outfile, 'w')
    for gene_id, taxon_id in additional_ushers:
        ucount += 1
        print ' %s/%s - Dealing with %s from taxon %s' % (ucount, additional_ushers_count, gene_id, taxon_id)
        url = 'http://localhost:8000/imgrepl/?section=GeneDetail&page=geneDetail&gene_id=%s&taxon_id=%s' % (gene_id, taxon_id)
        res = requests.get(url)
        res = res.content
        details = json.loads(res)
        seq = '>%s\n%s\n' % (details['fasta_description'], details['amino_acid_sequence'])
        outfile.write(seq)
    outfile.close()
    print 'Writing of sequences done!'