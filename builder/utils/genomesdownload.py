from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import os
import json


"""
The following codes were executed in a live python interpreter.
"""


    
browser = webdriver.Firefox()
t4d = json.load(open('/Users/joemartaganna/Desktop/taxa4download.json'))
gmap = {}
errors = []

for t in t4d:
    count += 1
    print count, len(t4d)
    if t not in gmap.keys():
        try:
            url = 'https://img.jgi.doe.gov/cgi-bin/er/main.cgi?section=TaxonDetail&page=taxonDetail&taxon_oid=%s' % t
            browser.get(url)
            html = browser.page_source
            soup = BeautifulSoup(html)
            download_href = soup.find_all('a', attrs={'class': 'download-btn'})[0]['href']
            gmap[t] = download_href
        except IndexError:
            errors.append(t)
    
    

def download_genome(img_taxon_id, genome_shortcode=None, source='genome.jgi', browser=None):
    """
    Downloads the genome -- either the all-in-one folder from Genome.JGI or the Genbank
    file from IMG/ER
    """
    if source == 'genome.jgi':
        base_url = 'http://genome.jgi.doe.gov/'
        url = base_url + '%s/download/_IMG2/img_web_data/download/%s.tar.gz' % (genome_shortcode, img_taxon_id)
        os.system('wget %s' % url)
        os.system('tar xvzf *.tar.gz')
        os.system('rm *.tar.gz')
    elif source == 'img_er':
        base_url = 'https://img.jgi.doe.gov/cgi-bin/er/main.cgi?'
        url = base_url + 'section=TaxonDetail&page=taxonDetail&taxon_oid=%s' % img_taxon_id
        browser.get(url)
        buttons = browser.find_elements_by_class_name('lgbutton')
        if buttons[-1].get_attribute('value') == 'Generate Genbank File':
            # Generate and download the Genbank file
            buttons[-1].click()
            select = Select(browser.find_element_by_name('scaffold_oid'))
            for option in select.options:
                option.click()
            browser.find_element_by_name('imgTermOverride').click()
            browser.find_element_by_name('gene_oid_note').click()
            browser.find_element_by_name('misc_features').click()
            browser.find_element_by_name('_section_TaxonDetail_processArtemisFile').click()
            try:
                browser.find_element_by_name('_section_TaxonDetail_downloadArtemisFile_noHeader').click()
            except NoSuchElementException:
                pass
            # Generate the download the gene annotations
            annot_url = base_url + 'section=GeneInfoPager&page=viewGeneInformation&taxon_oid=%s' % img_taxon_id
            browser.get(annot_url)
            annot_href = 'main.cgi?section=TaxonDetail&downloadTaxonInfoFile=1&taxon_oid=%s&_noHeader=1' % img_taxon_id
            xpath_string = "//a[@href='%s']" % annot_href
            browser.find_element_by_xpath(xpath_string).click()
        else:
            print 'Generate Genbank File button was not found!'


withhtmlend = [x for x in gmap if '.html' in gmap[x]]
    
for k,v in gmap.items():
    if '.html' in v:
        sc = v.split('/')[3]
        count += 1
        print '\n\n********** %s/%s **********\n\n' % (count, len(withhtmlend))
        if k not in glob.glob('*'):
            download_genome(sc,k)
        else:
            print 'This genome has been downloaded'