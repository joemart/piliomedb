from django.conf import settings
from Bio import SeqIO
import redis
import re
import os


class GenomeIO(object):
    # TODO -- Refactor the parsing methods to minimize the overhead of parsing and loading files
    # in every request for a target gene. This can be done by either caching each genome object
    # or fetching only the info for the target gene and discarding the rest.
    
    
    def __init__(self, genome_path, target_gene, source='IMG'):
        self.genome_path = genome_path
        self.target_gene = target_gene
        self.source = source
        self.genes = {}
        if self.source == 'IMG':
            if '.gbk' in genome_path:
                self.taxon_id = os.path.split(self.genome_path)[-1].split('.')[0]
                self.parse_genbank()
                self.load_annotations()
            else:
                self.taxon_id = os.path.split(self.genome_path)[-1]
                self.load_gff()
                self.load_aa_sequences()
                self.load_pfam_annotations()
                self.load_cog_annotations()
        
    
    def __getitem__(self, k):
        return self.genes[k]
        
        
    def __iter__(self):
        for k in self.genes:
            yield k
    
    
    def parse_genbank(self):
        seq = SeqIO.parse(self.genome_path, 'genbank')
        for record in seq:
            for feature in record.features:
                if feature.type == 'source':
                    organism = feature.qualifiers['organism'][0]
                if feature.type == 'CDS':
                        q = feature.qualifiers
                        gene_id = q['note'][0].split('=')[-1]
                        if gene_id == self.target_gene:
                            gene_start = feature.location.start.position
                            gene_end = feature.location.end.position
                            dna_length = len(q['translation'][0]) * 3
                            if feature.strand == 1:
                                dna_coords = '%s..%s (+)(%sbp)' % (gene_start, gene_end, dna_length)
                                strand = '+'
                            elif feature.strand == -1:
                                dna_coords = '%s..%s (+)(%sbp)' % (gene_start, gene_end, dna_length)
                                strand = '-'
                            if 'locus_tag' in  q.keys():
                                locus_tag = q['locus_tag'][0]
                            else:
                                locus_tag = ''
                            data = {
                                'product_name': q['product'][0],
                                'locus_tag': locus_tag,
                                'amino_acid_sequence': q['translation'][0],
                                'amino_acid_sequence_length': len(q['translation'][0]),
                                'fasta_description': '%s %s %s [%s]' % (gene_id, record.id, q['product'][0], organism),
                                'dna_coordinates': dna_coords,
                                'gene_id': gene_id,
                                'scaffold_name': record.id,
                                'img_orf_type': 'CDS'
                            }
                            self.genes[gene_id] = data
        # Remove the seq object from memory
        del seq
        
        
    def load_annotations(self):
        annots_table = os.path.join(os.path.split(self.genome_path)[0], '%s.info.xls' % self.taxon_id)
        annots = open(annots_table, 'r').readlines()
        
        for line in annots:
            
            gene_id = line.strip().split('\t')[0]
            
            if gene_id == self.target_gene:
            
                if re.search('pfam(\d)',line):
                    gene_id, locus_tag, pfam_id, pfam_name, blank, score = line.strip().split('\t')
                    try:
                        self.genes[gene_id]['pfam'] = '%s; %s - %s' % (self.genes[gene_id]['pfam'], pfam_id, pfam_name)
                    except KeyError:
                        try:
                            self.genes[gene_id]['pfam'] = '%s - %s' % (pfam_id, pfam_name)
                        except KeyError:
                            pass
                
                if re.search('COG(\d)', line):
                    gene_id, locus_tag, cog_id, cog_name, blank, score = line.strip().split('\t')
                    try:
                        self.genes[gene_id]['cog'] = '%s; %s - %s' % (self.genes[gene_id]['cog'], cog_id, cog_name)
                    except KeyError:
                        try:
                            self.genes[gene_id]['cog'] = '%s - %s' % (cog_id, cog_name)
                        except KeyError:
                            pass
                
            #if gene_id == self.target_gene:
            #   break
        # Delete the annots objects from memory
        del annots
    
    
    def load_gff(self):
        gff_file = os.path.join(self.genome_path, '%s.gff' % self.taxon_id)
        gff_file = open(gff_file, 'rb')
        #taxon_name = self.get_taxon_name()
        for line in gff_file.readlines():
            if '#' not in line:
                data_contents = line.split('\t')
                del data_contents[5] # Remove the score (all set at .)
                del data_contents[6] # Remove the phase (all set at 0)
                desc = data_contents.pop(-1).strip().split(';')
                try:
                    data_contents.append(desc[1].split('locus_tag=')[-1])
                except IndexError:
                    data_contents.append('')
                try:
                    data_contents.append(desc[2].split('product=')[-1])
                except IndexError:
                    data_contents.append('')
                data_headers = ['scaffold_name', 'img_version', 'img_orf_type', 'gene_start',
                'gene_end', 'strand', 'locus_tag', 'product_name']
                data = dict(zip(data_headers, data_contents))
                start, end = (data['gene_start'], data['gene_end'])
                bplength = abs(int(start) - int(end)) + 1
                if data['strand'] == '+':
                    coord_format = (start, end, bplength) 
                    data['dna_coordinates'] = '%s..%s (+)(%sbp)' % coord_format
                elif data['strand'] == '-':
                    coord_format = (start, end, bplength, start, end) 
                    data['dna_coordinates'] = '%s..%s (-)(%sbp) [ complement(%s..%s) ]' % coord_format
                #data['taxon_name'] = taxon_name
                del data['gene_start']
                del data['gene_end']
                del data['img_version']
                del data['strand']
                gene_id = desc[0].split('ID=')[-1]
                data['gene_id'] = gene_id
                self.genes[gene_id] = data
                if gene_id == self.target_gene:
                    break
    
    
    def load_aa_sequences(self):
        aa_seqs_file = os.path.join(self.genome_path, '%s.genes.faa' % self.taxon_id)
        aa_seqs = SeqIO.parse(aa_seqs_file, 'fasta')
        for aa in aa_seqs:
            try:
                self.genes[aa.id]['fasta_description'] = aa.description
                self.genes[aa.id]['amino_acid_sequence'] = aa.seq.tostring()
                self.genes[aa.id]['amino_acid_sequence_length'] = len(aa.seq)
                # Break if target gene is found
                if aa.id == self.target_gene:
                    break
            except KeyError:
                pass
            
    
    def load_pfam_annotations(self):
        pfam_file = os.path.join(self.genome_path, '%s.pfam.tab.txt' % self.taxon_id)
        if os.path.exists(pfam_file):
            pfam_file = open(pfam_file, 'rb')
            for line in pfam_file.readlines()[1:]:
                gene_id = line.split('\t')[0]
                pfam_id, pfam_name = line.split('\t')[8], line.split('\t')[9]
                try:
                    self.genes[gene_id]['pfam'] = '%s; %s - %s' % (self.genes[gene_id]['pfam'], pfam_id, pfam_name)
                except KeyError:
                    try:
                        self.genes[gene_id]['pfam'] = '%s - %s' % (pfam_id, pfam_name)
                    except KeyError:
                        pass
                # Break if target gene is found
                if gene_id == self.target_gene:
                    break
        
        
    def load_cog_annotations(self):
        cog_file = os.path.join(self.genome_path, '%s.cog.tab.txt' % self.taxon_id)
        if os.path.exists(cog_file):
            cog_file = open(cog_file, 'rb')
            for line in cog_file.readlines()[1:]:
                gene_id = line.split('\t')[0]
                cog_id, cog_name = line.split('\t')[9], line.split('\t')[10]
                try:
                    self.genes[gene_id]['cog'] = '%s; %s - %s' % (self.genes[gene_id]['cog'], cog_id, cog_name)
                except KeyError:
                    try:
                        self.genes[gene_id]['cog'] = '%s - %s' % (cog_id, cog_name)
                    except KeyError:
                        pass
                # Break if target gene is found
                if gene_id == self.target_gene:
                    break
                
                
    def get_taxon_name(self):
        fna_file = os.path.join(self.genome_path, '%s.fna' % self.taxon_id)
        fna_file = open(fna_file, 'rb')
        taxon_name_line = fna_file.readline()
        taxon_name = taxon_name_line.split(':')[0].strip()
        taxon_name = [x for x in taxon_name.split() if '>' not in x]
        return ' '.join(taxon_name)