"""
Search for ushers with a chaperone nearby (within up to 5 ORFs up or down)
"""

from django.db.models import Q
from data.models import Gene
import re


def execute_cu_search():
    """
    This function has been added to tasks.py so this can be executed
    in the background in parallel.
    """

    # List all Pfam and COG IDs for chaperones
    chaperones = ['pfam00345', 'pfam02753', 'COG3121']
    
    # List of all Pfam and COG IDs for fimbrial subunits / adhesins
    subunits = ['pfam00419', 'pfam04619', 'pfam03627', 'pfam05229', 'pfam06551', 'COG5430', 'COG3539']

    # Fetch all the ushers
    ushers = Gene.objects.filter(piliome_annotation='usher').values_list('gene_id')
    ushers_count = ushers.count()
    count = 0
    for usher in ushers:
        usher = usher[0]
        usher_gene = Gene.objects.filter(gene_id=usher)[0]
        usher_scaffold = usher_gene.scaffold_name
        # Encode all genes in the cluster into a code word
        # U for ushers, C for chaperones, and X for others
        count += 1
        print '\n%s out of %s\n' % (count, ushers_count)
        print '  Dealing with %s' % usher
        
        coded_genes = ''
        print '  Fetching cluster...'
        cluster = Gene.objects.filter(Q(id=usher_gene.id) | Q(associated_usher__gene_id=usher))
        print '  This cluster has %s gene(s)' % cluster.count() 
        print '\n  Iterating over the genes in the cluster:'
        gcount = 0
        if cluster.count() > 1:
            cluster = cluster.order_by('cu_cluster_position')
            for gene in cluster:
                gcount += 1
                gene_scaffold = gene.scaffold_name
                if usher_scaffold == gene_scaffold:
                    gene_code = 'X' # Default code
                else:
                    gene_code = 'x'
                
                print '    %s - Checking if gene is an usher...' % gcount
                # Check if this gene is an usher
                gene_records = Gene.objects.filter(gene_id=gene.gene_id)
                gene_annotations = [x.piliome_annotation for x in gene_records]
                if 'usher' in gene_annotations:
                    if usher_scaffold == gene_scaffold:
                        if gene.gene_id == usher:
                            gene_code = '[U]'
                        else:
                            gene_code = 'U'
                    else:
                        if gene.gene_id == usher:
                            gene_code = '[U]'
                        else:
                            gene_code = 'u'
                
                print '    %s - Checking if gene is a chaperone...' % gcount
                # Check if this gene is a chaperone
                annotations = []
                if gene.pfam:
                    for pf in gene.pfam.split(';'):
                        pf = pf.split('-')[0].strip()
                        #print pf
                        annotations.append(pf)
                if gene.cog:
                    for cg in gene.cog.split(';'):
                        cg = cg.split('-')[0].strip()
                        #print cg
                        annotations.append(cg)
                if set(annotations).intersection(set(chaperones)):
                    if usher_scaffold == gene_scaffold:
                        gene_code = 'C'
                    else:
                        gene_code = 'c'

                # Add the gene_code to the coded_genes
                coded_genes += gene_code
        
        # Save this coded_genes to database
        usher_gene.cluster_coded_annotation = coded_genes
        usher_gene.save()
        
		# Print the coded_genes
        print '\n  Code: %s' % coded_genes
        
        
def find_chaperones_and_split_ushers(usher):
    """
    Search for the chaperone in the cluster, which in most cases will be as 
    simple as finding C in the coded annotations but becomes more complicated
    if another usher is also found within the cluster
    """
    cca = usher.cluster_coded_annotations
    cluster = usher.get_cluster()
    print 'Finding CU pairs...'
    res = re.finditer("C*X{0,3}U*[UM]U*X{0,3}C*", cca)
    print res
    for x in res:
        pair = x.group(0)
        print pair
        if 'M' in pair:
            mindex = pair.index('M') + x.start()
            ccount = 0
            for code in pair:
                if code == 'C':
                    print 'Chaperone found'
                    cindex = ccount + x.start()
                    chap = cluster[cindex]
                    chap.piliome_annotation = 'chaperone'
                    chap.save()
                elif code == 'U':
                    print 'Usher fragment found'
                    uindex = ccount + x.start()
                    ush = cluster[uindex]
                    ush.piliome_annotation = 'usher'
                    if ush.amino_acid_sequence_length <= 500:
                        ush.is_usher_fragment = True
                    ush.save()
                    if ush.is_usher_fragment and usher.amino_acid_sequence_length <= 500:
                        usher.is_usher_fragment = True
                        usher.save()
                ccount += 1
