from Bio.Blast.Applications import NcbiblastnCommandline, NcbiblastpCommandline 
from data.models import Gene, NonRedundantUsher, UsherHomologyGroup, ReferenceLink, LiteratureSearched
#from data.utils.pubmed_fetcher import FetchPubmed
from Bio.Alphabet import IUPAC
from Bio.Blast import NCBIXML
from Bio.Seq import Seq
from Bio import Alphabet
from django.conf import settings
from cStringIO import StringIO
import tempfile
import logging
import json
import os
    
    

class BlastP(object):
    
    def __init__(self, sequence, db='reps_prot', identity_cutoff=95, fetcher=None):
        """
        Initializes the object
        """
        self.identity_cutoff = identity_cutoff
        self.fetcher = fetcher
        self.raw_sequence = sequence
        if '>' in sequence:
            self.accession = sequence.strip().splitlines()[0]
            self.sequence = ''.join(sequence.strip().splitlines()[1:])
        else:
            self.sequence = ''.join(sequence.strip().split())
        if db == 'ushers':
            self.db = 'reps_prot'
        else:
            self.db = db
        self.db_dir = os.path.join(settings.BASE_DIR, 'misc', 'blast_db') 
        self.generate_db()
        self.run_search()
        self.parse_results()
    
    def generate_db(self):
        """
        Generates the BLAST databases if they don't exist yet
        """
        
        self.prot_db = os.path.join(self.db_dir, self.db)
        #self.nuc_db = os.path.join(self.db_dir, 'reps_nuc')
        
        if not os.path.exists(self.prot_db + '.fasta'):
            print 'Writing the sequence file...'
            # Open streams for the files to write
            prot_seq = open(self.prot_db + '.fasta', 'w')

            if self.db == 'reps_prot':
                ushers = NonRedundantUsher.objects.all()
                for usher in ushers:
                    #rep = pt.representative
                    # Write the usher sequences
                    if self.db == 'reps_prot':
                        usher_prot = usher.representative.get_fasta_sequence()
                    else:
                        usher_prot = usher.get_fasta_sequence()
                    prot_seq.write(usher_prot)
                    
            elif self.db == 'adhesins':
                # Write the adhesin sequences
                adhesins = Gene.objects.filter(piliome_annotation='adhesin', associated_usher__isnull=False)
                for adhesin in adhesins:
                    adhesin_prot = adhesin.get_fasta_sequence(indicate_usher=True)
                    prot_seq.write(adhesin_prot)

            elif self.db == 'chaperones':
                # Write the chaperone sequences
                chaperones = Gene.objects.filter(piliome_annotation='chaperone', associated_usher__isnull=False)
                for chaperone in chaperones:
                    chaperone_prot = chaperone.get_fasta_sequence(indicate_usher=True)
                    prot_seq.write(chaperone_prot)
                    
            elif self.db == 'major_subunits':
                # Write the chaperone sequences
                subunits = Gene.objects.filter(piliome_annotation='major subunit', associated_usher__isnull=False)
                for subunit in subunits:
                    subunit_prot = subunit.get_fasta_sequence(indicate_usher=True)
                    prot_seq.write(subunit_prot)

            prot_seq.close()
            print 'Producing the non-redundant sequence file...'
            nr_output = prot_seq.name.split('.')[0] + '_nr100.fasta'
            os.system('/usr/local/bin/cd-hit -i %s -o %s -c 1.0' % (prot_seq.name, nr_output))
            print 'Making the BLAST db...'
            os.system('/usr/local/bin/makeblastdb -in %s -dbtype prot -out %s' % (nr_output, self.prot_db))         
            
    def run_search(self):
        """
        Parses the input sequence and runs the appropriate BLAST program
        """
        alphabet = 'protein'
        # Create file paths
        qfile = tempfile.NamedTemporaryFile(delete=True)
        query = qfile.name + '.fasta'
        out = qfile.name + '.xml'
        qfile.close()
        self.results = out
        # Write the query to file
        query_out = open(query, 'w')
        query_out.write(self.sequence)
        query_out.close()
        # Call the appropriate BLAST program
        if alphabet == 'unambiguous_dna':
            blast_cline = NcbiblastnCommandline(
                    query=query, 
                    db=self.nuc_db, 
                    evalue=0.001, 
                    outfmt=5, 
                    out=out
                )
        elif alphabet == 'protein':
            blast_cline = NcbiblastpCommandline(
                    query=query, 
                    db=self.prot_db, 
                    evalue=0.001, 
                    outfmt=5, 
                    out=out
                )
        # Execute the search
        blast_cline()
                    
    def parse_results(self):
        """
        Reads and parses the results of the search
        """
        if self.results:
            blast_record = NCBIXML.read(open(self.results, 'r'))
            self.blast_record = blast_record
        else:
            self.blast_record = None
        
    def get_uhg_hits(self):
        self.uhgs = UsherHomologyGroup.objects.all()
        uhg_list = []
        match_list = []
        for alignment in self.blast_record.alignments:
            for hsp in alignment.hsps:
                percent_id = round(float(hsp.identities)/hsp.align_length, 3) * 100
                if hsp.expect < 0.0001 and percent_id > self.identity_cutoff:
                    match_list.append({'alignment': alignment, 'hsp': hsp})
                    if self.db == 'ushers':
                        uhg = self.uhgs.filter(members__contains=alignment.title.split()[-1])
                    else:
                        u = alignment.title.split()[-1].split('usher:')[-1]
                        uhg = self.uhgs.filter(all_members__contains=u)
                    if uhg:
                        uhg_id = int(uhg[0].id)
                        uhg_list.append(uhg_id)
        if match_list:
            self.match = match_list[0]
        else:
            self.match = None
        return uhg_list
        
    def save_paper(self, accession, uhg_id):
        ls = LiteratureSearched.objects.filter(proteins__contains="'%s'" % accession)[0]
        pmid = ls.pubmed_id
        rlcheck = ReferenceLink.objects.filter(uhg__id=uhg_id, reference_paper__pubmed_id=pmid, reference_sequence__accession=accession)
        if not rlcheck:
            prot = {'accession': self.accession.split('|')[1], 'sequence': self.raw_sequence, 'length': len(self.sequence)}
            f = self.fetcher(pmid, proteins=[prot], uhg=uhg_id, match=self.match)
            sr = f.save_to_database()
            return sr[0]
        else:
            return 'exists'
            
    def save_to_database(self, verbose=False):
        uhgs = self.get_uhg_hits()
        #if len(set(uhgs)) == 1:
        if uhgs:
            if verbose:
                print 'UHG hit found!'
            acc = self.accession.split('|')[1]
            sr = self.save_paper(acc, uhgs[0])
            if verbose:
                if sr == 'saved':
                    print 'Paper and associated proteins and/or structures were saved to database!'
                elif sr == 'exists':
                    print 'This paper already exists in the database.'
            return sr
    
            
def download_all_prots_sequence(outfile, infile=None):
    import time
    import json
    from Bio import Entrez
    Entrez.email = "voidfunc84@gmail.com"
    if infile:
        prots = json.load(open(infile))
    else:
        lits = LiteratureSearched.objects.all()
        prots = []
        for lit in lits:
            if lit.proteins:
                prots += eval(lit.proteins)
        prots = list(set(prots))
    outf = open(outfile, 'a')
    fetched_prots = []
    for i, p in enumerate(prots):
        time.sleep(2)
        print i+1, len(prots), '--', p
        while True:
            try:
                if p not in fetched_prots:
                    seq = Entrez.efetch(db='protein', id=p, rettype='fasta')
                    seq = seq.read()
                    outf.write(seq)
                    fetched_prots.append(p)
            except:
                print ' Error: Will retry after 15 seconds...'
                time.sleep(15)
                continue
            break
    outf.close()
    
                