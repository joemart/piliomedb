from data.models import PiliomeDBVersion, Gene


"""
This script was run to provide data for one column in the Gene table
to indicate if all the genes in the CU gene cluster (the usher +10 and -10)
are located in the same scaffold
"""

from data.models import Gene


def update_ushers():
    ushers = Gene.objects.filter(piliome_annotation='usher')
    count = 0
    for usher in ushers:
        count += 1
        print count, len(ushers)
        cluster = Gene.objects.filter(associated_usher=usher)
        cluster_in_same_scaffold = True
        scaffold_name = usher.scaffold_name
        for gene in cluster:
            if gene.scaffold_name != scaffold_name:
                cluster_in_same_scaffold = False
                break
        usher.cluster_in_same_scaffold = cluster_in_same_scaffold
        usher.save()
        
        

def ushers_with_missing_scaffolds():
    dbversion = PiliomeDBVersion.objects.filter(db_version='0.1')[0]
    scaffolds_with_details = dbversion.scaffolds.get_query_set()
    swd = [x.scaffold_name for x in scaffolds_with_details]
    ums = dbversion.genes.filter(piliome_annotation='usher', associated_usher__isnull=True).exclude(scaffold_name__in=swd)
    return ums