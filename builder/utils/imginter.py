from bs4 import BeautifulSoup
from data.models import Gene
from django.conf import settings
import requests
import urllib2
import redis
import json
import os


class ScrapeIMG(object):
    """
    Object for scraping data from IMG
    """
    
    def __init__(self, query_type, query_img_id, taxon_id=None, fetch_aa_seq=False, use_tor=False, fetch_from='img', browser=None):
        self.query_type = query_type
        self.query_img_id = query_img_id
        self.taxon_id = taxon_id
        self.fetch_aa_seq = fetch_aa_seq
        self.use_tor = use_tor
        self.fetch_from = fetch_from
        self.browser = browser
        if fetch_from == 'img':
            self.scrape()
            if self.status_code == 200:
                if query_type == 'gene':
                    self.get_gene_info()
                elif query_type == 'taxon':
                    self.get_taxon_info()
                elif query_type == 'scaffold':
                    self.get_scaffold_info()
        else:
            self.rc = redis.Redis(connection_pool=settings.REDIS_CONNECTION_POOL)
            self.get_gene_info()
        
        
    def send_request(self, url):
        print url
        if self.browser:
            self.browser.get(url)
            content = self.browser.page_source
            status_code = 200
        else:
            if self.use_tor:
                # Using urllib2 with Privoxy and TOR
                proxy_support = urllib2.ProxyHandler({"http" : "127.0.0.1:8118"})
                opener = urllib2.build_opener(proxy_support)
                headers = ('User-Agent', 'Mozilla/5.0 (Windows NT 5.1; rv:26.0) Gecko/20100101 Firefox/26.0')
                opener.addheaders = [headers]
                r = opener.open(url)
                status_code = r.getcode()
                if r.getcode() == 200:
                    content = r.read()
                else:
                    content = None
            else:
                headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 5.1; rv:26.0) Gecko/20100101 Firefox/26.0'}
                proxy_dict = {
                    'http': 'http://63.249.146.73:29786' 
                }
                r = requests.get(url, headers=headers) #, proxies=proxy_dict)
                status_code = r.status_code
                if r.status_code == 200:
                    content = r.content
                else:
                    content = None
        self.content = content
        return {'status_code': status_code, 'content': content}
        
        
    def scrape(self):
        # Build the url for the GET request
        db = 'w' #'w'
        if db == 'w':
            self.base_url = 'https://img.jgi.doe.gov/cgi-bin/w/main.cgi?'
        elif db == 'er':
            self.base_url = 'https://img.jgi.doe.gov/cgi-bin/er/main.cgi?'
        if self.query_type == 'gene':
            url = self.base_url + 'section=GeneDetail&page=geneDetail&gene_oid=%s' % self.query_img_id
        elif self.query_type == 'taxon':
            url = self.base_url + 'section=TaxonDetail&page=taxonDetail&taxon_oid=%s' % self.query_img_id
        elif self.query_type == 'scaffold':
            url = self.base_url + 'section=ScaffoldGraph&page=scaffoldDetail&scaffold_oid=%s' % self.query_img_id
        self.url = url
        req = self.send_request(url)
        self.status_code = req['status_code']
        if req['status_code'] == 200:
            soup = BeautifulSoup(req['content'])
            self.rows = soup.findChildren('tr', attrs={'class':'img'})
    
    
    def get_gene_info(self):
        details = {}
        if self.fetch_from == 'img':
            headers = ['Gene ID', 'Gene Symbol', 'Locus Tag', 'Display Name', 'Product Name', 'SwissProt Protein Product',
            'SEED', 'IMG Term', 'DNA Coordinates', 'Scaffold Source', 'IMG ORF Type', 'External Links',
            'Fused Gene', 'Amino Acid Sequence Length', 'COG', 'KOG', 'Transmembrane Helices', 'Signal Peptide',
            'Pfam', 'Transport Classification']
            for row in self.rows[0:25]:
                row_list = row.text.strip().split('\n')
                th = row_list[0].strip()
                if th in headers:
                    if  th == 'Scaffold Source':
                        scname = '; '.join([x.strip() for x in row_list[1:] if x])
                        details['scaffold_name'] = scname.split(':')[-1].strip().split()[0]
                        details['taxon_name'] = scname.split(':')[0].strip()
                    elif th == 'Amino Acid Sequence Length':
                        details['amino_acid_sequence_length'] = int(row_list[-1].strip('aa'))
                    else:
                        details[th.lower().replace(' ', '_')] = '; '.join([x.strip() for x in row_list[1:] if x])
                if '>Genome</th>' in str(row):
                    details['taxon_id'] = row.td.a['href'].split('=')[-1]
                if '>Scaffold Source</th>' in str(row):
                    details['scaffold_id'] = row.td.a['href'].split('scaffold_oid=')[-1].split('&start_coord')[0]
                if '>Amino Acid Sequence Length</th>' in str(row) and self.fetch_aa_seq:
                    seq_url = '/'.join(self.base_url.split('/')[0:-1]) + '/' + row.td.a['href']
                    seq_content = self.send_request(seq_url)['content']
                    aa_soup = BeautifulSoup(seq_content)
                    aa_seq = aa_soup.findChildren('div', attrs={'id': 'content_other'})[0].pre.text.strip()
                    details['fasta_description'] = aa_seq.split('\n')[0].strip('>')
                    details['amino_acid_sequence'] = ''.join(aa_seq.split('\n')[1:])
            self.details = details
            #del self.content
            #self.rows
        
        elif self.fetch_from == 'imgrepl':
            
            if self.taxon_id:
                taxon_id = self.taxon_id
            else:
                try:
                    gene = Gene.objects.filter(gene_id__icontains=str(self.query_img_id)[0:-2])
                    taxon_id = gene[0].taxon_id
                except IndexError:
                    additional_ushers = json.load(open(os.path.join(settings.PILIOMEDB_FILES, 'bacteria_ushers', 'additional_ushers.json')))
                    taxon_map = {x[0]:x[1] for x in additional_ushers}
                    taxon_id = taxon_map[self.query_img_id]
            url = 'http://localhost:8000/imgrepl/?section=GeneDetail&gene_id=%s&taxon_id=%s' % (self.query_img_id, taxon_id)
            r = requests.get(url)
            if r.status_code == 200:
                self.details = json.loads(r.content)
                if 'taxon_id' not in self.details.keys():
                    self.details['taxon_id'] = self.taxon_id
            else:
                self.details = {}
            self.status_code = r.status_code
            
                
    def get_taxon_info(self):
        details = {}
        strain_details = {}
        headers = ['Organism Name', 'Taxon ID', 'IMG Submission ID', 'NCBI Taxon ID', 'External Links', 
        'Genome Type', 'Lineage', 'Sequencing Status', 'Release Date', 'Add Date', 'Obsolete Flag', 
        'IMG Product Flag', 'Is Public', 'GOLD ID', 'Isolation Country', 'NCBI Project ID', 
        'GOLD Sequencing Status', 'Sequencing Center', 'Assembly Method', 'Gram Staining', 'Host Name',
        'Biotic Relationships', 'Cell Shape', 'Isolation', 'Habitat', 'Oxygen Requirement', 'Temperature Range',
        'Relevance', 'Diseases', 'Energy Source']
        for row in self.rows:
            row_list = row.text.strip().split('\n')
            th = row_list[0].strip()
            if th in headers:
                if th == 'Organism Name':
                    strain_details['name'] = '; '.join([x.strip() for x in row_list[1:] if x])
                elif th == 'Release Date':
                    strain_details['img_release_date'] = '; '.join([x.strip() for x in row_list[1:] if x])
                elif th == 'Add Date':
                    strain_details['img_add_date'] = '; '.join([x.strip() for x in row_list[1:] if x])
                elif th == 'Taxon ID':
                    strain_details['img_taxon_id'] = '; '.join([x.strip() for x in row_list[1:] if x])
                elif th == 'Lineage':
                    taxon_level_details = {}
                    lineage = row.td.find_all('a')
                    for l in lineage:
                        taxon_level, name = l['href'].split('&')[-1].split('=')
                        taxon_level_details[taxon_level.split('_')[-1]] = name
                    details['lineage'] = taxon_level_details
                elif th == 'Oxygen Requirement':
                    if 'PIPELINE' not in row.td.text:
                        strain_details['oxygen_requirement'] = '; '.join([x.strip() for x in row_list[1:] if x])
                else:
                    strain_details[th.lower().replace(' ', '_')] = '; '.join([x.strip() for x in row_list[1:] if x])
        details['strain'] = strain_details
        self.details = details
        #del self.content
        del self.rows
        
        
    def get_scaffold_info(self):
        details = {}
        headers = ['Scaffold ID', 'Scaffold Name', 'Topology', 'Type', 'Sequence Length', 'GC Content',
        'Gene Count', 'RNA Count', 'Add Date', 'Last Update']
        for row in self.rows[0:25]:
            row_list = row.text.strip().split('\n')
            if row_list[0] in headers:
                if row_list[0] == 'Type':
                    details['scaffold_type'] = '; '.join([x.strip() for x in row_list[1:] if x])
                elif row_list[0] == 'Add Date':
                    details['img_add_date'] = '; '.join([x.strip() for x in row_list[1:] if x])
                elif row_list[0] == 'Last Update':
                    details['img_last_update'] = '; '.join([x.strip() for x in row_list[1:] if x])
                elif row_list[0] == 'Sequence Length':
                    details['sequence_length'] = int(row_list[-1])
                elif row_list[0] == 'GC Content':
                    details['gc_content'] = float(row_list[-1])
                elif row_list[0] == 'Gene Count':
                    details['gene_count'] = int(row_list[-1])
                elif row_list[0] == 'RNA Count':
                    try:
                        details['rna_count'] = int(row_list[-1])
                    except ValueError:
                        details['rna_count'] = None
                else:
                    details[row_list[0].lower().replace(' ', '_')] = '; '.join([x.strip() for x in row_list[1:] if x])
            if '>Genome</th>' in str(row):
                details['taxon_id'] = row.td.a['href'].split('=')[-1]     
        self.details = details
        #del self.content
        del self.rows  