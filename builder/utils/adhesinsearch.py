from data.models import Gene, Taxon, UsherHomologyGroup, NonRedundantUsher
from Tkinter import *
from django.conf import settings
from Bio import SearchIO
import tempfile
import json
import os


subunits = ['COG5430', 'pfam07434', 'COG3539', 'pfam00419', 'pfam04449', 'pfam02432', 'pfam06551']


def get_fasta(gene_id):
    g = Gene.objects.filter(gene_id=gene_id)[0]
    if g.img_orf_type == 'CDS':
        seq = g.get_fasta_sequence()
        if len(seq.split()) < 2:
            return None
        else:
            return seq
    else:
        return None


def find_pfam_domains(gene_id, evalue=0.001):
    seq = get_fasta(gene_id)
    if seq:
        pfam_dbs_dir = os.path.join(settings.PILIOMEDB_FILES, 'PfamDBs') 
        results = {}
        for db in ['Pfam-A.hmm', 'Pfam-B.hmm']:
            fd, seq_file_name = tempfile.mkstemp(suffix='.fasta')
            with os.fdopen(fd, 'w') as seq_file:
                seq_file.write(seq)
            fdout, output_file = tempfile.mkstemp(suffix='hmmscan_output.txt')
            os.close(fdout)
            cmd_args = (output_file, evalue, os.path.join(pfam_dbs_dir, db), seq_file_name)
            cmd = '/usr/local/bin/hmmscan -o %s --acc --incE %s %s %s' % cmd_args
            os.system(cmd)
            search = SearchIO.parse(output_file, 'hmmer3-text')
            try:
                hits = search.next()
                results[db.split('.')[0]] = list(hits.iterhit_keys())
            except StopIteration:
                results = {'Pfam-A': [], 'Pfam-B': []}
            os.remove(seq_file_name)
            os.remove(output_file)
        return results
    else:
        return None
    

def find_cog_domains(gene_id, evalue=0.001):
    seq = get_fasta(gene_id)
    if seq:
        cog_db = os.path.join(settings.PILIOMEDB_FILES, 'BLAST_DBs', 'COG', 'COGdb')
        fd, seq_file_name = tempfile.mkstemp(suffix='.fasta')
        with os.fdopen(fd, 'w') as seq_file:
            seq_file.write(seq)
        fdout, output_file = tempfile.mkstemp(suffix='rpsblast_output.txt')
        os.close(fdout)
        cmd_args = (seq_file_name, cog_db, evalue, output_file)
        cmd = '/usr/local/bin/rpsblast -query %s -db %s -evalue %s -out %s -outfmt 5' % cmd_args
        os.system(cmd)
        try:
            search = SearchIO.parse(output_file, 'blast-xml')
            hits = search.next()
            results = [x.description.split(',')[0] for x in hits]
        except StopIteration:
            results = []
        os.remove(seq_file_name)
        os.remove(output_file)
        return results
    else:
        return None
    

def get_cluster_details(usher_gene_id):
    g = Gene.objects.filter(gene_id=usher_gene_id, piliome_annotation='usher', associated_usher__isnull=True)[0]
    chaperones = [x.gene_id for x in g.get_chaperones()]
    strain = Taxon.objects.filter(img_taxon_id=g.taxon_id)[0]
    print 'Strain: %s' % strain.name
    for m in g.get_cluster():
        if 'complement' in m.dna_coordinates:
            strand = '>>'
        else:
            strand = '<<'
        if m.pfam: 
            pfam = m.pfam.split()[0]
        else:
            pfam = ''
        if m.cog:
            cog = m.cog.split()[0]
        else:
            cog = ''
        if m.amino_acid_sequence_length:
            aa_length = m.amino_acid_sequence_length
        else:
            aa_length = ''
        if m.gene_id == usher_gene_id:
            annot = 'USHER'
        elif m.gene_id in chaperones:
            annot = 'chaperone'
        elif pfam in subunits or cog in subunits:
            annot = '==> subunit'
        else:
            annot = m.piliome_annotation
        output = [strand, m.gene_id, pfam, cog, aa_length, annot]
        print ' / '.join([str(x) for x in output])


def add_extra_annotations():
    uhgs = UsherHomologyGroup.objects.all()
    ucount = 0
    for uhg in uhgs:
        ucount += 1
        print '\n%s/%s - dealing with UHG %s\n' % (ucount, uhgs.count(), uhg.id)
        members = eval(uhg.members) + eval(uhg.additional_redundant_members)
        members = Gene.objects.filter(gene_id__in=members, piliome_annotation='usher', associated_usher__isnull=True)
        mcount = 0
        for m in members:
            mcount += 1
            print '  %s/%s - UHG %s: %s' % (mcount, members.count(), uhg.id, m.gene_id)
            cluster = m.get_cluster()
            for g in cluster:
                if not g.extra_pfam_a or g.extra_pfam_b:
                    pfam = find_pfam_domains(g.gene_id)
                    if pfam:
                        g.extra_pfam_a = str(pfam['Pfam-A'])
                        g.extra_pfam_b = str(pfam['Pfam-B'])
                if not g.extra_cog:
                    cog = find_cog_domains(g.gene_id)
                    if cog:
                        g.extra_cog = cog
                g.save()
                
    print '**** Complete! ****'
        

class AdhesinSearch(object):
    
    """
    Object for the semi-automated search and annotation of potential adhesins
    """
    
    
    def __init__(self):
        pass
        
        
    def deal_with(self, uhg):
        self.uhg = UsherHomologyGroup.objects.filter(id=uhg)[0]
        
        
    def load_additional_redundant_members(self):
        if self.uhg.additional_redundant_members:
            print 'Additional redundant members already exists.'
        else:
            for m in eval(self.uhg.members):
                # Get the gene object for this usher
                g = Gene.objects.filter(gene_id=m, piliome_annotation='usher', associated_usher__isnull=True)[0]
                # Get the other (redundant) members
                other_members = NonRedundantUsher.objects.get(representative__gene_id=m).members.get_query_set()
                if self.uhg.additional_redundant_members:
                    existing_addtl_members = eval(self.uhg.additional_redundant_members)
                else:
                    existing_addtl_members = []
                self.uhg.additional_redundant_members = str(existing_addtl_members + [str(x.gene_id) for x in other_members])
                self.uhg.save()
            print 'Done!'
    
    
    def get_all_members(self):
        all_members = eval(self.uhg.members) + eval(self.uhg.additional_redundant_members)
        return Gene.objects.filter(gene_id__in=all_members, piliome_annotation='usher', associated_usher__isnull=True)
        
    
    def assign_adhesins(self, gcve_export_file):
        exp = json.load(open(gcve_export_file))
        adhesins = exp['savedSelections']['adhesins']
        acount = 0
        for adh in adhesins:
            acount += 1
            print '%s/%s - dealing with %s' % (acount, len(adhesins), adh)
            gid = adh.keys()[0]
            adh_genes = Gene.objects.filter(gene_id=gid)
            for g in adh_genes:
                g.piliome_annotation = 'adhesin'
                g.save()
        print '****** Done! ******'