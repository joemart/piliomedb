from django.conf import settings
from ete2 import Tree
from Bio import AlignIO
from StringIO import StringIO
from django.db.models import Q
from ete2 import PhyloTree
from data.models import Gene, NonRedundantUsher, SciPhyCluster, UsherHomologyGroup
import tempfile
import json
import os


ushers_analysis_dir = os.path.join(settings.PILIOMEDB_FILES, 'ushers_analyses')


def load_alignment_clusters():
    """
    Identifies and loads the representative sequence used in the 
    alignment and tree after gappyout trimming using TrimAl
    """
    print 'Loading alignment cluster representatives...'
    clusters_fname = 'nrushers_reps_clustalo_alignment_trimmed_nr100.fasta.clstr'
    clusters_file = os.path.join(ushers_analysis_dir, 'gappyout_trimming', clusters_fname)
    cf = open(clusters_file, 'r')
    cf = cf.read()
    clusters_count = cf.count('>Cluster')
    cf = cf.split('>Cluster')
    ccount = 0
    for cluster in cf:
        if cluster:
            ccount += 1
            print '  %s/%s - Dealing with cluster %s' % (ccount, clusters_count, ccount)
            rep = cluster.split('...')[0].split('>')[-1]
            all_members = cluster.split('...')
            for member in all_members:
                if '>' in member:
                    gene_id = member.split('>')[-1]
                    nrusher = NonRedundantUsher.objects.filter(representative__gene_id=gene_id)[0]
                    nrusher.alignment_representative = NonRedundantUsher.objects.get(representative__gene_id=rep)
                    nrusher.save()
    print 'Done!'
    
    
def load_sciphy_clusters():
    """
    Loads the clusters created by SciPhy into the database
    """
    print 'Loading SciPhy clusters to the database...'
    # Locate the clusters file
    clusters_file = os.path.join(ushers_analysis_dir, 'gappyout_trimming', 'SciPhy_Run1', 'clusters.json')
    clusters = json.load(open(clusters_file))
    # Locate the SciPhy output file
    sciphy_output_file = os.path.join(ushers_analysis_dir, 'gappyout_trimming', 'SciPhy_Run1', 'sciphy_output_mino10.txt')
    sciphy_output = open(sciphy_output_file, 'r')
    # Read sciphy_output
    sciphy_output = sciphy_output.read()
    alignments = {}
    for subfamily in sciphy_output.split('%'):
        if subfamily:
            lines = subfamily.splitlines()
            cluster_number = int(lines[0].split()[-1])
            aln = '\n'.join(lines[1:])
            alignments[cluster_number] = aln
            
    # Locate the tree file
    tree_fname = 'nrushers_reps_clustalo_alignment_trimmed_nr100_WAGgamma.tree'
    tree_file = os.path.join(ushers_analysis_dir, 'gappyout_trimming', 'SciPhy_Run1', tree_fname)
    tree = Tree(tree_file)
    ccount = 0
    for k,v in clusters.items():
        ccount += 1
        print '\n\n  %s/%s - Dealing with cluster %s\n\n' % (ccount, len(clusters), ccount)
        # Create temporary files for the hmm and alignment files
        hmm_outf = tempfile.NamedTemporaryFile(delete=False)
        hmm_outf_name = hmm_outf.name
        hmm_outf.close()
        aln_file = tempfile.NamedTemporaryFile(delete=False)
        aln_file.write(alignments[int(k)])
        aln_file_name = aln_file.name
        aln_file.close()
        sto_file = tempfile.NamedTemporaryFile(delete=False)
        sto_file_name = sto_file.name
        aln_file = open(aln_file_name, 'rU')
        aln = AlignIO.parse(aln_file, 'fasta')
        AlignIO.write(aln, sto_file_name, 'stockholm')
        sto_file.close()
        os.system('hmmbuild --amino -n usher_subfamily%s %s %s' % (k, hmm_outf_name, sto_file_name))
        sc = SciPhyCluster(
                cluster_number=k, 
                minimum_overlap=10, 
                members = str(clusters[k]),
                members_count = len(clusters[k]),
                alignment = alignments[int(k)],
                hmm_profile = open(hmm_outf_name, 'r').read(),
                sciphy_run_dir=os.path.split(clusters_file)[0]
            )
        monophyly_check = tree.check_monophyly(values=[str(x) for x in v], target_attr="name")
        if monophyly_check[0] is True:
            sc.is_monophyletic = True
        else:
            sc.is_monophyletic = False
        sc.save()
    print 'Done!'
    
    
def load_usher_types():
    print 'Loading SciPhy clusters to the database...'
    # Locate the clusters file
    clusters_file = os.path.join(ushers_analysis_dir, 'gappyout_trimming', 'SciPhy_Run1', 'clusters.json')
    clusters = json.load(open(clusters_file))
    
    
def classify_nonmonophyletic_clusters():
    """
    Classify the non-monophyletic clusters into any of the following categories: 
    Loss of function, divergence of subclade(s), or convergence
    
    Take into account the support values in the classification
    """
    # Locate the clusters file
    print 'Locating the cluster and tree files...'
    clusters_file = os.path.join(ushers_analysis_dir, 'gappyout_trimming', 'SciPhy_Run1', 'clusters.json')
    clusters = json.load(open(clusters_file))
    clusters = {int(k): [str(x) for x in v] for k,v in clusters.items()}
    # Locate the tree file
    tree_fname = 'nrushers_reps_clustalo_alignment_trimmed_nr100.tree'
    tree_file = os.path.join(ushers_analysis_dir, 'gappyout_trimming', 'SciPhy_Run1', tree_fname)
    # Load the tree
    print 'Loading the tree and adding cluster annotations to each leaf...'
    tree = Tree(tree_file)
    # Reverse mapping
    clusters_rev = {}
    for k,v in clusters.items():
        for x in v:
            clusters_rev[x] = k
    # Add cluster information to all leaves
    for node in tree:
        if node.is_leaf():        
            node.add_feature('cluster', clusters_rev[node.name])
            
    # Get the non-monophyletic clusters
    print 'Detecting divergence of subclades...'
    polys = SciPhyCluster.objects.filter(is_monophyletic=False)
    pcount = 0
    for poly in polys:
        pcount += 1
        cluster_number = int(poly.cluster_number)
        # Check for divergence of subclade(s)
        poly_anc = tree.get_common_ancestor(*clusters[cluster_number])
        subclades_found = []
        for node in poly_anc:
            if node.is_leaf():
                if node.cluster != cluster_number and node.cluster not in subclades_found:
                    subclades_found.append(node.cluster)
        print '%s/%s' % (pcount, polys.count())
        print '\t', cluster_number, '==>', subclades_found
    
    return clusters
    
    
class SciPhyClusterUpdater(object):
    
    def __init__(self, cluster_number=None, tree_file=None):
        tree_fname = 'nrushers_reps_clustalo_alignment_trimmed_nr100_WAGgamma.tree'
        tree_file = os.path.join(ushers_analysis_dir, 'gappyout_trimming', 'SciPhy_Run1', tree_fname)
        clusters_file = os.path.join(ushers_analysis_dir, 'gappyout_trimming', 'SciPhy_Run1', 'clusters.json')
        tree = PhyloTree(tree_file, sp_naming_function=None)
        tree.set_outgroup(tree.get_midpoint_outgroup())
        events = tree.get_descendant_evol_events()
        self.events = events
        self.tree = tree
        clusters = json.load(open(clusters_file))
        self.clusters = {int(k): [str(x) for x in v] for k,v in clusters.items()}
        if cluster_number:
            self.sciphy_cluster = SciPhyCluster.objects.filter(cluster_number=cluster_number)[0]
            self.members = eval(self.sciphy_cluster.members)
    
    def deal_with(self, cluster_number):
        self.sciphy_cluster = SciPhyCluster.objects.filter(cluster_number=cluster_number)[0]
        self.members = eval(self.sciphy_cluster.members)
        print 'Now dealing with cluster %s' % cluster_number
        
    def remove_member(self, name):
        if name in self.members:
            members_removed = self.sciphy_cluster.members_removed
            if members_removed: 
                members_removed = eval(members_removed)
                if name not in members_removed:
                    members_removed.append(name)
            else:
                members_removed = [name]
            self.sciphy_cluster.members_removed = str(members_removed)
            self.sciphy_cluster.save()
        else:
            print 'Error: The protein is not a member of the cluster.'
        
    def add_member(self, name, add_to=None):
        if add_to:
            target = SciPhyCluster.objects.filter(cluster_number=add_to)[0]
        else: 
            target = self.sciphy_cluster
        members_added = target.members_added
        if members_added: 
            members_added = eval(members_added)
            if name not in members_added:
                members_added.append(name)
        else:
            members_added = [name]
        target.members_added = str(members_added)
        target.save()
        
    def transfer_member(self, name, transfer_to=None):
        self.remove_member(name)
        self.add_member(name, add_to=transfer_to)
        
    def transfer_all_members(self, target):
        confirm = raw_input('Are you sure you want to transfer these members? [y]/n?') or 'y'
        if confirm == 'y':
            for m in eval(self.sciphy_cluster.members):
                self.remove_member(str(m))
                self.add_member(str(m), add_to=target)
        elif confirm == 'n':
            print 'Ok. Nothing was transferred.'
            
    def remove_clade(self, rep_name1, rep_name2):
        common_anc = self.tree.get_common_ancestor(rep_name1, rep_name2)
        leaves = [x.name for x in common_anc.get_leaves()]
        print 'This clade is composed of %s leaves:' % len(leaves)
        print ', '.join(leaves)
        confirm = raw_input('Are you sure you want to remove this clade? [y]/n?') or 'y'
        if confirm == 'y':
            for leaf in leaves:
                self.remove_member(leaf)
        elif confirm == 'n':
            print 'Ok. Nothing was removed.'
            
    def add_clade(self, rep_name1, rep_name2, add_to=None):
        common_anc = self.tree.get_common_ancestor(rep_name1, rep_name2)
        leaves = [x.name for x in common_anc.get_leaves()]
        print 'This clade is composed of %s leaves:' % len(leaves)
        print ', '.join(leaves)
        confirm = raw_input('Are you sure you want to add this clade? [y]/n?') or 'y'
        if confirm == 'y':
            for leaf in leaves:
                self.add_member(leaf, add_to=add_to)
        elif confirm == 'n':
            print 'Ok. Nothing was added.'   
            
    def transfer_clade(self, rep_name1, rep_name2, transfer_to=None):
        common_anc = self.tree.get_common_ancestor(rep_name1, rep_name2)
        leaves = [x.name for x in common_anc.get_leaves()]
        print 'This clade is composed of %s leaves:' % len(leaves)
        print ', '.join(leaves)
        confirm = raw_input('Are you sure you want to transfer this clade to %s? [y]/n?' % transfer_to) or 'y'
        if confirm == 'y':
            for leaf in leaves:
                self.remove_member(leaf)
                self.add_member(leaf, add_to=transfer_to)
        elif confirm == 'n':
            print 'Ok. Nothing was transferred.'
            
    def reassign_member(self, name, source=None, dest=None):
        c1 = SciPhyCluster.objects.filter(cluster_number=source)[0]
        c2 = SciPhyCluster.objects.filter(cluster_number=dest)[0]
        c1_members_added = eval(c1.members_added)
        c2_members_added = eval(c2.members_added)
        if name in c1_members_added:
            c1_members_added.remove(name)
            if name not in c2_members_added:
                c2_members_added.append(name)
        c1.members_added = str(c1_members_added)
        c1.save()
        c2.members_added = str(c2_members_added)
        c2.save()
        
    def reassign_clade(self, rep_name1, rep_name2, source=None, dest=None):
        common_anc = self.tree.get_common_ancestor(rep_name1, rep_name2)
        leaves = [x.name for x in common_anc.get_leaves()]
        print 'This clade is composed of %s leaves:' % len(leaves)
        print ', '.join(leaves)
        confirm = raw_input('Are you sure you want to reassign this clade to %s? [y]/n?' % dest) or 'y'
        if confirm == 'y':
            c1 = SciPhyCluster.objects.filter(cluster_number=source)[0]
            c2 = SciPhyCluster.objects.filter(cluster_number=dest)[0]
            c1_members_added = eval(c1.members_added)
            c2_members_added = eval(c2.members_added)
            for leaf in leaves:
                if leaf in c1_members_added:
                    c1_members_added.remove(leaf)
                    if leaf not in c2_members_added:
                        c2_members_added.append(leaf)
            c1.members_added = str(c1_members_added)
            c1.save()
            c2.members_added = str(c2_members_added)
            c2.save()
        elif confirm == 'n':
            print 'Ok. Nothing was reassigned.'
        
    def mark_as_checked(self):
        self.sciphy_cluster.manually_checked = True
        self.sciphy_cluster.save()


def load_additional_members_to_uhgs():
    uhgs = UsherHomologyGroup.objects.all()
    for i, uhg in enumerate(uhgs):
        print i+1, uhgs.count()
        addtl_members = []
        members = eval(uhg.members)
        for member in members:
            nr = NonRedundantUsher.objects.filter(representative__gene_id=member)[0]
            addtl_members += [x.gene_id for x in nr.members.get_query_set()]
            try:
                nr_aln = NonRedundantUsher.objects.filter(alignment_representative=nr)[0]
                addtl_members += [x.gene_id for x in nr_aln.members.get_query_set()]
            except IndexError:
                print 'Error:', uhg.id, member
        print 'members:', len(list(set(addtl_members)))
        uhg.additional_redundant_members = addtl_members
        uhg.save()


def identify_sciphy_scluster(gene_id):
    gene = Gene.objects.filter(gene_id=gene_id)[0]
    nrusher = gene.nrusher_cluster.get_query_set()[0]
    aln_rep = nrusher.alignment_representative.representative
    sc = SciPhyCluster.objects.filter(Q(members__contains=aln_rep.gene_id) | Q(members_added__contains=aln_rep.gene_id))[0]
    return int(sc.cluster_number)
    
    
def identify_usher_type(gene_id):
    gene = Gene.objects.filter(gene_id=gene_id)[0]
    nrusher = gene.nrusher_cluster.get_query_set()[0]
    aln_rep = nrusher.alignment_representative.representative
    sc = UsherHomologyGroup.objects.filter(members__contains=aln_rep.gene_id)[0]
    return int(sc.modified_cluster_number)