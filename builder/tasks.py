from data.models import Gene, Scaffold, PiliomeDBVersion, Taxon, NonRedundantUsher
from django.core.management import call_command
from builder.utils.blast import BlastP
from subprocess import Popen, PIPE
from builder.utils.imginter import ScrapeIMG
from taskqueue.models import BackgroundTask
from celery import current_app as celery
from celery.result import AsyncResult
from django.db.models import F
from datetime import datetime
from django.conf import settings
from celery import task
from django.db.models import Q
from subprocess import Popen, PIPE
import cPickle as pickle
from Bio import SeqIO
import hashlib
import uuid
import json
import time
import glob
import re
import os

 
        
def _restart_tor():
    c = Popen('python manage.py supervisor status tor', shell=True, stdout=PIPE)
    stdout, stderr = c.communicate()
    state = stdout.split()[1]
    if state == 'RUNNING':
        runtime = stdout.split()[-1]
        hours, minutes, seconds = [int(x) for x in runtime.split(':')]
        total_seconds = seconds + (60*minutes) + (3600*hours)
        if total_seconds > 30:
            call_command('supervisor', 'restart', 'tor')
        
    #return {'state': state, 'runtime': runtime}


@task
def _fetch_gene_details(query_id, master_task_pk, fasta_desc, aa_seq, dbversion, usher=None, cu_cluster_position=0, fetch_from='img', http_error_as_success=True, browser=None):
    # Scrape then save to database
    subtask_id = _fetch_gene_details.request.id
    _fetch_gene_details.update_state(subtask_id, state='PROCESSING')
    #try:
    #    try:
    task = BackgroundTask.objects.get(pk=master_task_pk)
    if usher:
        usher_gene = Gene.objects.filter(gene_id=usher, associated_usher__isnull=True)[0]
    # Check if gene exists
    if usher:
        gene = Gene.objects.filter(gene_id=query_id, associated_usher=usher_gene, cu_cluster_position=cu_cluster_position)
    else:
        gene = Gene.objects.filter(gene_id=query_id, cu_cluster_position=cu_cluster_position)
    # If gene exists
    if gene:
        print 'This gene exists in the database: %s' % query_id
        if usher:
            print 'Incrementing fetch count'
            usher_gene.cu_cluster_fetch_count = F('cu_cluster_fetch_count') + 1
            usher_gene.save()
            print 'Incremented the CU cluster fetch count for the associated usher.'
        task.increment_success_count()
        #task.subtasks_success_count = F('subtasks_success_count') + 1
        #task.save()
    else:
        # Send request if gene is not in the database
        print 'Fetching', query_id
        if aa_seq:
            fetch_aa_seq = False
        else:
            fetch_aa_seq = True
        try:
            if usher:
                taxon_id = usher_gene.taxon_id
            else:
                taxon_id = None
            # Perform the scrape
            #print usher_gene.taxon_id
            print 'Taxon ID: %s' % taxon_id
            scrape = ScrapeIMG('gene', query_id, taxon_id=taxon_id, fetch_aa_seq=fetch_aa_seq, use_tor=False, fetch_from=fetch_from, browser=browser)
            
            if scrape.status_code == 200:
                if scrape.details:
                    details = scrape.details
                    if fetch_aa_seq:
                        try:
                            aa_seq = details['amino_acid_sequence']
                        except KeyError:
                            aa_seq = ''
                    details['task'] = task
                    details['cu_cluster_position'] = cu_cluster_position
                    if not fetch_aa_seq:
                        details['fasta_description'] = fasta_desc
                    if usher:
                        details['associated_usher'] = usher_gene
                        details['piliome_annotation'] = ''
                    else:
                        details['piliome_annotation'] = 'usher'
                    details['amino_acid_sequence'] = aa_seq
                    g = Gene.objects.create(**details)
                    g.db_versions.add(dbversion)
                    g.save()
                    task.increment_success_count()
                    #task.subtasks_success_count = F('subtasks_success_count') + 1
                    #task.save()
                    if usher:
                        print 'Incrementing fetch count'
                        usher_gene.cu_cluster_fetch_count = F('cu_cluster_fetch_count') + 1
                        usher_gene.save()
                else:
                    task.increment_success_count()
                    #task.subtasks_success_count = F('subtasks_success_count') + 1
                    #task.save()
                    if usher:
                        print 'Gene not found!', 'Incrementing fetch count'
                        usher_gene.cu_cluster_fetch_count = F('cu_cluster_fetch_count') + 1
                        usher_gene.save() 
                print '*** Saved to database! ***'
            else:
                _fetch_gene_details.update_state(subtask_id, state='FAILURE')
                if http_error_as_success:
                    task.increment_success_count()
                    if usher:
                        print 'Gene not found!', 'Incrementing fetch count'
                        usher_gene.cu_cluster_fetch_count = F('cu_cluster_fetch_count') + 1
                        usher_gene.save()
                else:
                    task.increment_failure_count()
                #task.subtasks_failure_count = F("subtasks_failure_count") + 1
                #task.save()
                # Restart Tor
                ##_restart_tor()
                #call_command('supervisor', 'restart', 'tor')
                raise Exception('Scraping failed: HTTP Status Code %s' % scrape.status_code)
                
        except IndexError:
            print 'ERROR: %s' % query_id
            _fetch_gene_details.update_state(subtask_id, state='FAILURE')
            task.increment_failure_count()
            #task.subtasks_failure_count = F("subtasks_failure_count") + 1
            #task.save()
            # Restart Tor
            ##_restart_tor()
            #call_command('supervisor', 'restart', 'tor')


@task
def load_ushers(ushers_aa, user, version, browser=None):
    # Record this task
    task_id = load_ushers.request.id
    # Read the sequence file
    s = SeqIO.index(ushers_aa, 'fasta')
    file_hash = hashlib.md5(open(ushers_aa).read()).hexdigest()
    # Get the DB version
    dbversion = PiliomeDBVersion.objects.filter(db_version=version)[0]
    # Save this background task
    task = BackgroundTask(
        user=user, 
        task_id=task_id,
        tool_name='ushers_loader',
        pipeline_name='img_data_loading_pipeline',
        db_version=version,
        input_type='file',
        input_file=ushers_aa,
        input_file_hash=file_hash,
        total_subtasks = len(s)
    )
    task.save()
    # Iterate over the sequences
    tasks = []
    for k in s:
        # Scrape the gene details page to retrieve 
        # important pieces of information
        fasta_desc = s[k].description
        aa_seq = s[k].seq.tostring()
        kwargs = {'cu_cluster_position': 0, 'fetch_from': 'imgrepl', 'browser': browser}
        subtask = _fetch_gene_details.apply_async(args=(k, task.id, fasta_desc, aa_seq, dbversion), kwargs=kwargs)
        tasks.append(subtask.task_id)
        
    task.subtasks = json.dumps(tasks)
    task.save()
    
    
@task
def _fetch_scaffold_details(scaffold_id, master_task_pk, dbversion):
    # TODO -- The error catching mechanisms for this function has been disabled.
    # This needs to be restored and improved if this pipeline is intended to 
    # be run purely from the web interface
    
    subtask_id = _fetch_scaffold_details.request.id
    _fetch_scaffold_details.update_state(subtask_id, state='PROCESSING')
    #try:
        #try:
    # TODO -- Some ushers have scaffold IDs that are not found in 
    # IMG. In this case, it's best to just remove those ushers from the
    # database. This function should be changed to catch the "Scaffold not found"
    # error and should just delete the ushers with those scaffolds.
    task = BackgroundTask.objects.get(pk=master_task_pk)
    scaffold = Scaffold.objects.filter(scaffold_id=scaffold_id)
    if not scaffold:
        print 'Sending request'
        scrape = ScrapeIMG('scaffold', scaffold_id, use_tor=True)    
        if scrape.status_code == 200: 
            if scrape.details:
                details = scrape.details
                details['task'] = task
                s = Scaffold.objects.create(**details)
                s.save()
                s.db_versions.add(dbversion)
                s.save()
                task.subtasks_success_count = F("subtasks_success_count") + 1
                task.save()
                print 'Saved to database!'
            else:
                _fetch_scaffold_details.update_state(subtask_id, state='FAILURE')
                raise Exception('Scraping failed: Relevant rows not found')
        else:
            _fetch_scaffold_details.update_state(subtask_id, state='FAILURE')
            raise Exception('Scraping failed: HTTP Status Code %s' % scrape.status_code)
    else:
        task.subtasks_success_count = F("subtasks_success_count") + 1
        task.save()
        print 'This scaffold is already in the database'
                
        #except:
            #raise Exception('Connection Error: Could not connet')
    #except Exception as exc:
        #raise _fetch_scaffold_details.retry(exc=exc, countdown=10, max_retries=3)
     
    
@task
def fetch_scaffolds(user, version):
    # Record this task
    task_id = fetch_scaffolds.request.id
    # Get the DB version
    dbversion = PiliomeDBVersion.objects.filter(db_version=version)[0]
    # Get the ushers from this version and take all their scaffold IDs
    scaffolds = dbversion.genes.filter(piliome_annotation='usher').values('scaffold_id').distinct()
    # Get only the scaffolds from this version
    existing_scaffolds = dbversion.scaffolds.get_queryset().values('scaffold_id').distinct()
    missing_scaffolds = [x for x in scaffolds if x not in existing_scaffolds]
    # Save this background task
    task = BackgroundTask(
        user=user, 
        task_id=task_id,
        tool_name='scaffolds_details_fetcher',
        pipeline_name='img_data_loading_pipeline',
        input_type='database entries',
        db_version=version,
        total_subtasks=len(missing_scaffolds)
    )
    task.save()
    # Iterate over the scaffold IDs
    tasks = []
    for k in missing_scaffolds:
        subtask = _fetch_scaffold_details.apply_async(args=(k['scaffold_id'], task.id, dbversion))
        tasks.append(subtask.task_id)
        
    task.subtasks = json.dumps(tasks)
    task.save()


@task
def _fetch_taxon_details(taxon_id, master_task_pk, dbversion):
    subtask_id = _fetch_taxon_details.request.id
    _fetch_taxon_details.update_state(subtask_id, state='PROCESSING')
    
    task = BackgroundTask.objects.get(pk=master_task_pk)
    taxon = Taxon.objects.filter(img_taxon_id=taxon_id)
    if not taxon:
        print 'Sending request'
        scrape = ScrapeIMG('taxon', taxon_id, use_tor=True)    
        if scrape.status_code == 200: 
            if scrape.details:
                # Save the parent taxon levels:
                lineage = scrape.details['lineage']
                
                def _lineage(tx):
                    try:
                        return lineage[tx]
                    except KeyError:
                        return 'unclassified'
                        
                ## Domain
                domain = dbversion.taxa.filter(level='domain', name=_lineage('domain'))
                if domain:
                    domain = domain[0]
                else:
                    domain = Taxon.objects.create(level='domain', name=_lineage('domain'), task=task)
                    domain.db_versions.add(dbversion)
                    domain.save()
                ## Phylum
                phylum = None
                if _lineage('phylum') != 'unclassified':
                    for child in domain.child_levels.all():
                        if child.name == _lineage('phylum'):
                            phylum = child
                if not phylum:
                    phylum = Taxon.objects.create(level='phylum', name=_lineage('phylum'), task=task)
                    phylum.db_versions.add(dbversion)
                    phylum.save()
                    domain.child_levels.add(phylum)
                    domain.save()
                ## Class
                txclass = None
                if _lineage('class') != 'unclassified':
                    for child in phylum.child_levels.all():
                        if child.name == _lineage('class'):
                            txclass = child
                if not txclass:
                    txclass = Taxon.objects.create(level='class', name=_lineage('class'), task=task)
                    txclass.db_versions.add(dbversion)
                    txclass.save()
                    phylum.child_levels.add(txclass)
                    phylum.save()
                ## Order
                order = None
                if _lineage('order') != 'unclassified':
                    for child in txclass.child_levels.all():
                        if child.name == _lineage('order'):
                            order = child
                if not order:
                    order = Taxon.objects.create(level='order', name=_lineage('order'), task=task)
                    order.db_versions.add(dbversion)
                    order.save()
                    txclass.child_levels.add(order)
                    txclass.save()
                ## Family
                family = None
                if _lineage('family') != 'unclassified':
                    for child in order.child_levels.all():
                        if child.name == _lineage('family'):
                            family = child
                if not family:
                    family = Taxon.objects.create(level='family', name=_lineage('family'), task=task)
                    family.db_versions.add(dbversion)
                    family.save()
                    order.child_levels.add(family)
                    order.save()
                ## Genus
                genus = None
                if _lineage('genus') != 'unclassified':
                    for child in family.child_levels.all():
                        if child.name == _lineage('genus'):
                            genus = child
                if not genus:
                    genus = Taxon.objects.create(level='genus', name=_lineage('genus'), task=task)
                    genus.db_versions.add(dbversion)
                    genus.save()
                    family.child_levels.add(genus)
                    family.save()
                ## Species
                species = None
                if _lineage('species') != 'unclassified':
                    for child in genus.child_levels.all():
                        if child.name == _lineage('species'):
                            species = child
                if not species:
                    species = Taxon.objects.create(level='species', name=_lineage('species'), task=task)
                    species.db_versions.add(dbversion)
                    species.save()
                    genus.child_levels.add(species)
                    genus.save()
                        
                # Save the strain details
                strain_details = scrape.details['strain']
                strain_details['level'] = 'strain'
                strain_details['task'] = task
                strain_details['with_genome'] = True
                strain = Taxon.objects.create(**strain_details)
                strain.save()
                species.child_levels.add(strain)
                species.save()
                strain.db_versions.add(dbversion)
                strain.save()
                
                # Update the subtasks success count
                task.subtasks_success_count = F("subtasks_success_count") + 1
                task.save()
                print 'Saved to database!'
            else:
                _fetch_taxon_details.update_state(subtask_id, state='FAILURE')
                raise Exception('Scraping failed: Relevant rows not found')
        else:
            _fetch_taxon_details.update_state(subtask_id, state='FAILURE')
            raise Exception('Scraping failed: HTTP Status Code %s' % scrape.status_code)
    else:
        task.subtasks_success_count = F("subtasks_success_count") + 1
        task.save()
        print 'This taxon is already in the database'
    
    
@task
def fetch_taxa(user, version):
    # Record this task
    task_id = fetch_taxa.request.id
    # Get the DB version
    dbversion = PiliomeDBVersion.objects.filter(db_version=version)[0]
    # Get the ushers from this version and take all their taxon IDs
    taxa = dbversion.genes.filter(piliome_annotation='usher').values('taxon_id').distinct()
    # Get only the scaffolds from this version
    existing_taxa = dbversion.taxa.get_queryset().filter(with_genome=True).values('img_taxon_id').distinct()
    missing_taxa = [x for x in taxa if x not in existing_taxa]
    # Save this background task
    task = BackgroundTask(
        user=user, 
        task_id=task_id,
        tool_name='taxa_details_fetcher',
        pipeline_name='img_data_loading_pipeline',
        input_type='database entries',
        db_version=version,
        total_subtasks=len(missing_taxa)
    )
    task.save()
    # Iterate over the taxon IDs
    tasks = []
    for k in missing_taxa:
        subtask = _fetch_taxon_details.apply_async(args=(k['taxon_id'], task.id, dbversion))
        tasks.append(subtask.task_id)
        
    task.subtasks = json.dumps(tasks)
    task.save()


@task
def _fetch_cucluster_genes(usher_id, master_task_pk, dbversion):
    """
    Fetches the genes -10 to +10 of the usher
    """
    task = BackgroundTask.objects.get(pk=master_task_pk)
    subtasks = []
    for x in range(-10, 11):
        if x is not 0:
            gene_id = int(usher_id) + x
            kwargs = {'usher': usher_id, 'cu_cluster_position': x, 'fetch_from': 'imgrepl'}
            args = (gene_id, master_task_pk, None, None, dbversion)
            subtask_id = str(uuid.uuid4())
            subtask = _fetch_gene_details.subtask(task_id=subtask_id, args=args, kwargs=kwargs)
            subtask.delay()
            subtasks.append(subtask_id)
    # Update the subtasks success count
    task.subtasks = json.dumps(subtasks)
    task.save()
            

@task
def fetch_cuclusters(user, version, fetch_from='imgrepl'):
    # TODO -- Update which ushers had already their CU gene clusters fetched
    # Record this task
    task_id = fetch_cuclusters.request.id
    # Get the DB version
    dbversion = PiliomeDBVersion.objects.filter(db_version=version)[0]
    # Get the ushers with CU gene clusters unfetched
    ushers = dbversion.genes.filter(piliome_annotation='usher', associated_usher__isnull=True, cu_cluster_fetch_count__lt=20)
    if fetch_from == 'imgrepl':
        genomes_dir = os.path.join(settings.PILIOMEDB_FILES, 'genomes')
        genbanks_dir = os.path.join(settings.PILIOMEDB_FILES, 'genbank_files')
        genbank_files = [os.path.split(x)[-1].split('.')[0] for x in glob.glob(os.path.join(genbanks_dir, '*.gbk'))]
        genomes = [os.path.split(x)[-1] for x in glob.glob(os.path.join(genomes_dir, '*'))]
        all_genomes = genbank_files + genomes
        ushers = ushers.filter(cu_cluster_fetch_count=0, taxon_id__in=all_genomes)
    ushers = ushers
    json.dump([x.gene_id for x in ushers], open('/Users/joemartaganna/Desktop/fetch_cu_clusters_ushers.json','w'))
    # Save this background task
    task = BackgroundTask(
        user=user,
        task_id=task_id,
        tool_name='cu_clusters_fetcher',
        pipeline_name='img_data_loading_pipeline',
        db_version=version,
        input_type='database objects',
        total_subtasks = len(ushers) * 20
    )
    task.save()
    # Iterate over the ushers
    tasks = []
    for usher in ushers:
        subtask = _fetch_cucluster_genes.subtask(args=(usher.gene_id, task.id, dbversion))
        subtask.delay()
        #tasks.append(subtask.task_id)
        
    #task.subtasks = json.dumps(tasks)
    #task.save()


@task
def _encode_cluster_annotations(usher, master_task_pk, user, version):
    
    # Get the task object
    task = BackgroundTask.objects.get(pk=master_task_pk)
    # List of Pfam and COG IDs for chaperone
    chaperones = ['pfam00345', 'pfam02753', 'COG3121']
    # Fetch usher gene object
    usher_gene = Gene.objects.filter(gene_id=usher)[0]
    # Get usher scaffold name
    usher_scaffold = usher_gene.scaffold_name
    # Fetch the genes in the cluster
    cluster = usher_gene.get_cluster()
    
    if True: #len(usher_gene.cluster_coded_annotations) != cluster.count():
        
        print 'Encoding annotation for %s' % usher
        coded_genes = ''
        if cluster:
            for gene in cluster:
                #gcount += 1
                gene_scaffold = gene.scaffold_name
                if usher_scaffold == gene_scaffold:
                    gene_code = 'X' # Default code
                else:
                    gene_code = 'x'
        
                #print '    %s - Checking if gene is an usher...' % gcount
                # Check if this gene is an usher
                gene_records = Gene.objects.filter(gene_id=gene.gene_id)
                gene_annotations = [x.piliome_annotation for x in gene_records]
                if 'usher' in gene_annotations:
                    if usher_scaffold == gene_scaffold:
                        if gene.gene_id == usher:
                            gene_code = 'M' # main usher
                        else:
                            gene_code = 'U' # any other usher from same scaffold
                    else:
                        gene_code = 'u' # any other usher from different scaffold
        
                #print '    %s - Checking if gene is a chaperone...' % gcount
                # Check if this gene is a chaperone
                annotations = []
                if gene.pfam:
                    for pf in gene.pfam.split(';'):
                        pf = pf.split('-')[0].strip()
                        #print pf
                        annotations.append(pf)
                if gene.cog:
                    for cg in gene.cog.split(';'):
                        cg = cg.split('-')[0].strip()
                        #print cg
                        annotations.append(cg)
                if set(annotations).intersection(set(chaperones)):
                    if usher_scaffold == gene_scaffold:
                        gene_code = 'C'
                    else:
                        gene_code = 'c'

                # Add the gene_code to the coded_genes
                coded_genes += gene_code
		
    	# Print the coded_genes
        print '%s coded annotations: %s' % (usher, coded_genes)

        # Save this coded_genes to database
        usher_gene.cluster_coded_annotations = coded_genes
        usher_gene.save()

    # Increment the success count
    task.increment_success_count()
    
    # Execute step 2 if step 1 is finished
    if task.get_success_count() == task.total_subtasks:
        step2 = run_cu_search.apply_async(args=(user, version, 2))


@task
def _find_chaperones_and_split_ushers(usher, master_task_pk):
    """
    Search for the chaperone in the cluster, which in most cases will be as 
    simple as finding C in the coded annotations but becomes more complicated
    if another usher is also found within the cluster
    """
    print 'Searching CU pairs for %s' % usher
    # Get the task object
    task = BackgroundTask.objects.get(pk=master_task_pk)
    # Fetch usher gene object
    usher = Gene.objects.filter(gene_id=usher)[0]
    # Get the cluster_coded_annotations for this usher
    cca = usher.cluster_coded_annotations
    cluster = usher.get_cluster()
    #print 'Finding CU pairs...'
    res = re.finditer("C*X{0,3}U*[UM]U*X{0,3}C*", cca)
    print res
    for x in res:
        pair = x.group(0)
        #print pair
        if 'M' in pair:
            mindex = pair.index('M') + x.start()
            ccount = 0
            for code in pair:
                if code == 'C':
                    #print 'Chaperone found'
                    cindex = ccount + x.start()
                    chap = cluster[cindex]
                    chap.piliome_annotation = 'chaperone'
                    chap.save()
                elif code == 'U':
                    #print 'Usher fragment found'
                    uindex = ccount + x.start()
                    ush = cluster[uindex]
                    ush.piliome_annotation = 'usher'
                    if ush.amino_acid_sequence_length <= 500:
                        ush.is_usher_fragment = True
                    ush.save()
                    if ush.is_usher_fragment and usher.amino_acid_sequence_length <= 500:
                        usher.is_usher_fragment = True
                        usher.save()
                ccount += 1
    # Increment the success count
    task.increment_success_count()


@task
def run_cu_search(user, version, step):
    task_id = run_cu_search.request.id
    ushers = Gene.objects.filter(piliome_annotation='usher', associated_usher__isnull=True).values_list('gene_id')
    # Get the DB version
    dbversion = PiliomeDBVersion.objects.filter(db_version=version)[0]
    
    # Save this background task
    task = BackgroundTask(
        user=user,
        task_id=task_id,
        tool_name='cu_search',
        step = step,
        pipeline_name='img_data_loading_pipeline',
        db_version=version,
        input_type='database objects',
        total_subtasks = len(ushers)
    )
    task.save()
       
    # Iterate over the ushers and call the subtasks
    for usher in ushers:
        usher = usher[0]
        if step == 1: 
            subtask = _encode_cluster_annotations.subtask(args=(usher, task.id, user, version))
            subtask.delay()
        elif step == 2:
            subtask = _find_chaperones_and_split_ushers.subtask(args=(usher, task.id))
            subtask.delay()


@task
def _save_nr_cluster(representative, members, master_task_pk, dbversion):
    master_task = BackgroundTask.objects.get(pk=master_task_pk)
    details = {
        'representative': Gene.objects.filter(gene_id=representative)[0],
        'task': master_task
    }
    nrrep = NonRedundantUsher.objects.create(**details)
    nrrep.db_versions.add(dbversion)
    nrrep.members.add(*[Gene.objects.filter(gene_id=x)[0] for x in members])
    nrrep.save()
    master_task.save()
    master_task.increment_success_count()
    

@task
def _append_sequence(outfile, seq, master_task_pk, user, version):
    seqout = open(outfile, 'a')
    seqout.write(seq)
    seqout.close()
    master_task = BackgroundTask.objects.get(pk=master_task_pk)
    current_success_count = master_task.get_success_count()
    if (current_success_count + 1) == master_task.total_subtasks:
        print 'Writing sequence file done'
        step2 = run_cdhit_clustering.apply_async(args=(user, version, 2))
    master_task.increment_success_count()
    
    
@task
def run_cdhit_clustering(user, version, step):
    # TODO -- For some reason the database count of successful tasks is not 
    # accurate in concurrent mode. This may have something to do with the fact
    # that execution is too fast. One solution is to put this particular task
    # in a queue with a single worker.
    
    # Build the base_path needed for defining output files later
    base_path = os.path.join(settings.BASE_DIR, 'piliomedb', 'media', 'results', 'sequences')
    # Build the paths for some output files
    seqout_path = os.path.join(base_path, 'all_ushers_v%s.fasta' % version)
    nrseq_path = os.path.join(base_path, 'all_ushers_v%s_nr1.fasta' % version)
    # Record this task
    task_id = run_cdhit_clustering.request.id
    # Get the DB version
    dbversion = PiliomeDBVersion.objects.filter(db_version=version)[0]
    
    if step == 1:
        print 'Writing usher sequences to file...'
        # Get all ushers
        ushers = Gene.objects.filter(piliome_annotation='usher', associated_usher__isnull=True).exclude(is_usher_fragment=True)
        ushers = ushers.exclude(amino_acid_sequence_length__lt=700)
        # Save this background task
        task = BackgroundTask(
            user=user, 
            task_id=task_id,
            tool_name='cdhit_clustering_tool',
            step=step,
            pipeline_name='cu_classification_pipeline',
            input_type='database entries',
            db_version=version,
            total_subtasks=ushers.count(),
            misc_output=seqout_path
        )
        task.save()
        # Remove the output files, if any
        output_files = glob.glob(os.path.join(base_path,'all_ushers_v%s*' % version))
        for f in output_files:
            os.remove(f)
        # Run the subtasks
        #tasks = []
        for usher in ushers:
            seq = '>%s\n%s\n' % (usher.gene_id, usher.amino_acid_sequence)
            subtask = _append_sequence.subtask(args=(seqout_path, seq, task.id, user, version))
            subtask.delay()
        # Update the main task
        #task.subtasks = json.dumps(tasks)
        task.save()
        
    elif step == 2:
        # Save this background task
        task = BackgroundTask(
            user=user, 
            task_id=task_id,
            tool_name='cdhit_clustering_tool',
            step=step,
            pipeline_name='cu_classification_pipeline',
            input_type='file',
            db_version=version,
            misc_output=nrseq_path
        )
        task.save()
        print 'Running CD-HIT'
        os.system('/usr/local/bin/cd-hit -i %s -o %s -c 1.0' % (seqout_path, nrseq_path))
        clusters_path = nrseq_path + '.clstr'
        nrseq = SeqIO.index(nrseq_path, 'fasta')
        # Update the main task
        task.total_subtasks = len(nrseq)
        task.save()
        clusters_file = open(clusters_path, 'r')
        clusters = clusters_file.read()
        clusters = [x for x in clusters.split('>Cluster ') if x]
        #tasks = []
        for cluster in clusters:
            cluster = cluster.split('\n')
            rep = [x for x in cluster if '*' in x][0]
            rep = rep.split('>')[-1].split('...')[0]
            members = [x.split('>')[-1].split('...')[0] for x in cluster if '>' in x]
            subtask = _save_nr_cluster.subtask(args=(rep, members, task.id, dbversion))
            subtask.delay()
            #tasks.append(subtask.task_id)
        # Update the main task
        #task.subtasks = json.dumps(tasks)
        task.save()
        
        
@task
def execute_blast(user, sequence, db):
    task_id = execute_blast.request.id
    # Save this background task
    task = BackgroundTask(
        user=user, 
        task_id=task_id,
        tool_name='blast',
        input_type='sequence',
        total_subtasks=1
    )
    task.save()
    # Execute the search
    results = BlastP(sequence, db=db, identity_cutoff=60)
    # Create the temp dir, if it does not exist yet
    search_temp_dir = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'search')
    if not os.path.exists(search_temp_dir):
        os.mkdir(search_temp_dir)
    # Save the result to a pickle file
    outpath = os.path.join(settings.BASE_DIR, 'misc', 'temp', 'search', 'blast_output-%s.pickle' % task_id)
    outfile = open(outpath, 'w')
    pickle.dump(results, outfile)
    outfile.close()
    # Update the database
    task.misc_output = outpath
    task.completed = datetime.now()
    task.save()
    task.increment_success_count()