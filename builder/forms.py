from django import forms
from data.models import PiliomeDBVersion, UsherHomologyGroup
from taskqueue.models import BackgroundTask
from django.forms.utils import ErrorList
import hashlib


class UshersUploadForm(forms.Form):
    uploaded_file  = forms.FileField(required=True)
    tool = forms.CharField(widget=forms.HiddenInput())
    
    def clean(self):
        infile = self.cleaned_data['uploaded_file']
        file_hash = hashlib.md5(infile.read()).hexdigest() 
        file_exists = BackgroundTask.objects.filter(input_file_hash=file_hash)
        if file_exists:
            if not self._errors.has_key('uploaded_file'):
                self._errors['uploaded_file'] = ErrorList()
            self._errors['uploaded_file'].append('The file you selected has been uploaded')
            
            
class VersionSelectForm(forms.Form):
    _choices = ((x.db_version, 'version %s' % x.db_version) for x in PiliomeDBVersion.objects.all())
    version = forms.ChoiceField(_choices, widget=forms.Select(attrs={'class':'form-control', 'style': 'width: 140px; margin-right: 5px;'}))
    pipeline = forms.CharField(widget=forms.TextInput(attrs={'class': 'hide'}))
    

uhgs = UsherHomologyGroup.objects.all()

def uhg_name(uhg):
    if uhg.name:
        if uhg.synonyms:
            synonyms = eval(uhg.synonyms)
            return 'UHG%s - %s (%s)' % (uhg.id, uhg.name, ', '.join(synonyms))
        else:
            return 'UHG%s - %s' % (uhg.id, uhg.name)
    else:
        return 'UHG%s' % uhg.id
    
class UHGSelectForm(forms.Form):
    query = forms.ChoiceField(choices=[('uhg:%s' % int(x.id), uhg_name(x)) for x in uhgs], label='Select UHG')
    page = forms.IntegerField(widget=forms.HiddenInput(), initial=1)
