from builder.tasks import load_ushers, fetch_scaffolds, fetch_taxa, fetch_cuclusters, run_cu_search, run_cdhit_clustering
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from builder.forms import UshersUploadForm, VersionSelectForm, UHGSelectForm
from django.contrib.auth.decorators import login_required
from data.models import Gene, Taxon, PiliomeDBVersion, Scaffold, UsherHomologyGroup
from taskqueue.models import BackgroundTask
from data.utils.seq2redis import r
from django.core.paginator import Paginator
from django.views.decorators.cache import cache_page
from builder.utils.contextviewer import find_similar
from django.conf import settings
import hashlib
import json
import uuid
import os


@login_required
def version_select(request):
    pipeline = request.POST['pipeline']
    version = request.POST['version']
    url = '/administration/tools/%s/dbversion_%s/' % (pipeline, version)
    return HttpResponseRedirect(url)
    

@login_required
#@cache_page(60 * 15)
def pipelines(request, pipeline, version):
    
    # Get specific info about this version
    if version != 'select':
        dbversion = PiliomeDBVersion.objects.filter(db_version=version)[0]
        ushers = dbversion.genes.filter(piliome_annotation='usher', associated_usher__isnull=True)
        all_taxa = ushers.values('taxon_id').distinct()
        all_taxa_queryset = Taxon.objects.filter(img_taxon_id__in=all_taxa)
        taxa_with_details = dbversion.taxa.get_query_set().filter(with_genome=True)
        all_scaffolds = ushers.values('scaffold_name').distinct()
        all_scaffolds_queryset = Scaffold.objects.filter(scaffold_name__in=all_scaffolds)
        scaffolds_with_details = dbversion.scaffolds.get_query_set()
        fetched_clusters = dbversion.genes.filter(piliome_annotation='usher', associated_usher__isnull=True, cu_cluster_fetch_count__gt=0)
        context = {
            'version': version,
            'ushers_count': ushers.count(),
            'all_taxa_count': all_taxa.count(),
            'taxa_with_details_count': taxa_with_details.count(),
            'all_scaffolds_count': all_scaffolds.count(),
            'scaffolds_with_details_count': scaffolds_with_details.count(),
            'fetched_clusters_count': fetched_clusters.count(),
            'cluster_cu_pairs_searched': ushers.exclude(cluster_coded_annotations='').count()
        }
    else:
        version_select = VersionSelectForm(initial={'pipeline': pipeline})
        context = {
            'version': version,
            'version_select': version_select
        }
        
    def _get_running_tasks(tool_name, step=None):
        filters = {
            'tool_name': tool_name,
            'step': step,
            'completed__isnull': True,
            'aborted__isnull': True
        }
        ts = BackgroundTask.objects.filter(**filters)
        return ts
        
    ## Get running tasks:
    
    # Ushers loader
    ushers_loader_task = _get_running_tasks('ushers_loader')
    if ushers_loader_task:
        context['ushers_loader_task_id'] = ushers_loader_task.latest().task_id
        
    # Scaffolds fetcher
    fetch_scaffolds_task = _get_running_tasks('scaffolds_details_fetcher')
    if fetch_scaffolds_task:
        context['fetch_scaffolds_task_id'] = fetch_scaffolds_task.latest().task_id
    fetch_taxa_task = _get_running_tasks('taxa_details_fetcher')
    
    # Taxa fetcher
    if fetch_taxa_task:
        context['fetch_taxa_task_id'] = fetch_taxa_task.latest().task_id
        
    # CU clusters fethcer
    fetch_cuclusters_task = _get_running_tasks('cu_clusters_fetcher')
    if fetch_cuclusters_task:
        context['fetch_cuclusters_task'] = fetch_cuclusters_task.latest()
        context['fetch_cuclusters_task_id'] = fetch_cuclusters_task.latest().task_id
        
    # CU search tasks
    cu_search_task1 = _get_running_tasks('cu_search', step=1)
    if cu_search_task1:
        context['cu_search_task1'] = cu_search_task1.latest()
        context['cu_search_task1_id'] = cu_search_task1.latest().task_id
    cu_search_task2 = _get_running_tasks('cu_search', step=2)
    if cu_search_task2:
        context['cu_search_task2'] = cu_search_task2.latest()
        context['cu_search_task2_id'] = cu_search_task2.latest().task_id
    cu_search_tasks = BackgroundTask.objects.filter(tool_name='cu_search', completed__isnull=False)
    if cu_search_tasks.count() >= 2:
        context['cu_search_done'] = True
    else:
        context['cu_search_done'] = False
        
    # CD-HIT clustering tasks
    cdhit_clustering_task1 = _get_running_tasks('cdhit_clustering_tool', step=1)
    if cdhit_clustering_task1:
        context['cdhit_clustering_task1_id'] = cdhit_clustering_task1.latest().task_id
    cdhit_clustering_task2 = _get_running_tasks('cdhit_clustering_tool', step=2)
    if cdhit_clustering_task2:
        context['cdhit_clustering_task2_id'] = cdhit_clustering_task2.latest().task_id
    cdhit_clustering_tasks = BackgroundTask.objects.filter(tool_name='cdhit_clustering_tool', completed__isnull=False)
    if cdhit_clustering_tasks.count() >= 2:
        context['cdhit_clustering_done'] = True
    else:
        context['cdhit_clustering_done'] = False
        
    # Intitialize the upload form
    ushers_upload_form = UshersUploadForm(initial={'tool': 'ushers_loader'})    
    
    # Handling the POST requests from the forms in the img_data_loading_pipeline
    if request.method == 'POST' and pipeline == 'img_data_loading_pipeline':
        # Determine the name of the tool from which this form is sent
        try:
            tool = request.POST['tool']
        except:
            tool = ''
        
        # Dealing with the file upload from the ushers loader tool
        if tool == 'ushers_loader':
            ushers_upload_form = UshersUploadForm(request.POST, request.FILES)
            if ushers_upload_form.is_valid():
                f = request.FILES['uploaded_file']
                fpath = os.path.join(settings.MEDIA_ROOT, 'uploads', f.name)
                with open(fpath, 'wb+') as destination:
                    for chunk in f.chunks():
                        destination.write(chunk)
                ushers_loader_task_id = str(uuid.uuid4())
                ushers_load_task = load_ushers.apply_async(task_id=ushers_loader_task_id, args=(fpath, request.user, version))
                #context['ushers_load_task'] = ushers_load_task
                context['ushers_loader_task_id'] = ushers_loader_task_id
                
        # Dealing with the fetch button from the scaffolds details fetcher tool 
        elif tool == 'scaffolds_details_fetcher':
            fetch_scaffolds_task_id = str(uuid.uuid4())
            context['fetch_scaffolds_task_id'] = fetch_scaffolds_task_id
            scaffolds_fetch_task = fetch_scaffolds.apply_async(task_id=fetch_scaffolds_task_id, args=(request.user, version))
            
        # Dealing with the fetch button from the taxon details fetcher tool
        elif tool == 'taxa_details_fetcher':
            fetch_taxa_task_id = str(uuid.uuid4())
            context['fetch_taxa_task_id'] = fetch_taxa_task_id
            taxa_fetch_task = fetch_taxa.apply_async(task_id=fetch_taxa_task_id, args=(request.user, version))
            
        # Dealing with the fetch button from the CU clusters fetcher tool
        elif tool == 'cu_clusters_fetcher':
            fetch_cuclusters_task_id = str(uuid.uuid4())
            context['fetch_cuclusters_task_id'] = fetch_cuclusters_task_id
            fetch_cuclusters_task = fetch_cuclusters.apply_async(task_id=fetch_cuclusters_task_id, args=(request.user, version))
    
        # Dealing with the search button from the CU search tool
        elif tool == 'cu_search':
            cu_search_task1_id = str(uuid.uuid4())
            cu_search_task1 = run_cu_search.apply_async(task_id=cu_search_task1_id , args=(request.user, version, 1))
            context['cu_search_task1_id'] = cu_search_task1_id 
        
    
    # Handling the POST requests from the forms in the cu_classification_pipeline    
    elif request.method == 'POST' and pipeline == 'cu_classification_pipeline':    
        # Determine the name of the tool from which this form is sent
        tool = request.POST['tool']
        
        # Dealing the run button from the CD-HIT clustering tool
        if tool == 'cdhit_clustering_tool':
            cdhit_clustering_task1_id = str(uuid.uuid4())
            cdhit_clustering_task1 = run_cdhit_clustering.apply_async(task_id=cdhit_clustering_task1_id, args=(request.user, version, 1))
            context['cdhit_clustering_task1_id'] = cdhit_clustering_task1_id
        
    context['pipeline'] = pipeline
    context['path'] = request.path
    context['ushers_upload_form'] = ushers_upload_form

    return render_to_response('interface/administration.html', context, RequestContext(request))
    
    
def context_viewer_launcher(request):
    form = UHGSelectForm()
    context = {'form': form}
    return render_to_response('builder/context_viewer_launcher.html', context, RequestContext(request))

    
def _chunks(l, n):
    """ Yield successive n-sized chunks from l """
    chunks = []
    for i in xrange(0, len(l), n):
        chunks.append(l[i:i+n])
    return chunks

    
def context_viewer(request):
    context = {}
    page = 1 # default
    if 'query' in request.GET.keys():
        query = request.GET['query']
        if 'uhg:' in query:
            uhgs = query.split(':')[-1].split(',')
            members = []
            for uhg in uhgs:
                uhg = UsherHomologyGroup.objects.filter(id=int(uhg))[0]
                uhg_members = list(set(uhg.get_members()))
                members += uhg_members
                #members += r.lrange('uhg%s_ushers' % uhg, 0, -1)
            print len(members)
            # Screen based on genus, if given
            if 'genus' in request.GET.keys():
                genus = request.GET['genus']
                subset = []
                for member in members:
                    gene = Gene.objects.filter(gene_id=member)[0]
                    taxon = Taxon.objects.filter(img_taxon_id=gene.taxon_id)[0]
                    td = taxon.get_levels()
                    if td['genus'] == genus:
                        subset.append(member)
                members = subset
            page = request.GET['page']
            members = _chunks(members, 20)
            try:
                context['input_data'] = ','.join(members[int(page) - 1])
            except IndexError:
                context['input_data'] = ''
        elif 'id:' in query:
            gids = query.split(':')[1].split(',')
            subset = []
            for gid in gids:
                gene = Gene.objects.filter(id=gid, associated_usher__isnull=True)[0]
                subset.append(gene.gene_id)
            members = subset
            members = _chunks(members, 20)
            try:
                context['input_data'] = ','.join(members[int(page) - 1])
            except IndexError:
                context['input_data'] = ''
        else:
            context['input_data'] = query
        context['page_number'] = int(page)
        context['pages'] = len(members)
        context['uhg_id'] = int(uhg.id)
    return render_to_response('builder/context_viewer.html', context, RequestContext(request))
    
    
def context_viewer_find(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        query_gene = data['queryGene']
        genes = data['genes'].split(',')
        if data['method'] == 'blast':
            results = find_similar(query_gene, genes)
        else:
            results = []
        return HttpResponse(json.dumps(results), mimetype="application/json")

        
def context_viewer_get_uhg_members(request):
    uhgID = request.GET['uhg']
    members = []
    if 'span' in request.GET.keys():
        span = request.GET['span']
    else:
        span = 'nr'
    uhg = UsherHomologyGroup.objects.filter(id=int(uhgID))
    if uhg:
        if span == 'nr':
            members = eval(uhg[0].members)
        elif span == 'all':
            members = uhg[0].get_members()
    if 'page' in request.GET.keys():
        page = int(request.GET['page'])
    else:
        page = 1
    members = list(set(members))
    sublist = _chunks(members, 40)[page - 1]
    print ','.join(sublist)
    return HttpResponse(','.join(sublist))
        
        
def context_viewer_save_annotation(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        genes = data['genes']
        annotation = data['annotation']
        for gene in genes:
            g = Gene.objects.filter(id=gene)
            if g.count():
                for x in g:
                    x.piliome_annotation = annotation
                    x.save()
        return HttpResponse('Done!')