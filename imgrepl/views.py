from django.http import HttpResponse
from django.conf import settings
from builder.utils.genomereader import GenomeIO
from django.db import reset_queries
import json
import os


def fetch_data(request):
    section = request.GET['section']
    if section == 'GeneDetail':
        taxon_id = request.GET['taxon_id']
        genome_path = os.path.join(settings.PILIOMEDB_FILES, 'genomes', taxon_id)
        if not os.path.exists(genome_path):
            genome_path = os.path.join(settings.PILIOMEDB_FILES, 'genbank_files', '%s.gbk' % taxon_id)
        gene_id = request.GET['gene_id']
        genome = GenomeIO(genome_path, gene_id)
        data = genome[gene_id]
        del genome
        reset_queries()
    return HttpResponse(json.dumps(data), content_type="application/json")
