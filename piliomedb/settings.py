"""
Django settings for piliomedb project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PLATFORM = os.uname()[0]


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'dc5z=i9_dxl_nuqo%l!e93*p8ebvqoijma$u#&vg9!u6vq9jc^'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = False

if PLATFORM == 'Linux':
    ALLOWED_HOSTS = ['.piliomedb.org']
else:
    ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'kombu.transport.django',
    'django_extensions',
    'django_forms_bootstrap',
    'djangobower',
    'gunicorn',
    'redis',
    'djsupervisor',
    'tastypie',
    'data',
    'interface',
    'taskqueue',
    'builder',
    'imgrepl',
    'simplesearch'
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request',
    'interface.context_processors.analytics'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'piliomedb.urls'

WSGI_APPLICATION = 'piliomedb.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE':'django.db.backends.mysql',
        'NAME': 'piliomedb',
        'USER': 'root',
        'PASSWORD': 'toor',
        'HOST': '',
        'PORT': '',
    }
}

# Celery configuration

BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
#CELERY_DEFAULT_RATE_LIMIT = '1/s'

# Configuration for bower

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'djangobower.finders.BowerFinder'
)

BOWER_COMPONENTS_ROOT = os.path.join(BASE_DIR, 'piliomedb', 'bower')

BOWER_PATH = '/usr/local/share/npm/bin/bower'

BOWER_INSTALLED_APPS = (
    'jquery',
    'bootstrap',
    'jquery.cookie',
    'snap.svg'
)

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, 'piliomedb', 'static')

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'piliomedb', 'media')

LOGIN_URL = '/login'


# Path to the PiliomeDB files

PILIOMEDB_FILES = os.path.join(os.path.dirname(BASE_DIR), 'piliomedb_files')


# Connection to Redis db 1
import redis

REDIS_CONNECTION_POOL = redis.ConnectionPool(host='localhost', port=6379, db=1)


# Cache settings

CACHES = {
    #'default': {
    #    'BACKEND': 'redis_cache.RedisCache',
    #    'LOCATION': 'localhost:6379',
    #    'OPTIONS': {
    #        'DB': 2,
    #        'PASSWORD': '',
    #        'PARSER_CLASS': 'redis.connection.HiredisParser'
    #    },
    #},
    'default': {
        'BACKEND': 'uwsgicache.UWSGICache',
    }
}

GOOGLE_ANALYTICS_KEY = 'UA-36679313-1'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'joemar.ct@gmail.com'
EMAIL_HOST_PASSWORD = 'gdlr dmma pkes tpfd'

ADMINS = ['joemar.ct@gmail.com']
