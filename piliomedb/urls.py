from django.conf.urls import patterns, include, url
from tastypie.api import Api
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from data.api import GeneResource, ScaffoldResource, TaxonResource

from django.contrib import admin
admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(TaxonResource())
v1_api.register(ScaffoldResource())
v1_api.register(GeneResource())
#v1_api.register(UsherResource())
#v1_api.register(PfamResource())
#v1_api.register(COGResource())

urlpatterns = patterns('',
    url(r'^$', 'interface.views.home'),
    url(r'^browse/$', 'interface.views.browse'),
    url(r'^browse_table_view/$', 'interface.views.browse_table_view'),
    url(r'^uhg_info/$', 'interface.views.uhg_info'),
    url(r'^tree_comparison/$', 'interface.views.tree_comparison'),
    url(r'^tree_comparison2/$', 'interface.views.tree_comparison2'),
    url(r'^search/by_id_keyword/$', 'interface.views.search'),
    url(r'^search/by_id_keyword/results/$', 'interface.views.search_results'),
    url(r'^search/by_sequence/results/(?P<task_id>.*)/$', 'interface.views.search_results'),
    url(r'^search/by_sequence/$', 'interface.views.search'),
    url(r'^search/by_pdb/$', 'interface.views.search'),
    url(r'^download/$', 'interface.views.download'),
    url(r'^user_guide/$', 'interface.views.user_guide'),
    url(r'^tools/context_viewer/launcher/$', 'builder.views.context_viewer_launcher'),
    url(r'^tools/context_viewer/$', 'builder.views.context_viewer'),
    url(r'^tools/context_viewer/find_similar/$', 'builder.views.context_viewer_find'),
    url(r'^tools/context_viewer/get_uhg_members/$', 'builder.views.context_viewer_get_uhg_members'),
    url(r'^tools/context_viewer/save_annotation/$', 'builder.views.context_viewer_save_annotation'),
    url(r'^tools/conservation_mapper', 'interface.views.conservation_mapper'),
    url(r'^downloads/output_file/(?P<task_id>.*)/(?P<file_name>.*)', 'interface.views.serve_download_file'),
    url(r'^send_feedback/', 'interface.views.send_feedback'),
    url(r'^downloads/file/combined_uhgs.hmm', 'interface.views.download_profiles'),
    url(r'^login/$', 'interface.views.login_view'),
    url(r'^logout/$', 'interface.views.logout_view'),
    url(r'^administration/$', 'interface.views.administration'),
    url(r'^administration/tools/(?P<pipeline>.*)/dbversion_(?P<version>.*)/$', 'builder.views.pipelines'),
    url(r'^administration/version_select/$', 'builder.views.version_select'),
    url(r'^administration/curation/add_reference/$', 'interface.views.curation_add_reference'),
    url(r'^curation/create_reference/$', 'interface.views.create_reference'),
    url(r'^references/$', 'interface.views.list_references'),
    url(r'^task/$', 'taskqueue.views.task_control'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^imgrepl/$', 'imgrepl.views.fetch_data'),
    url(r'^api/', include(v1_api.urls)),
)

urlpatterns += staticfiles_urlpatterns()