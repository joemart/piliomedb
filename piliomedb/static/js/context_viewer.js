/*
# This is the controller file for the Gene Context Viewer App
# This app is intended to perform drawing of genes in their genomic
# context, hence the Gene Context Viewer App. This is developed
# as part of the PiliomeDB Tools.
#
# Author: Joemar Taganna
# Website: www.joemartaganna.com
 */


// Initialize the angular app
var app = angular.module("geneContextViewer", ['ui.bootstrap']);
app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});


// Add PATCH support to Angular HTTP
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.headers.patch = {
        'Content-Type': 'application/json;charset=utf-8'
    }
}])


// Object prototype modification for determining size or length of
// associative arrays but this also works for ordinary arrays (a.k.a. list)
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

// Array prototype modification for removing elements
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};


// This the main controller function
function ContextViewerCtrl($scope, $http){

    /*
    First comes the definitions and assignments
    of global variables
     */

    // Set defaults for custom controls
    $scope.customCtrl = {
        //name: false,
        //color: true,
        add: false,
        //picker: false,
        //highlight: true,
        clear: false
    };

    // Storage object for gene selections
    $scope.currentSelections = {'default':[]};

    // Storage object for saved selections and convenience
    // array that list their names
    $scope.savedSelections = {};
    $scope.savedSelectionNames = [];

    // Storage objects for gene OID to name / color mappings
    // These were created in order to avoid iterating over the
    // saved selections object everytime the maps are redrawn
    $scope.colorMap = {};
    $scope.textColorMap = {};
    $scope.nameMap = {};
	
	// Associated usher map
	$scope.geneIdMap = {};

    // Array of names of saved selections that
    // are loaded to the viewer
    $scope.loadedSelections = [];

    // Variable to store the progress of drawing
    $scope.mapsDrawn = 0;

    // Define other defaults
    $scope.inputType;
    $scope.geneNames = "noName";
    $scope.queryStrand = "identical";
    $scope.geneColors = "plain";
    $scope.inputSelectionColor = "#000000";
    $scope.inputSelectionName = "";
    $scope.displayResults = false;
    $scope.queryHighlight = 1;
    $scope.currentSelectedGene = 'gene';
    $scope.defaultTextColor = '#000000';
    $scope.defaultGeneColor = "#000000";
    $scope.useColorFor = 'geneOID:gene'; // gene or text

    // Storage object for the scaffolds
    $scope.scaffolds = [];

    // Stores all the query genes that are fetched
    $scope.genes = [];

    $scope.queriesCount = 0;

    //$scope.genesToPfam = {};

    // List of all genes
    $scope.allGenes = [];
	
    /*
    What follows are functions that are invoked from the HMTL
    or from within this controller
     */
	
	// Check for pre-loaded input data
	$scope.init = function() {
		var inputData = $("#input-data");
		if (inputData.text().length > 1) {
			$("#initial-form").hide();
			$scope.gene_oids = inputData.text();
			$scope.visualize(40000);
		}
	}

    // The fetch data function
    $scope.fetch_data = function(sl){

        $scope.scaLength = sl;

        // Get the raw string input
        // var goid_string = $scope.gene_oids;

        if ($scope.gene_oids == 'example') {
            $scope.gene_oids = '648232874,640800331,2511902644,651165695,2511786508,637062059,2511851918';
        }

		function fetchGenes() {
		
			var goid_string = $scope.gene_oids;
		
	        // If it is not empty
	        if ($scope.scaffolds.length == 0 && goid_string.length > 0) {
			
	            // Split this string by the comma separators
	            var raw_gene_oids = goid_string.split(",");

	            // Sanitize each gene_oid by removing white spaces
	            var gene_oids = [];
	            for (var h=0; h < raw_gene_oids.length; h++) {
	                gene_oids.push(raw_gene_oids[h].replace(/\s+/g,""));
	            }

	            // Fetch the genes within the area defined by the viewing window
	            // By default this viewing window is set to 40K nucleotides
	            var scaffold_id, vstart, vend;
	            for (var i=0; i < gene_oids.length; i++) {
	                $scope.queriesCount += 1;
	                var gene_oid = gene_oids[i];
	                var qurl = '/api/v1/gene/?gene_id='+ gene_oid + '&associated_usher__isnull=True&format=json';
	                $http.get(qurl).success(function(qdata){
                    
						var query_gene = qdata.objects[0];
	                    $scope.genes.push(query_gene);
						scaffold_id = query_gene.scaffold_name;
					
						/*
	                    var qstart = query_gene.start;
	                    var qend = query_gene.end;
	                    var qlength = query_gene.dna_length;
	                    var qlenhalf = qlength / 2;
	                    vstart = Math.round((qstart + qlenhalf) - ($scope.scaLength/2));
	                    vend = Math.round((qend - qlenhalf) + ($scope.scaLength/2));

	                    var surl = '/api/v1/gene/?img_orf_type=CDS&scaffold_name='+scaffold_id+'&start__gt='+vstart+'&end__lt='+vend+'&associated_usher__isnull=False&format=json';
	                    */
						var surl = '/api/v1/gene/?img_orf_type=CDS&associated_usher__id='+query_gene.id+'&scaffold_name='+scaffold_id+'&format=json';
						$http.get(surl).success(function(sdata){
						
							sdata.objects.push(query_gene);

	                        // Add each gene to the list of all genes
	                        jQuery.each(sdata.objects, function(index, value){
							
								// If annotation is present, add the gene to the saved selections
								// under the name of the given annotation
								var annot = value.piliome_annotation;
								var annotColor = {
									'usher': 'red',
									'chaperone': 'gold',
									'adhesin': 'green',
									'major subunit': 'cyan'
								};
							
								if (value.piliome_annotation) {
								
									var annotObject = {};
				                    annotObject[value.gene_id] = {
				                        'genecolor': annotColor[annot],
				                        'name': '',
				                        'textcolor': ''
				                    };
								
									if (jQuery.inArray(annot, $scope.savedSelectionNames) == -1) {
										$scope.savedSelectionNames.push(annot);
										$scope.loadedSelections.push(annot);
										$scope.savedSelections[annot] = [annotObject];
										$scope.colorMap[value.gene_id] = annotColor[annot];
									} else {
										$scope.savedSelections[annot].push(annotObject);
										$scope.colorMap[value.gene_id] = annotColor[annot];
									}
								
								}
							
								// Append the gene to the list of all genes
	                            $scope.allGenes.push(value.gene_id);
								
								// Append the gene to the associated usher map
								$scope.geneIdMap[value.gene_id] = value.id;
	                        });

	                        // Store genes to scaffolds
	                        $scope.scaffolds.push(
	                            {
	                                'qgene': query_gene,
	                                'genes': sdata.objects.reverse()
	                            }
	                        );
	                    })
	                })
	            }
	        }
		}
		
		if ($scope.gene_oids.indexOf(":UHG=") != -1) {
			var genoids_list = $scope.gene_oids.split(":")
			var params = new Object();
			jQuery.each(genoids_list, function(index,value){
				vlist = value.split('=');
				params[vlist[0]] = vlist[1];
			});
			murl = '/tools/context_viewer/get_uhg_members/?uhg='+params['UHG']+'&span='+params['span']+'&page='+params['page'];
			$http.get(murl).success(function(mdata){
				$scope.gene_oids = mdata;
				fetchGenes();
				//goidStringObject['goidString'] = 'mama';
			});
		} else {
			fetchGenes();
		}
		
    };

    // Begin the visualization process...
    // For IMG Gene Object IDs input:
    $scope.visualize = function(sl){
        $scope.inputType = "IMG Gene Object IDs";
        // Execute fetching of data
        $scope.fetch_data(sl);
    };

    // For exported data input:
    $scope.loadAndVisualize = function(sl){
        $scope.scaLength = sl;
        $scope.customize = 1;
        $scope.geneColors = "plain";
        $scope.queryStrand = "identical";
        // Populate genes, allGenes, and scaffolds with info from exported data
        var data = jQuery.parseJSON($scope.exporteddata);
        $scope.inputType = "exported data";
        $scope.genes = data.queryGenes;
        $scope.queriesCount = data.queryGenes.length;
        $scope.scaffolds = data.scaffolds;
        jQuery.each(data.scaffolds, function(index, scaffold){
            jQuery.each(scaffold.genes, function(index, gene){
                $scope.allGenes.push(gene.gene_id);
            });
        });
        $scope.savedSelections = data.savedSelections;
        $scope.savedSelectionNames = Object.keys(data.savedSelections);
        jQuery.each($scope.savedSelectionNames, function(index, name){
            $scope.loadSelection(name);
        });
    };

    // Function for updating the color, name and textcolor maps
    $scope.updateColorNameMaps = function() {
        var found = 0;
        if (Object.size($scope.currentSelections) > 0) {
            jQuery.each($scope.currentSelections, function(selection, members){
                if (Object.size(members) > 0) {
                    jQuery.each(members, function(index, member){
                        found += 1;
                        var key = Object.keys(member)[0]
                        $scope.colorMap[key] = member[key]['genecolor'];
                        if (!$scope.nameMap[key]) {
                            $scope.nameMap[key] = member[key]['name'];
                        }
                        if (!$scope.textColorMap[key]) {
                            $scope.textColorMap[key] = member[key]['textcolor'];
                        }
                    })
                }
            });
        }

        if (found == 0) {
            $scope.colorMap = {};
        }
    };

    // Function that clears the current selections
    $scope.clearCurrentSelections = function(){
        $scope.currentSelections = {'default':[]};
        $scope.colorMap = {};
        $scope.nameMap = {};
    };

    // Function that checks if selection name is
    // currently loaded to viewer
    $scope.selectionIsLoaded = function(name){
        return jQuery.inArray(name, $scope.loadedSelections) > -1;
    };

    // Function that lists all currently selected genes
    $scope.listSavedGenes = function(){
        var selectedGenes = [];
        jQuery.each($scope.savedSelections, function(selname, members){
            jQuery.each(members, function(index, member){
                var gid = Object.keys(member)[0];
                selectedGenes.push(gid);
            });
        });
        return selectedGenes;
    };


    /*
     What follows are event listeners for the events emitted
     from various directives define further below
     */

    // Event listener for hiding the color picker
    $scope.$on('hideColorPicker', function(){
        //alert('Action triggered!');
        $scope.colorPicker = 0;
    });

    // Event listener that adds genes to the selections
    $scope.$on('addToSelections', function(event, args){
        $scope.$apply(function(){
            
            var gid = args.geneOID.split(" (")[0];
            var geneObject = {};
            var assignedName;
            var savedGenes = $scope.listSavedGenes();

            // Proceed only if this gene has not been added to any saved selection
            if (jQuery.inArray(gid, savedGenes) < 0) {
                if ($scope.nameMap[gid]) {
                    assignedName = $scope.nameMap[gid];
                } else {
                    assignedName = $scope.inputSelectionName;
                }
                geneObject[gid] = {
                    'genecolor': $scope.inputSelectionColor,
                    'name': assignedName,
                    'textcolor': $scope.defaultTextColor
                };

                // jQuery's inArray function returns -1 if element is not present
                // Otherwise it returns the index
                var currGenes = []; // Genes in the default selection
                if (Object.size($scope.currentSelections) > 0) {
                    jQuery.each($scope.currentSelections['default'], function(key,value){
                        currGenes.push(Object.keys(value)[0]);
                    });
                }

                var defaultSel = $scope.currentSelections['default'];
                if (jQuery.inArray(gid, currGenes) < 0) {
                    // Add the gene to default selection if it does not exist yet
                    defaultSel.push(geneObject);
                    // Update the color map
                    $scope.colorMap[gid] = $scope.inputSelectionColor;
                } else {
                    // Clicking a selected gene should remove it from the selection
                    jQuery.each($scope.currentSelections['default'], function(index, member){
                        var key = Object.keys(member)[0];
                        if (gid == key) {
                            defaultSel.remove(member);
                            delete $scope.colorMap[gid];
                        }
                    });
                }
            }
        });
    });

    // Event listener that sets the custom modifications
    $scope.$on('customModification', function(event, args){
        $scope.customCtrl = jQuery.parseJSON(args.params);

        // If clear button is pressed
        if ($scope.customCtrl.clear == true) {
            if (Object.size($scope.currentSelections['default']) > 0) {
                var c = confirm("Are you sure you want to clear your current selections?");
                if (c == true) {
                    // Delete the color and name mappings
                    jQuery.each($scope.currentSelections['default'], function(index, member){
                        var gid = Object.keys(member)[0];
                        delete $scope.colorMap[gid];
                        delete $scope.nameMap[gid];
                    });
                    // Set the default selection to blank
                    $scope.currentSelections['default'] = [];
                    $scope.customCtrl.clear = false;
                    $scope.customCtrl.save = false;
                } else {
                    $scope.customCtrl.clear = false;
                }
            } else {
                $scope.customCtrl.clear = false;
            }
        }

        // When save selection button is clicked and the default selection contains any gene
        if ($scope.customCtrl.save == true && Object.size($scope.currentSelections['default']) != 0) {

            // Save the current selection
            // Ask first for a name
            var p = prompt("Give it a name: ");
            // If a name is entered
            if (p.length > 0) {
                // Transfer all the objects in the default to the saved selection
                $scope.savedSelections[p] = [];
                jQuery.each($scope.currentSelections['default'], function(index, item){
                   $scope.savedSelections[p].push(item);
                });

                // Clear the default/current selections
                $scope.currentSelections['default'] = [];
                // Load the newly saved selection
                $scope.loadSelection(p);
                // Toggle back the save button
                $scope.customCtrl.save = false;
                // Set the gene color input to default
                $scope.inputSelectionColor = $scope.defaultGeneColor;
            } else {
                // If no name is entered, just toggle back the button
                $scope.customCtrl.save = false;
            }

            // Get the names of the saved selections into
            // the savedSelectionNames list for easy checking
            var names = [];
            jQuery.each($scope.savedSelections, function(key, value) {
                names.push(key)
            });
            $scope.savedSelectionNames = names;

        // When it is clicked but the default selection does not contain anything
        } else if (Object.size($scope.currentSelections['default']) == 0 && $scope.customCtrl.save == true) {
            // Toggle it back to off position
            $scope.customCtrl.save = false;
        }

        // When the add to existing button is clicked
        if ($scope.customCtrl.add == true) {

            var pd = prompt('Enter name of destination');
            if (pd.length > 0 && jQuery.inArray(pd, $scope.savedSelectionNames) > -1) {

                jQuery.each($scope.currentSelections['default'], function(index, object){
                    $scope.savedSelections[pd].push(object);

                    // Clear the default/current selections
                    $scope.currentSelections['default'] = [];
                    // Load and unload destination selection
                    $scope.unloadSelection(pd);
                    $scope.loadSelection(pd);

                })

            } else {
                $scope.customCtrl.add = false;
            }

            // Toggle back the button
            $scope.customCtrl.add = false;
        }

    });

    // Event listener that increment the count of maps drawn
    $scope.$on('incrementMapsDrawn', function(){
        $scope.mapsDrawn += 1;
    });

    // Event listener that registers the current selected gene
    $scope.$on('setCurrentSelectedGene', function(event, args){
        $scope.$apply(function(){
            $scope.currentSelectedGene = args.geneOID;
        });
    });

    // Event listener for exporting saved selections
    $scope.$on('exportSavedSelections', function(){
        //alert('Export invoked!');
        var exportData = {};
        var queryGenes = [];
        jQuery.each($scope.genes, function(index, query){
            queryGenes.push(query.gene_oid);
        });
        exportData['scaffolds'] = $scope.scaffolds;
        exportData['queryGenes'] = queryGenes;
        exportData['savedSelections'] = $scope.savedSelections;
        var textArea = jQuery("#export-textarea");
        textArea.css('display', 'block');
        textArea.html(JSON.stringify(exportData));

        $scope.export = 0;
    });

    /*
    What follows are actions from the
    menu list of saved selections and the context menus
    */

    // Load a saved selection to the current selections
    $scope.loadSelection = function(name){
        $scope.currentSelections[name] = [];
        for (var i=0; i < $scope.savedSelections[name].length; i++) {
            $scope.currentSelections[name].push($scope.savedSelections[name][i]);

        }

        // Update the loaded selections list
        $scope.loadedSelections.push(name);

        // Mass update the color and name maps
        $scope.updateColorNameMaps();
    };

    // Unload a selection
    $scope.unloadSelection = function(name){

        // Delete from current selections
        if ($scope.currentSelections[name]) {
            jQuery.each($scope.currentSelections[name], function(key, value){
                var gid = Object.keys(value)[0];

                delete $scope.colorMap[gid];
                delete $scope.nameMap[gid];
                delete $scope.textColorMap[gid];
            });
            delete $scope.currentSelections[name];

            // Update the loaded selections list
            $scope.loadedSelections.remove(name);
        }
    };

    // Assign color to genes of a selection
    $scope.assignColor = function(name){
        $scope.colorPicker = 1;
        $scope.useColorFor = name+':genecolor:selection';
    };

    // Assign textcolor to genes of a selection
    $scope.assignTextColor = function(name){
        $scope.colorPicker = 1;
        $scope.useColorFor = name+':textcolor:selection';
    };

    // Assign name to selection
    $scope.assignName = function(name){
        var assignedName = prompt('Enter the name');
        if (assignedName.length > 0) {
            jQuery.each($scope.savedSelections[name], function(index, member){
                var gid = Object.keys(member)[0];
                member[gid]['name'] = assignedName;
                $scope.nameMap[gid] = assignedName;
            });
        }
    };

    // Delete selection
    $scope.deleteSelection = function(name){
        if (jQuery.inArray(name, $scope.loadedSelections) > -1) {
            $scope.unloadSelection(name);
        }
        delete $scope.savedSelections[name];
        $scope.updateColorNameMaps();
        $scope.savedSelectionNames.remove(name);
    };
	
    // Save annotation to database
    $scope.saveToDatabase = function(name){
		var postData = {};
		postData['genes'] = [];
		jQuery.each($scope.savedSelections[name], function(index, value){
			var geneId = $scope.geneIdMap[Object.keys(value)[0]];
			postData['genes'].push(geneId);
			//console.log(Object.keys(value)[0], geneId);
		});
		postData['annotation'] = name;
		$http({'method': 'POST', 'url': '/tools/context_viewer/save_annotation/', 'data': postData}).success(function(res){
			alert('Done!');
		});
    };

    // Check if a name exists for a gene
    $scope.nameExists = function(gid) {
        return $scope.nameMap[gid];
    };
	
	// Display gene details
	$scope.displayGeneDetails = function(name){
		alert(name);
	};

    // Set the name of an individual gene
    $scope.assignIndividualGeneName = function(name){
        var assignedName = prompt('Enter the name');
        if (assignedName.length > 0) {
            $scope.nameMap[$scope.currentSelectedGene] = assignedName;

            // Find the selection where this gene belongs
            // Then save the change made
            var found = 0;
            jQuery.each($scope.currentSelections, function(selname, members){
                jQuery.each(members, function(index, member){
                    var gid = Object.keys(member)[0];
                    if (name == gid) {
                        found += 1;
                        member[gid]['name'] = assignedName;
                        $scope.nameMap[gid] = assignedName;
                    }
                });
            });

            // If not found, add to default selection
            if (found == 0) {
                var geneObject = {};
                geneObject[name] = {
                    'genecolor': $scope.inputSelectionColor,
                    'name': assignedName,
                    'textcolor': $scope.defaultTextColor
                };

                $scope.currentSelections['default'].push(geneObject);
            }
        }
    };

    // Set the color of an inidividual gene
    $scope.assignIndividualColor = function (name){
        $scope.colorPicker = 1;
        $scope.useColorFor = name+':genecolor';

        // Find the selection where this gene belongs
        // Then save the change made
        var found = 0;
        jQuery.each($scope.currentSelections, function(selname, members){
            jQuery.each(members, function(index, member){
                var gid = Object.keys(member)[0];
                if (name == gid) {
                    found += 1;
                    member[gid]['genecolor'] = $scope.inputSelectionColor;
                    $scope.colorMap[gid] = $scope.inputSelectionColor;
                }
            });
        });

        // If not found, add to default selection
        if (found == 0) {
            var geneObject = {};
            geneObject[name] = {
                'genecolor': $scope.inputSelectionColor,
                'name': '',
                'textcolor': $scope.defaultTextColor
            };

            $scope.currentSelections['default'].push(geneObject);
        }
    };
	
  	// Remove the annotation
  	$scope.removeGeneAnnotation = function(name) {
  		var raurl = '/api/v1/gene/' + $scope.geneIdMap[name.split(" (")[0]] + '/';
  		var patchData = {
  			'piliome_annotation': '', 
  			'gene_id': name.split(" (")[0]};
  		var headers = {"X-HTTP-Method-Override": "PATCH"};
  		var params = {
  			method: 'PATCH', 
  			data: patchData, 
  			headers: headers,
  			url: raurl};
  		// Send the request
  		$http(params).success(function(){
  			$scope.removeFromSelection(name.split(" (")[0]);
  		});
  	};

    // Set the text color of an individual gene
    $scope.assignIndividualTextColor = function(name){
        $scope.colorPicker = 1;
        $scope.useColorFor = name+':textcolor';

        // Find the selection where this gene belongs
        // Then save the change made
        var found = 0;
        jQuery.each($scope.currentSelections, function(selname, members){
            jQuery.each(members, function(index, member){
                var gid = Object.keys(member)[0];
                if (name == gid) {
                    found += 1;
                    member[gid]['textcolor'] = $scope.inputSelectionColor;
                    $scope.textColorMap[gid] = $scope.inputSelectionColor;
                }
            });
        });

        // If not found, add to default selection
        if (found == 0) {
            var geneObject = {};
            geneObject[name] = {
                'genecolor': "#000000",
                'name': '',
                'textcolor': $scope.inputSelectionColor
            };

            $scope.currentSelections['default'].push(geneObject);
        }
    };

    // Find similar sequences
    $scope.findSimilar = function(name, method){

        function increase_brightness(hex, percent){
            // strip the leading # if it's there
            hex = hex.replace(/^\s*#|\s*$/g, '');

            // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
            if(hex.length == 3){
                hex = hex.replace(/(.)/g, '$1$1');
            }

            var r = parseInt(hex.substr(0, 2), 16),
                g = parseInt(hex.substr(2, 2), 16),
                b = parseInt(hex.substr(4, 2), 16);

            return '#' +
                ((0|(1<<8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
                ((0|(1<<8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
                ((0|(1<<8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
        }
		
        // List the genes that have been saved
        var savedGenes = [];
        jQuery.each($scope.savedSelectionNames, function(index, selname){
            jQuery.each($scope.savedSelections[selname], function(index, member){
                var geneOID = Object.keys(member)[0];
                savedGenes.push(geneOID);
            });
        });

		if (method == 'blast') {
			
	        var fsurl = "/tools/context_viewer/find_similar/";
			var genes = $scope.allGenes.join(',');
			var postData = {'method': 'blast', 'queryGene': name.split(' (')[0], 'genes': genes};
	        $http({method: 'POST', data: postData, url: fsurl}).success(function(fsdata){

	            if (fsdata.length > 0) {

	                jQuery.each(fsdata, function(index, match){

	                    var geneObject = {};
	                    geneObject[match.gene_id] = {
	                        'genecolor': increase_brightness("#0000ff", 100 - match.identity),
	                        'name': '',
	                        'textcolor': ''
	                    };

	                    // Proceed only if the match cannot be found in any of the saved selections
	                    if (jQuery.inArray(match.gene_id, savedGenes) == -1) {

	                        // List the genes in the default selection
	                        var defGenes = [];
	                        jQuery.each($scope.currentSelections['default'], function(index, member){
	                            var geneOID = Object.keys(member)[0];
	                            defGenes.push(geneOID);
	                        });

	                        // Add to default selection if it's not there yet
	                        if (jQuery.inArray(match.gene_id, defGenes) == -1) {
	                            $scope.currentSelections['default'].push(geneObject);
	                            $scope.colorMap[match.gene_id] = increase_brightness("#0000ff", 100 - match.identity);
	                        }
							
	                    }
	                });
	            }
	        });
			
		} else if (method == 'domainMatching') {
			
	        var fsurl = "/tools/context_viewer/find_similar/";
			var genes = $scope.allGenes.join(',');
			var postData = {'method': 'domainMatching', 'queryGene': name.split(' (')[0], 'genes': genes};
			$http({method: 'POST', data: postData, url: fsurl}).success(function(fsdata){
				
				if (fsdata.length > 0) {
					jQuery.each(fsdata, function(index, match){
						
	                    var geneObject = {};
	                    geneObject[match.gene_id] = {
	                        'genecolor': "#0000ff",
	                        'name': '',
	                        'textcolor': ''
	                    };
						
	                    // Proceed only if the match cannot be found in any of the saved selections
	                    if (jQuery.inArray(match.gene_id, savedGenes) == -1) {
							
	                        // List the genes in the default selection
	                        var defGenes = [];
	                        jQuery.each($scope.currentSelections['default'], function(index, member){
	                            var geneOID = Object.keys(member)[0];
	                            defGenes.push(geneOID);
	                        });

	                        // Add to default selection if it's not there yet
	                        if (jQuery.inArray(match.gene_id, defGenes) == -1) {
	                            $scope.currentSelections['default'].push(geneObject);
	                            $scope.colorMap[match.gene_id] = increase_brightness("#ff0000", 100 - match.identity);
	                        }
	                    	
	                    }
					});
				}
			});
		}
    };

    // Remove gene from selection
    $scope.removeFromSelection = function(geneName){
        var name = geneName.split(" (")[0]
        // Remove from current selections
        jQuery.each($scope.currentSelections, function(selname, members){
            if (selname != 'default') {
                jQuery.each(members, function(index, member){
                    var gid = Object.keys(member)[0];
                    if (name == gid) {

                        // Delete from current selection, if loaded to view
                        if (jQuery.inArray(selname, $scope.loadedSelections) > -1) {
                            $scope.currentSelections[selname].remove(member);

                            // Delete from saved selection
                            $scope.savedSelections[selname].remove(member);
                        }
                    }
                });
            }
        });

        // Remove entries from color maps
        if ($scope.colorMap[name]) {
            delete $scope.colorMap[name];
        }
        if ($scope.nameMap[name]){
            delete $scope.nameMap[name];
        }
        if ($scope.textColorMap[name]){
            delete $scope.textColorMap[name];
        }

    };

    jQuery('div.color-picker').mouseleave(function(){
        $scope.$apply(function(){
            $scope.colorPicker = 0;
            $scope.useColorFor = 'nothing';
            $scope.inputSelectionColor = "#000000";
        });
    });
}


/*
Then comes the directives
*/

// This directive governs the behavior of the submit button,
// signals the display of the results and scrolls the page to the
// start position of the results container

app.directive('processingStatus',function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs)
        {

            scope.$watch('mapsDrawn + queriesCount + qgenes', function(){

                if (scope.mapsDrawn != 0){
                    if (scope.mapsDrawn >= scope.queriesCount) {
                        jQuery("#loading").remove();
                        jQuery('#initial-form').hide();
                    }
                }
            });

            element.css("display", "none");
            scope.$watch("exporteddata + gene_oids + genes", function(){
                if (scope.gene_oids && scope.gene_oids.length > 0 || scope.exporteddata && scope.exporteddata.length > 0) {
                    element.css("display", "block");
                    element.click(function(){
                        if (scope.genes && scope.genes.length == 0) {
                            element.html("Processing...")
                        }
                    })
                } else {
                    element.css("display", "none");
                }

                if (scope.genes && scope.genes.length > 0) {
                    element.remove();
                    scope.displayResults = true;
                }
            })
        }
    }
});


// This directive does the heavy lifting for the drawing.
// It receives all the necessary models and processes them for
// the drawing of the context map per query gene.

app.directive('contextMap', function(){
    return {
        restrict: 'E',
        scope: {
            // Bind the attributes into local scope
            scaffold: '@',
            mapwidth: '@',
            names: '@',
            scalength: '@',
            maphgt: '@',
            orient: '@',
            colormap: '@',
            colors: '@',
            custom: '@',
            incolor: '@',
            customize: '@',
            namemap: '@',
            queryhighlight: '@',
            textcolmap: '@'
            //pfammap: '@',
            //genestopfam: '@'
        },
        link: function(scope, element, attrs)
        {

            // Make elements of the list sortable and "deletable"
            // Items can be deleted by dragging them out of the viewing area
            var sortableIn = 0;
            jQuery("#img-list").sortable({
                receive: function(e, ui) { sortableIn = 1; },
                over: function(e, ui) { sortableIn = 1; },
                out: function(e, ui) { sortableIn = 0; },
                beforeStop: function(e, ui) {
                    if (sortableIn == 0) {
                        var p = confirm("Do you really want to remove this item?");
                        if (p == true) {
                            ui.item.delay(500).remove();
                        }
                    }
                }
            });


            // List the models to watch
            var modelsToWatch = ['mapwidth', 'maphgt', 'scaffold', 'names',
            'scalength', 'orient', 'colors', 'custom', 'incolor',
            'customize', 'namemap', 'colormap', 'queryhighlight', 'textcolmap']; //, 'pfammap', 'genestopfam'];

            scope.$watch( modelsToWatch.join(" + "), function(){

                // Parse JSON of the models passed to the scope
                var custom = jQuery.parseJSON(scope.custom);
                var colormap = jQuery.parseJSON(scope.colormap);
                var namemap = jQuery.parseJSON(scope.namemap);
                var textcolmap = jQuery.parseJSON(scope.textcolmap);
                var scaffold = jQuery.parseJSON(scope.scaffold);

                // console.log(currentselections);
                //var pfammap = jQuery.parseJSON(scope.pfammap);
                //var genestopfam = jQuery.parseJSON(scope.genestopfam);


                // Empty the element to allow redrawing of svg without
                // overwriting any pre-existing elements
                element.html("");

                var map_width = scope.mapwidth;

                if (map_width.length == 0) {
                    map_width = 960;
                }

                var qgene = scaffold.qgene;
                var sca_length = scope.scalength;

                // Add an id to this element
                element.attr('id', 'map-'+qgene.gene_id);

                var qstart = qgene.start;
                var qlength = qgene.dna_length;
                var qlenhalf = qlength / 2;
                var vstart = Math.round((qstart + qlenhalf) - (sca_length/2));

                // Assign the genes to a new variable for brevity
                var genes = scaffold.genes;

                // Get the set map height
                var map_height = scope.maphgt;

                // Append SVG to this element
                var map = d3.select(element[0]).
                    append('svg').
                    attr('width', map_width).
                    attr('height', map_height).
                    attr('class', 'svg');


                // Write the horizontal midline - the strand separator
                map.append('path').
                    attr('d', 'M 0 40 L '+map_width+' 40').
                    attr('stroke', 'black').
                    attr('stroke-width', 0.5);

                /*
                If identical orientation of the query genes is needed
                Two things have to be modified:
                1. The distances
                2. The strand

                For the distances of each gene, the new start positions will vary
                depending on the location of the gene in relation to the midpoint
                of the viewing range. There are 3 possibilities:

                1. The gene is the query gene
                    - new start = old end
                    - new end = old start

                2. The gene is originally located before the midpoint
                    - new start = old end + ((midpoint - old end) * 2)
                    - new end = old start + ((midpoint - old start) * 2)

                3. The gene is originally located after the midpoint
                    - new start = old end - ((old end - midpoint) * 2)
                    - new end = old start - ((old start - midpoint) * 2)
                */


                // Loop through the genes and perform the drawing by adding
                // elements to the parent SVG appended to the current element

                for (var i=0; i < genes.length; i++) {

                    var gene = genes[i];
                    var strand = gene.strand;
                    var start = gene.start, end = gene.end;

                    if (scope.orient == "identical") {
                        // Define the midpoint
                        var mid = vstart + (sca_length/2);

                        if (qgene.strand == "-") {
                            // Change the strand
                            if (gene.strand == "-") {
                                strand = "+";
                            } else if (gene.strand == "+") {
                                strand = "-";
                            }

                            // Change the start and stop positions:

                            // If the gene is the query gene
                            if (gene.gene_id == qgene.gene_id) {
                                start = gene.end;
                                end = gene.start;
                            }

                            // If the gene is located before the midpoint
                            if (gene.start < mid) {
                                start = gene.end + ((mid - gene.end) * 2);
                                end = gene.start + ((mid - gene.start) * 2);
                            }

                            // If the gene is located after the midpoint
                            if (gene.start > mid) {
                                start = gene.end - ((gene.end - mid) * 2);
                                end = gene.start - ((gene.start - mid) * 2);
                            }
                        }
                    }

                    // Determine start position of this gene in the x axis
                    var rx = ((start - vstart)/sca_length) * map_width;

                    // Determine the width of the gene box
                    var wd = (gene.dna_length * (map_width/sca_length));

                    // Define the height of the gene box, which for now, is fixed
                    var gh = 20;

                    // Define the length of arrowhead, which is also fixed
                    var ar = 8;

                    // To make sure that genes with a width of less than the arrowhead
                    // gets drawn in a nice way, we just set the length of the arrowhead
                    // as its width. This sacrifices accuracy for the sake of beauty,
                    // only for the really short genes.
                    if (wd < ar) {
                        wd = ar;
                    }

                    // Define the coordinates of the gene box
                    if (strand == "+") {
                        // For the gene from the plus strand
                        var ry = 16;
                        var ht = ry+gh;
                        var pt1 = "M "+rx+" "+ry+" L "+(rx+wd-ar)+" "+ry;
                        var pt2 = " L "+(rx+wd)+" "+(ht-(gh/2))+" L "+(rx+wd-ar)+" "+ht+" L "+rx+" "+ht+" L "+rx+" "+ry;
                        var pt = pt1 + pt2;
                    } else {
                        // For the gene from the minus strand
                        ry = 45;
                        ht = ry+gh;
                        pt1 = "M "+(rx+7)+" "+ry+" L "+(rx+wd)+" "+ry;
                        pt2 = " L "+(rx+wd)+" "+ht+" L "+(rx+7)+" "+ht+" L "+rx+" "+(ht-(gh/2))+" L "+(rx+7)+" "+ry;
                        pt = pt1 + pt2;
                    }


                    var color = scope.colors, gfill;

                    if (color == 'plain') {
                        gfill = 'lightgray';
                    } else if (color == 'bright') {
                        gfill = 'gold';
                    } else if (color == 'blank') {
                        gfill = 'white';
                    }

                    //else if (color == 'pfam') {
                      //  gfill = 'white';
                      //  fillopa = 0.0;
                    //}

                    // Use colors specified in the color map, if any
                    if (Object.size(colormap) > 0) {
                        var colgene = colormap[gene.gene_id];
                        if (colgene) {
                            gfill = colgene;
                        }
                    }

                    // If query highlighting is desired
                    if (parseInt(scope.queryhighlight) == 1) {
                        // If this is the query gene, hightlight it
                        // by drawing a bolder line where it falls in the midline
                        if (gene.gene_id == qgene.gene_id) {
                            map.append('path').
                                attr('d', 'M '+rx+' 40'+' L '+(rx+wd)+' 40').
                                attr('stroke', 'darkblue').
                                attr('stroke-width', 2);
                        }
                    }
					
          					var product_name = 'Product: ' + gene.product_name;
                    var annotation = gene.piliome_annotation;
                    
                    if (annotation.length > 0) {
                      product_name += '<br>PiliomeDB annotation: ' + annotation;
                    }
          					/*
                    if (gene.extra_pfam_a.length > 3) {
          						product_name += '<br>Pfam-A: ' + gene.extra_pfam_a;
          					}
          					if (gene.extra_pfam_b.length > 3) {
          						product_name += '<br>Pfam-B: ' + gene.extra_pfam_b;
          					}
          					if (gene.extra_cog.length > 3) {
          						product_name += '<br>COG: ' + gene.extra_cog;
          					}
                    */

                    // Draw the gene box
                    map.append('path').
                        attr('d', pt).
                        attr('class', 'gene').
                        attr('name', gene.gene_id+' ('+gene.id+')').
                        // The attributes product, location and width are used by tipsy
            						//attr('pfama', gene.extra_pfam_a).
            						//attr('pfamb', gene.extra_pfam_b).
            						//attr('cog', gene.extra_cog).
                        attr('product', product_name).
                        attr('location', gene.start+'..'+gene.end).
                        attr('width', wd).
                        attr('aalength', gene.amino_acid_sequence_length).
                        attr('fill', gfill).
                        attr('stroke', 'black').
                        attr('stroke-width', 1).
                        on('mouseover', function(){
                            d3.select(this).style("stroke-width", 2);
                        }).
                        on('mouseout', function(){
                            d3.select(this).style("stroke-width", 1);
                            $(".tipsy").remove();
                        }).
                        on('click', function(){
                            if (parseInt(scope.customize) == 1) {
                                d3.select('#context-menu').style('display', 'none');
                                scope.$emit('addToSelections', { geneOID: d3.select(this).attr("name")});
                            }
                        }).
                        on("contextmenu", function() {
                            // Prevent default right click event
                            d3.event.preventDefault();

                            if (parseInt(scope.customize) == 1) {

                                // Show the context menu
                                var cmenu = d3.select('#context-menu');
                                cmenu.style({
                                    display: 'block',
                                    top: d3.event.pageY + 'px',
                                    left: d3.event.pageX + 'px'
                                });

                                // Set the current selected gene
                                scope.$emit('setCurrentSelectedGene', { geneOID: d3.select(this).attr("name")});
                            }
                        });

                    // Check the naming required
                    var name;
                    if (scope.names == 'locusTag') {
                        name = gene.locus_tag;
                    } else if (scope.names == 'geneOID') {
                        name = gene.gene_id;
                    } else {
                        name = '';
                    }

                    // Override the name with the assigned name, if any
                    if (namemap[gene.gene_id]) {
                        name = namemap[gene.gene_id];
                    }

                    if (strand == "+") {
                        var textxpos = rx + (wd-ar)/2;
                    } else {
                        textxpos = rx + (wd+ar)/2;
                    }

                    // Write the gene name, if any
                    if (name.length > 0) {
                        map.append('text').
                            text(name).
                            attr('x', textxpos).
                            attr('y', ry+14).
                            attr('font-size', 12).
                            attr('text-anchor', 'middle').
                            attr('fill', textcolmap[gene.gene_id]);
                    }
                }

				/*
                // Fetch the taxon info then draw
                var scafUrl = '/api/v1/scaffold/?scaffold_name=' + scaffold.qgene.scaffold_name + '&format=json';
                var scaftype = '';
                d3.json(scafUrl, function(scaf){
					
	                var taxonUrl = '/api/v1/taxon/?img_taxon_id=' + scaffold.qgene.taxon_id + '&format=json';
	                d3.json(taxonUrl, function(taxon) {
						
						var taxonLabel = '';
						try {
		                    scaftype = scaf.objects[0].scaffold_type
							taxonLabel = taxon.objects[0].name + ' (' + scaftype + ')';
						} catch (TypeError) {
							scaftype = ''
							taxonLabel = taxon.objects[0].name;
						}
					
	                    map.append('text').
	                        text(taxonLabel).
	                        attr('x', 0).
	                        attr('y', 9).
	                        attr('font-size', 11).
	                        attr('fill', 'black');
	                });
					
                });
				*/
				
				// Faster way of drawing the taxon label
				var taxonLabel = qgene.fasta_description.split("[")[1].split(":")[0];
                map.append('text').
                    text(taxonLabel).
                    attr('x', 0).
                    attr('y', 9).
                    attr('font-size', 11).
                    attr('fill', 'black');
				

                scope.$emit("incrementMapsDrawn", {});

                jQuery('.gene').tipsy({
                    gravity: 's',
                    html: true,
                    title: function(){
						            var geneid = this.attributes.name.nodeValue;
                        var product = this.attributes.product.nodeValue;
                        var location = this.attributes.location.nodeValue;
                        var protlength = this.attributes.aalength.nodeValue;
            						//var pfama = this.attributes.pfama.nodeValue;
            						//var pfamb = this.attributes.pfamb.nodeValue;
            						//var cog = this.attributes.cog.nodeValue;
						            var label = product + "<br>Gene ID: "+geneid+"<br>Location: "+location+ "<br>Length: "+protlength; 
						            // +"<br>Pfam-A: "+pfama+"<br>Pfam-B: "+pfamb+"<br>COG: "+cog;
                        return label;
                    }
                });

                jQuery('body').click(function(){
                    $("#context-menu").hide();
                });
                
            })
        }
    }
});


// This directive governs the behavior of the checkbox buttons,
// inputs and other elements for the customization of user-selected genes

app.directive('customPanel', function(){
    return {
        restrict: 'E',
        scope: {
            colorpicker: '@',
            incolor: '@',
            params: '@',
            export: '@'
        },
        link: function(scope, element, attrs){

            function updateInputColor(newVal){
                jQuery('#input-color').val(newVal).trigger('input');
                jQuery('div.color-picker').css('border-color', newVal);
            }

            scope.$watch('colorpicker + incolor + params + export', function(){

                scope.$emit('customModification', {params: scope.params});

                //console.log(scope.export);
                if (parseInt(scope.export)) {
                    scope.$emit('exportSavedSelections', {});
                }

                var colp = parseInt(scope.colorpicker);

                if (colp == 1) {
                    // Build the color picker
                    var cp = jQuery("div.color-picker");
                    var picker = jQuery.farbtastic(cp);
                    picker.setColor(scope.incolor);
                    picker.linkTo(updateInputColor);
                    cp.draggable();
                    cp.css('border-color', scope.incolor);
                }

            })

        }
    }
});


// Directive that governs the color control
app.directive('colorControl', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){

            scope.$watch('colorPicker + inputSelectionColor + useColorFor', function(){

                var nameORgid = scope.useColorFor.split(':')[0];
                var use = scope.useColorFor.split(':')[1];
                var selectionCheck = scope.useColorFor.split(':')[2];

                // If this change is for a selection of genes
                if (selectionCheck) {

                    if (use == 'genecolor') {

                        jQuery.each(scope.currentSelections[nameORgid], function(index, member) {
                            var gid = Object.keys(member)[0];
                            member[gid]['genecolor'] = scope.inputSelectionColor;
                            scope.colorMap[gid] = scope.inputSelectionColor;
                        });

                    } else if (use == 'textcolor') {

                        jQuery.each(scope.currentSelections[nameORgid], function(index, member) {
                            var gid = Object.keys(member)[0];
                            member[gid]['textcolor'] = scope.inputSelectionColor;
                            scope.textColorMap[gid] = scope.inputSelectionColor;
                        });

                    }

                // If this change is for individual genes
                } else {

                    jQuery.each(scope.currentSelections, function(selname, members){
                        jQuery.each(members, function(index, member){
                            var gid = Object.keys(member)[0];
                            if (gid == nameORgid) {
                                member[nameORgid][use] = scope.inputSelectionColor;
                            }
                        });
                    });

                    if (use == 'genecolor') {
                        scope.colorMap[nameORgid] = scope.inputSelectionColor;

                    } else if (use == 'textcolor') {
                        scope.textColorMap[nameORgid] = scope.inputSelectionColor;
                    }

                }

            });

        }
    }
});


/*
The functions below are for the two sliders from jQuery UI and the color picker.
They update values in inputs that then pass these changes to ng-models.
 */


function updateWidth( newVal){
    $("#mw-input").val(newVal).trigger('input');
}

$(function () {
    var elem = $("#mw-slider");
    elem.slider({
        range: false,
        min: 150,
        max: 5000,
        step: 10,
        value: 960,
        slide: function (event, ui) {
            updateWidth(ui.value);
        }
    });

    updateWidth(elem.slider("value"));

    $("#mw-input").change(function () {

        elem.slider("value", $("#mw-input").val());
    });

});


function updateCompactness(newVal){
    $("#mc-input").val(newVal).trigger('input');
}

$(function () {
    var elem = $("#mc-slider");
    elem.slider({
        range: false,
        orientation: 'vertical',
        min: 70,
        max: 170,
        step: 2,
        value: 70,
        slide: function (event, ui) {
            updateCompactness(ui.value);
        }
    });

    updateCompactness(elem.slider("value"));

    $("#mc-input").change(function () {

        elem.slider("value", $("#mc-input").val());
    });

});