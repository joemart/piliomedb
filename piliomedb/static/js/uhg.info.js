var root = jQuery.parseJSON(jQuery("#taxondata").text())

/*
var colorMap = {
    "Bacteria" : "#3182bd",
    "Cyanobacteria": "#fdae6b",
    "Proteobacteria": "#9ecae1",
    "Firmicutes": "#6baed6",
    "Spirochaetes": "#c7e9c0",
    "Aquificae": "#31a354",
    "Acidobacteria": "#e6550d",
    "Fusobacteria": "#756bb1",
    "Deinococcus-Thermus": "#636363",
    "Bacteroidetes": "",
  };
*/  

var width = 200,
  height = 250,
  radius = Math.min(width, height) / 2,
  color = d3.scale.category20c();

var svg = d3.select("#taxondist").append("svg")
  .attr("width", width)
  .attr("height", height)
  .append("g")
  //.fill("fill", "black")
  .attr("transform", "translate(" + width / 2 + "," + height * .52 + ")");

var partition = d3.layout.partition()
  .sort(null)
  .size([2 * Math.PI, radius * radius])
  .value(function(d) { return 1; });

var arc = d3.svg.arc()
  .startAngle(function(d) { return d.x; })
  .endAngle(function(d) { return d.x + d.dx; })
  .innerRadius(function(d) { return Math.sqrt(d.y); })
  .outerRadius(function(d) { return Math.sqrt(d.y + d.dy); });

var infoPanel;
var path = svg.datum(root).selectAll("path")
  .data(partition.nodes)
  .enter().append("path")
  .attr("d", arc)
  .style("stroke", "#fff")
  .style("fill", function(d) { 
    var col = color((d.children ? d : d.parent).name)
    //console.log(d.name)
    //console.log(col)
    //if (colorMap[d.name] != undefined) {
    //  col = colorMap[d.name]
    //}
    return col; 
  })
  .style("fill-rule", "evenodd")
  .on("mouseover", function(d) {
    d3.select(this).style("fill", "gray")
    infoPanel = svg.append("text")
      .attr("font-size", "12px")
      .attr("y", -110)
      .attr("text-anchor", "middle")
      .text(function(){
        var pct = (d.size/root.size) * 100
        if (pct > 1.0) {
          pct = parseFloat(pct).toFixed(0)
        } else {
          pct = parseFloat(pct).toFixed(2)
        }
        return d.name + " (" + d.size + " - " + pct + "%)"
      })
  })
  .on("mouseout", function(d) {
    d3.select(this)
    .style("fill", function(d) {
      return color((d.children ? d : d.parent).name)
    })
    /*
    .style("fill", function(d) { 
      var col = color((d.children ? d : d.parent).name)
      if (colorMap[d.name] != undefined) {
        col = colorMap[d.name]
      }
      return col; 
    })
    */
    infoPanel.remove()
  });