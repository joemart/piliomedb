from data.models import UsherHomologyGroup, NonRedundantUsher
from django.conf import settings
from Bio import AlignIO, SearchIO
import os


hmm_profiles_dir = os.path.join(settings.PILIOMEDB_FILES, 'ushers_analyses', 'uhg_hmm_profiles')


def build_hmm_profiles():
    uhgs = UsherHomologyGroup.objects.all()
    for i, uhg in enumerate(uhgs):
        print '\n%s/%s - now dealing with UHG %s\n' % (i+1, uhgs.count(), uhg.id)
        outfasta = open(os.path.join(hmm_profiles_dir, 'uhg%s_ushers.fasta' % uhg.id), 'w')
        for member in uhg.get_members():
            nr_rep = NonRedundantUsher.objects.filter(representative__gene_id=member)
            if nr_rep:
                outfasta.write(nr_rep[0].alignment_sequence)
        outfasta.close()
        outsto = outfasta.name.split('.')[0] + '.sto'
        outsto = open(outsto, 'w')
        aln = AlignIO.read(outfasta.name, 'fasta')
        outsto.write(aln.format('stockholm'))
        outsto.close()
        if len(aln) >= 3:
            hmmfile = outfasta.name.split('.')[0] + '.hmm'
            os.system('hmmbuild -n UHG-%s %s %s' % (uhg.id, hmmfile, outsto.name))
            hmmfile = open(hmmfile, 'r')
            uhg.hmm_profile = hmmfile.read()
            uhg.save()
        os.remove(outfasta.name)
        os.remove(outsto.name)
    # Combine the profiles into one HMM file
    all_profiles = os.path.join(hmm_profiles_dir, 'uhg*_ushers.hmm')
    combined_hmm = os.path.join(hmm_profiles_dir, 'combined_ushers.hmm')
    os.system('cat %s > %s' % (all_profiles, combined_hmm))
    # Execute hmmpress
    os.system('hmmpress %s' % combined_hmm)
    

def read_hmmer_result(r):
    h = SearchIO.parse(r, 'hmmer3-text')
    names = {}
    for x in h:
        n = x.description.split('| ')[-1]
        if x.hits:
            for hit in x.hits:
                if hit.is_included:
                    th = int(x.hits[0].id.split('-')[-1])
                    if th in names.keys():
                        if n not in names[th]:
                            names[th].append(n)
                    else:
                        names[th] = [n]
    return names